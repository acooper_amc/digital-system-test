PART_REF__LOCAL                 = '0'
PART_REF__DIGITAL_TRANSMITTER   = '0'
PART_REF__400_TRANSMITTER       = '1'
PART_REF__DTR_TRANSMITTER       = '3'
PART_REF__AMC_1D8R              = '1'
PART_REF__AMC_ER8RM_8           = '5'
PART_REF__AMC_1DDA1_8           = '9'
PART_REF__AMC_1DBCI_8           = '10'
PART_REF__AMC_1DBCO_8           = '6'

XMRT_PART_REF_DICT = {
    '0' : 'auto',
    '1' : '400',
    '3' : 'DTR'
}

START_OF_ARGS = 4
MSG_CLASS     = 2
ENABLED       = '1'
GAS_TYPE__UNDEF = '0'
ADF_ANALOG_OUT_V4__ADMIN_STATUS = 5
ADF_RELAY_V4__ADMIN_STATUS = 9
ADF_SENSOR_V4__GAS_TYPE = 5
ADF_ZONE_V3__ADMIN_STATUS = 5
ADF_ZONE_SENSOR_V3__SENSOR_ID = 6

ENG_UNIT_DICT = {
    '0'  : r'ppm',
    '1'  : r'ppb',
    '2'  : r'%lel',
    '3'  : r'%vol',
    '4'  : r'ppm-BG',
    '5'  : r'%lel-BG',
    '6'  : r'EXT-I',
    '7'  : r'EXT-V',
    '8'  : r'PSI',
    '9'  : r'KPa',
    '10' : r'DegC',
    '11' : r'%RH',
    '12' : r'W.C.'
}



###############################################################################
# UML Classes

class UMLObject:
    """Parent class for all UML object"""

    def __init__(self):
        pass

    def makeUMLString(self):
        pass

class monitor( UMLObject ):
    """A class representing a digital monitor"""
    
    def __init__(self):
        self.localRelays    = {}
        self.localAnalogOut = {}

    def makeUMLString(self):
        masterStr = ''
        masterStr += '\n'
        masterStr += 'object 1DBX {\n'
        masterStr += '   __Local Relays__\n'
        for relayID in self.localRelays:
            masterStr += '   Relay ' 
            masterStr += relayID 
            masterStr += ' : Local ' 
            masterStr += self.localRelays[relayID] 
            masterStr += '\n'
        masterStr += '   __Local Analog Out__\n'
        for analogOutID in self.localAnalogOut:
            masterStr += '   AnalogOut ' 
            masterStr += analogOutID 
            masterStr += ' : Local ' 
            masterStr += self.localAnalogOut[analogOutID] 
            masterStr += '\n'
        masterStr += '}\n'

        return masterStr

class MODBUSInterface( UMLObject ):
    """A class representing a modbus interface"""

    def __init__(self, ID):
        self.ID = ID

class digitalDevice( UMLObject ):
    """A class representing a digital device"""
    def __init__(self, MODBUS_ID):
        self.MODBUS_ID = MODBUS_ID

class digitalTransmitter( digitalDevice ):
    """A class extending digitalDevice representing a digital transmitter"""

    def __init__(self, MODBUS_ID, sensor_list):
        digitalDevice.__init__(self, MODBUS_ID)
        self.sensors = sensor_list
    
    def makeUMLString(self, transmitterNum) :
        masterStr = ''
        masterStr += '\n'
        masterStr += 'object Digital_Transmitters_' 
        masterStr += str(transmitterNum)
        masterStr += '{\n'
        for sensorID in self.sensors:
            masterStr += '   Sensor ' + sensorID + ' : '
            masterStr += 'Address ' + self.sensors[sensorID]['address'] + ', '
            masterStr += self.sensors[sensorID]['gas label'] + ', '
            masterStr += self.sensors[sensorID]['location'] + ', '
            masterStr += XMRT_PART_REF_DICT[self.sensors[sensorID]['hardware']]
            if PART_REF__DTR_TRANSMITTER == self.sensors[sensorID]['hardware']:
                masterStr += self.sensors[sensorID]['instance']
            masterStr += '\n'
        masterStr += '}\n'

        return masterStr

class digitalModules( digitalDevice ): 
    """A class extending digitalDevice representing a digital module"""

    def __init__(self, MODBUS_ID, address):
        digitalDevice.__init__(self, MODBUS_ID)
        self.address = address

class relayModule( digitalModules ):
    """A class extending digitalModule representing a relay module"""

    def __init__(self, MODBUS_ID, address, relay_list):
        digitalModules.__init__(self, MODBUS_ID, address)
        self.relays = relay_list

class analogInputModule( digitalModules ):
    """A class extending digitalModule representing an analog input module"""

    def __init__(self, MODBUS_ID, address, sensor_list):
        digitalModules.__init__(self, MODBUS_ID, address)
        self.sensors = sensor_list

class analogOutputModule( digitalModules ):
    """A class extending digitalModule representing an analog output module"""

    def __init__(self, MODBUS_ID, address, output_list):
        digitalModules.__init__(self, MODBUS_ID, address)
        self.outputs = output_list

class RS485Configuration():
    """A clas represeninting notes on a modbus interface"""

    def __init__(self, MODBUS_ID, Protocol=None, BaudRate=None, CharLen=None, Parity=None, StopBits=None):
        self.MODBUS_ID  = MODBUS_ID
        self.Protocol   = Protocol
        self.BaudRate   = BaudRate
        self.CharLen    = CharLen
        self.Parity     = Parity
        self.StopBits   = StopBits

