import xml.etree.ElementTree as ET
import csv
import copy

#Defines and Mundane Numbers
def writeToXML(XML_path, Xpath_list, value_list) :
    err = 0
    err_msg = ""
    try:
        #Load default XML doc -> inputTree
        inputTree = ET.parse(XML_path)

        #Update inputTree with VPD serialNumber and MPN
        inputRoot = inputTree.getroot()
        
        index = 0
        for Xpath in Xpath_list :
            index = Xpath_list.index(Xpath)
            inputRoot.find(Xpath).text = value_list[index]
            
        #Write inputTree to SML file on server
        outputFilename = XML_path
        inputTree.write(
            outputFilename, 
            xml_declaration=True, 
            method='xml', 
            encoding='UTF-8')

    except FileNotFoundError:
        err_msg = "File Not Found"
        err = -1000
    
    except AttributeError:
        err_msg = "XML does not contain register/value"
        err = -1010

    except OSError:
        err_msg = "File string malformation"
        err = -1020

    except:
        err_msg = "An error occured"
        err = -1
        raise
    
    return (err , err_msg, value_list)

def readFromXML(targetFile, Xpath_list) :

    err = 0
    err_msg = ""
    value_list = []
    try:
        
        #Load read XML doc -> outputTree
        outputTree = ET.parse(targetFile)
        outputRoot = outputTree.getroot()

        #Update parameters
        for Xpath in Xpath_list :
            if Xpath != "" :
                xmlValue = outputRoot.find(Xpath).text
                if xmlValue is None: xmlValue = ''
                value_list.append(xmlValue)
            err_msg = ",".join(value_list)

    except FileNotFoundError:
        err_msg = "File Not Found"
        err = -1000
    
    except AttributeError:
        err_msg = "XML does not contain register/value"
        err = -1010

    except OSError:
        err_msg = "File string malformation"
        err = -1020

    except:
        err_msg = "An error occured"
        err = -1
        raise
    
    return (err, err_msg, value_list)

def searchCSV(filename, searchStr) :

    errVal = []

    #Open CSV into lists
    with open(filename, newline='') as csv_file:
        reader = csv.reader(csv_file)
        data = list(reader)

    
    for row in data:
        if searchStr in row:
            errVal = copy.deepcopy(row)
            break

    return (errVal)

    #Search through lists from value
    #Return row

if __name__ == "__main__" :
    
    # Xpath_list = [".//register/[name='cca_serial_number']/ram_value"]
    # value_list = ["A20085001"]

    # err = writeToXML(
    # XML_path = "C:\\SVN Local 2 - Electric Bugaloo\\New Test Procedures\\TestStand Test Sequeces\\Config\\amc_data__manufacturing_data.xml" ,
    # Xpath_list = Xpath_list,
    # value_list = value_list
    # )

    # print (err)

    filename = "\\\\AMC-DATA\\Operations\\Manufacturing\\manufacturing test\\TestStand Support File\\XMLfiles\\firmware_v4.1.16\\AMC-SM\\AMC-SM-91A01-SE1209.xml"

    XML_path = [".major_version", "minor_version", "release_version"]
    #XML_path = ["/memory_export/structures/structure[4]/registers/register[6]/value"]
    values   = ["20050002"]

    
    err = readFromXML(
        targetFile=filename,
        Xpath_list=XML_path
    )
    
    '''
    err = writeToXML(
        XML_path=filename,
        Xpath_list=XML_path,
        value_list=values
    )
    '''
    print (err)