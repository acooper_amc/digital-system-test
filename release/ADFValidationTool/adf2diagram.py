"""
This module as two functions: generate a graphic representation of hardware connects describe in an ADF file, and
generate a list of instructions describing the behaviour of the system.

The graphic is generated using PlantUML to make an object diagram.  Objects in teh diagram represent various MODBUS 
devices (monitors, transmitters, relay modules, etc).  The connections between the objects represtn the MODUS networks.
The graphic is saved as a PNG file.

The instructions are coded into a JSON comptable dictionary.  The following schema shows the keywords and their values.

approvalDate        : The date the instruction set was approved by Sales.  Format string = '%Y-%m-%d'
salesQC             : The user name of the person who approved the instruction set.  e.g. acooper
sysTestDate         : The date the system test was performed and passed.  Format string = '%Y-%m-%d'
mfgQC               : The user neame of the person who approved the instruction set.  e.g. acooper
stepHeader          : A hardcoded list of the directions for each instruction step.  These are identical for all seteps.
steps               : These are the setp specific instructions and limits
steps.sensorList    : A list of sensors that share behaviour.  Only one of these needs to be tested.
steps.signalSim     : The signal that needs to be simulated on the sensor to give a response.
steps.relayList     : A list of relays that will respond to the simulated signal.
steps.analogOutList : A list of analog outputs that with respond the the simulated signal.
steps.analogCurrent : A list of current outputs form the analog outputs.
"""

import sys
import builtins
#from parseADFMsgFSM import State, StateMachine
#import parseADFMsgFSM
import ADFobjects
import UMLObjects
import subprocess
import datetime
import getpass


version = [
    0, #Major Version
    4  #Minor Version
]

PART_REF__LOCAL                 = '0'
PART_REF__DIGITAL_TRANSMITTER   = '0'
PART_REF__400_TRANSMITTER       = '1'
PART_REF__DTR_TRANSMITTER       = '3'
PART_REF__AMC_1D8R              = '1'
PART_REF__AMC_ER8RM_8           = '5'
PART_REF__AMC_1DDA1_8           = '9'
PART_REF__AMC_1DBCI_8           = '10'
PART_REF__AMC_1DBCO_8           = '6'

XMRT_PART_REF_DICT = {
    '0' : 'auto',
    '1' : '400',
    '3' : 'DTR'
}

START_OF_ARGS = 4
START_OF_LABEL_ARGS = 3
MSG_CLASS     = 2
ENABLED       = '1'
GAS_TYPE__UNDEF = '0'
ADF_ANALOG_OUT_V4__ADMIN_STATUS = 5
ADF_RELAY_V4__ADMIN_STATUS = 9
ADF_SENSOR_V4__GAS_TYPE = 5
ADF_ZONE_V3__ADMIN_STATUS = 5
ADF_ZONE_SENSOR_V3__SENSOR_ID = 6

ENG_UNIT_DICT = {
    '0'  : r'ppm',
    '1'  : r'ppb',
    '2'  : r'%lel',
    '3'  : r'%vol',
    '4'  : r'ppm-BG',
    '5'  : r'%lel-BG',
    '6'  : r'EXT-I',
    '7'  : r'EXT-V',
    '8'  : r'PSI',
    '9'  : r'KPa',
    '10' : r'DegC',
    '11' : r'%RH',
    '12' : r'W.C.'
}

GAS_TYPE_DICT = {
    '1'     :   'Cl2',
    '2'     :   'ClO2',
    '3'     :   'CO',
    '4'     :   'HF',
    '5'     :   'ETO',
    '6'     :   'HCl',
    '7'     :   'H2S',
    '8'     :   'HCN',
    '9'     :   'MERCA',
    '10'    :   'NH3',
    '11'    :   'NO',
    '12'    :   'NO2',
    '13'    :   'O3',
    '14'    :   'SO2',
    '15'    :   'PH3',
    '16'    :   'SiH4',
    '17'    :   'GeH4',
    '18'    :   'F2',
    '19'    :   'COCl2',
    '20'    :   'EXP',
    '21'    :   'C3H8',
    '22'    :   'CH4',
    '23'    :   'H2',
    '24'    :   'CO2',
    '25'    :   'O2-IN',
    '26'    :   'O2-DE',
    '27'    :   'O2-WN',
    '28'    :   'PRESS',
    '29'    :   'VACUM',
    '30'    :   'AmbT',
    '31'    :   'SurfT',
    '32'    :   'RelHu',
    '33'    :   'DiffP',
    '34'    :   'C2H2',
    '35'    :   'Fuel',
    '36'    :   'VOC',
    '37'    :   'N2O',
    '38'    :   'OTHER',
    '201'   :   'USER1',
    '202'   :   'USER2',
    '203'   :   'USER3',
    '204'   :   'USER4',
    '205'   :   'USER5',
    '206'   :   'USER6',
    '207'   :   'USER7',
    '208'   :   'USER8',
    '209'   :   'USER9',
}

RS485_PROTOCOL_DICT = {
    '0' : 'MODBUS_RTU',
    '1' : 'MODBUS_ASCII',
    '2' : 'BACNET_MSTP'
}

RS485_BAUD_DICT = {
    '0' : '1200',
    '1' : '2400',
    '2' : '4800',
    '3' : '9600',
    '4' : '19200',
    '5' : '38400',
    '6' : '76800',
    '7' : '115200'
}

RS485_CHARLEN_DICT = {
    '0' : '7 bits',
    '1' : '8 bits'
}

RS485_PARITY_DICT = {
    '0' : 'none',
    '1' : 'even',
    '2' : 'odd'
}

RS485_STOPBITS_DICT = {
    '0' : '1 bit',
    '1' : '1.5 bits',
    '2' : '2 bits'
}

def update_label_dictionaries(gas_label_list, eng_label_list):
    for ADFGasLabel in gas_label_list:
        GAS_TYPE_DICT.update({ADFGasLabel.GasID : ADFGasLabel.GasLabel})

    for ADFEngLabel in eng_label_list:
        ENG_UNIT_DICT.update({ADFEngLabel.UnitID : ADFEngLabel.UnitLabel})


class stateVariables:
    """Variables passed between states and persist for the life of the FSM"""

    def __init__(self):
        self.filename                   = ''
        self.ADFMsgLists                = []
        self.ADFAnalogOutList           = []
        self.ADFRelayList               = []
        self.ADFSensorList              = []
        self.ADFZoneList                = []
        self.ADFZoneSensorList          = []
        self.ADFRS485ConfigurationList  = []
        self.ADFGasLabelList            = []
        self.ADFEngLabelList            = []
        self.UMLmonitor                 = UMLObjects.monitor()
        self.UMLDigitalTransmitterList  = []
        self.UMLRelayModuleList         = []
        self.UMLAnalogInModuleList      = []
        self.UMLAnalogOutModuleList     = []
        self.UMLRS485ConfigList         = []
        self.instructionJSONDict        = {
            'approvalDate'  : None,
            'salesQC'        : None,
            'sysTestDate'   : None,
            'mfgQC'         : None,
            'stepHeader'    : [
                'Using sensor', 
                'simulate this gas concentration',
                'These relays should change state.', 
                'These analog outputs',
                'should measure this current'],
            'steps'         : []
        }
        self.instructionStr             = ''

###############################################################################
# Default classes

class StateMachine:
    """Parent class for state machine"""
    def __init__(self, initialState):
        pass
    # Template method:
    def runAll(self, inputs):
        pass

class State:
    """Parent class for all states"""

    def run(self):
        pass
    
    def next(self, input):
        pass

###############################################################################
# State Machine Classes

class readyState(State):

    def next(self):
        return readFileState()
    
    def run(self, stateVars: stateVariables):
        """Prompts user for file to be converted if not argv"""

        print("Ready\n")
        
        if len(sys.argv) == 2:
            stateVars.filename = sys.argv[1]    
        else:
            stateVars.filename = input("Enter filename: ")
        stateVars.filename = stateVars.filename.replace('"', '')

        #TODO AC 2019-08-07 Add exception to catch bad file names or files that are not .ADF files

class readFileState(State):

    def next(self):
        return convertToObjectsState()

    def run(self, stateVars: stateVariables):
        """Reads in lines from a .ADF file and puts them into a list"""
        print("Opening File\n")
        try:
            fileObject = open(stateVars.filename, 'r')
        except IOError:
            print("Error opening file " + stateVars.filename)
        
        print("Reading File\n")
        lineTxt = fileObject.readline()
        while (lineTxt != ''):
            lineTxt = lineTxt.replace('{', '')
            lineTxt = lineTxt.replace('}', '')
            lineTxt = lineTxt.replace('\n', '')
            stateVars.ADFMsgLists.append(lineTxt.split(','))

            lineTxt = fileObject.readline()
        fileObject.close

class convertToObjectsState(State):

    def next(self):
        return createUMLObjectsState()

    def run(self, stateVars: stateVariables):
        """Converts ADF messages into ADF objects"""

        print("Converting to ADF objects\n")
        for msg in stateVars.ADFMsgLists:

            if ('ADFAnalogOutV4' == msg[MSG_CLASS]) and (ENABLED == msg[ADF_ANALOG_OUT_V4__ADMIN_STATUS]):
                stateVars.ADFAnalogOutList.append(ADFobjects.AnalogOutV4(*msg[START_OF_ARGS:]))

                #Rebase zero indexed variables to 1 index
                stateVars.ADFAnalogOutList[-1].ID = str(int(stateVars.ADFAnalogOutList[-1].ID) + 1)
                stateVars.ADFAnalogOutList[-1].Instance = str(int(stateVars.ADFAnalogOutList[-1].Instance) + 1)

            elif ('ADFRelayV4' == msg[MSG_CLASS]) and (ENABLED == msg[ADF_RELAY_V4__ADMIN_STATUS]):
                stateVars.ADFRelayList.append(ADFobjects.RelayV4(*msg[START_OF_ARGS:]))

                #Rebase zero indexed variables to 1 index
                stateVars.ADFRelayList[-1].ID = str(int(stateVars.ADFRelayList[-1].ID) + 1)
                stateVars.ADFRelayList[-1].Instance = str(int(stateVars.ADFRelayList[-1].Instance) + 1)

        
            elif ('ADFSensorV4' == msg[MSG_CLASS]) and (msg[ADF_SENSOR_V4__GAS_TYPE] != GAS_TYPE__UNDEF):
                stateVars.ADFSensorList.append(ADFobjects.SensorV4(*msg[START_OF_ARGS:]))

                #Rebase zero indexed variables to 1 index
                stateVars.ADFSensorList[-1].ID = str(int(stateVars.ADFSensorList[-1].ID) + 1)
                stateVars.ADFSensorList[-1].Instance = str(int(stateVars.ADFSensorList[-1].Instance) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['1'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['1'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['1']) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['2'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['2'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['2']) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['3'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['3'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['3']) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['fail'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['fail'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['fail']) + 1)

            elif ('ZoneV3' == msg[MSG_CLASS]) and (ENABLED == msg[ADF_ZONE_V3__ADMIN_STATUS]):
                stateVars.ADFZoneList.append(ADFobjects.ZoneV3(*msg[START_OF_ARGS:]))
                
                #Rebase zero indexed variables to 1 index
                stateVars.ADFZoneList[-1].ID = str(int(stateVars.ADFZoneList[-1].ID) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['1'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['1']) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['2'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['2']) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['3'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['3']) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['fail'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['fail']) + 1)
                stateVars.ADFZoneList[-1].AnalogOut = str(int(stateVars.ADFZoneList[-1].AnalogOut) + 1)
        
            elif ('ZoneSensorV3' == msg[MSG_CLASS]) and (msg[ADF_ZONE_SENSOR_V3__SENSOR_ID]) != '-':
                ZoneSensorsV3_temp = ADFobjects.ZoneSensorV3(*msg[START_OF_ARGS:])
                #Rebase zero indexed variables to 1 index
                ZoneSensorsV3_temp.ID = str(int(ZoneSensorsV3_temp.ID) + 1)
                ZoneSensorsV3_temp.ZoneID = str(int(ZoneSensorsV3_temp.ZoneID) + 1)
                ZoneSensorsV3_temp.SensorID = str(int(ZoneSensorsV3_temp.SensorID) + 1)

                #Only add zone sensors if ther corespnding zone is enabled
                for zone in stateVars.ADFZoneList:
                    if (zone.ID == ZoneSensorsV3_temp.ZoneID) and ('1' == zone.AdminState):
                        stateVars.ADFZoneSensorList.append(ZoneSensorsV3_temp)
            
            elif 'ADFRS485ParametersV4' == msg[MSG_CLASS]:
                stateVars.ADFRS485ConfigurationList.append(ADFobjects.ADFRS485ParametersV4(*msg[START_OF_ARGS:]))
                stateVars.ADFRS485ConfigurationList[-1].RS485ID = str(int(stateVars.ADFRS485ConfigurationList[-1].RS485ID) + 1)

            elif 'UserGasLabel' == msg[MSG_CLASS]:
                stateVars.ADFGasLabelList.append(ADFobjects.UserGasLabel(*msg[START_OF_LABEL_ARGS:]))
            
            elif 'UserEngLabel' == msg[MSG_CLASS]:
                stateVars.ADFEngLabelList.append(ADFobjects.UserEngLabel(*msg[START_OF_LABEL_ARGS:]))

            else:
                pass

class createUMLObjectsState(State):

    def next(self):
        return writeFileState()

    def run(self, stateVars: stateVariables): 
        """Creates UML objects based on ADF objects"""

        #Update dictionaries with custom labels
        update_label_dictionaries(stateVars.ADFGasLabelList, stateVars.ADFEngLabelList)

        RelaysByModebusInterface = {
            '1' : [],
            '2' : [],
            '3' : [],
            '4' : []
        }
        relayModule_set = set()

        #For each ADFRelay object, group in either into local (by part_ref) 
        # or by MODBUS interface
        for relay in stateVars.ADFRelayList:
            if relay.PartRef == PART_REF__LOCAL:
                stateVars.UMLmonitor.localRelays.update({relay.ID : relay.Instance})
            elif PART_REF__AMC_ER8RM_8 == relay.PartRef:
                RelaysByModebusInterface[relay.Interface].append(relay)
                relayModule_set.add((relay.Interface, relay.Address))
            else:
                pass

        #For each module in the relay set, create a UML object
        relayModule_set = sorted(relayModule_set) 
        for module in relayModule_set:
            relay_list = {}
            for relay in RelaysByModebusInterface[module[0]]:
                if (relay.Address == module[1]):
                    relay_list.update({relay.ID: relay.Instance})
            stateVars.UMLRelayModuleList.append(UMLObjects.relayModule(module[0], module[1], relay_list) )
        
        AnalogOutByModebusInterface = {
            '1' : [],
            '2' : [],
            '3' : [],
            '4' : []
        }
        analogOutModule_set = set()

        #For each ADFAnalogOut object, group it either in local (by part_ref) or
        #by MODBUS interface
        for analogOut in stateVars.ADFAnalogOutList:
            if analogOut.PartRef == PART_REF__LOCAL:
                stateVars.UMLmonitor.localAnalogOut.update({analogOut.ID : analogOut.Instance})
            elif PART_REF__AMC_1DBCO_8 == analogOut.PartRef:
                AnalogOutByModebusInterface[analogOut.Interface].append(analogOut)
                analogOutModule_set.add((analogOut.Interface, analogOut.Address))
            else:
                pass
        
        #For each module in the analongOut set, create a UML object
        analogOutModule_set = sorted(analogOutModule_set)
        for module in analogOutModule_set:
            analogOutList = {}
            for analogOut in AnalogOutByModebusInterface[module[0]]:
                if (analogOut.Address == module[1]):
                    analogOutList.update({analogOut.ID : analogOut.Instance})
            stateVars.UMLAnalogOutModuleList.append(UMLObjects.analogOutputModule(module[0], module[1], analogOutList))
        
        AnalogInByModebusInterface = {
            '1' : [],
            '2' : [],
            '3' : [],
            '4' : []
        }    
        SensorsByModebusInterface = {
            '1' : [],
            '2' : [],
            '3' : [],
            '4' : []
        }

        #Group digital transmitters by MODBUS interface and
        #group analog inouts by MODBUS interface
        analogInModule_set = set()
        for sensor in stateVars.ADFSensorList: 

            if PART_REF__DIGITAL_TRANSMITTER == sensor.PartRef or PART_REF__400_TRANSMITTER == sensor.PartRef or PART_REF__DTR_TRANSMITTER == sensor.PartRef:
                SensorsByModebusInterface[sensor.Interface].append(sensor)
                
            elif PART_REF__AMC_1DBCI_8 == sensor.PartRef:
                AnalogInByModebusInterface[sensor.Interface].append(sensor)
                analogInModule_set.add((sensor.Interface, sensor.Address))

        #for each group of sensors sharing a MODBUS interface, create one UMLSensor object
        analogInModule_set = sorted(analogInModule_set)
        for MODBUSinterface in SensorsByModebusInterface:
            sensor_list = {}
            for sensor in SensorsByModebusInterface[MODBUSinterface]:
                sensorIsDigitalTransmitter  = PART_REF__DIGITAL_TRANSMITTER == sensor.PartRef
                sensorIs400                 = PART_REF__400_TRANSMITTER == sensor.PartRef
                sensorIsDTR                 = PART_REF__DTR_TRANSMITTER == sensor.PartRef

                if sensorIsDigitalTransmitter or sensorIs400:
                    sensor_list.update({sensor.ID : {'address' : sensor.Address, 'gas label' : GAS_TYPE_DICT[sensor.GasLabel], 'location' : sensor.Location, 'hardware' : sensor.PartRef}})
                elif sensorIsDTR:
                    sensor_list.update({sensor.ID : {'address' : sensor.Address, 'gas label' : GAS_TYPE_DICT[sensor.GasLabel], 'location' : sensor.Location, 'hardware' : sensor.PartRef, 'instance' : sensor.Instance}})

            stateVars.UMLDigitalTransmitterList.append(UMLObjects.digitalTransmitter(MODBUSinterface, sensor_list))
        
        for module in analogInModule_set:
            sensor_list = {}
            for sensor in AnalogInByModebusInterface[module[0]]:
                if (sensor.Address == module[1]):
                    sensor_list.update({sensor.ID : {'instance' : sensor.Instance, 'gas label' : GAS_TYPE_DICT[sensor.GasLabel], 'location' : sensor.Location}})
            stateVars.UMLAnalogInModuleList.append(UMLObjects.analogInputModule(module[0], module[1], sensor_list))

        for interface in stateVars.ADFRS485ConfigurationList:
            stateVars.UMLRS485ConfigList.append(UMLObjects.RS485Configuration(
                interface.RS485ID,
                interface.Protocol,
                interface.BaudRate,
                interface.CharLen,
                interface.Parity,
                interface.StopBits
            ))


class writeFileState(State):

    def __init__(self):
        self.masterStr = ''

    def next(self):

        return createInstructionsInJSONState()
        
    def run(self, stateVars: stateVariables) :
        """Writes UML objects into a PUML file"""

        from pathlib import Path

        print('Writing .puml file\n')
        #Add plantUML open string
        self.masterStr += '@startuml\n'

        #Add a title
        path = Path(stateVars.filename)
        title = path.name
        title = title.replace('\\', '\\\\')
        self.masterStr += ('\n' + 'title ' + title + '\\n' + 'Block Wiring Diagram' + '\n')

        #Create a monitor graphic object
        self.masterStr += ''
        self.masterStr += stateVars.UMLmonitor.makeUMLString()

        ObjectsByMODBUSInterface = {
            '1' : [],
            '2' : [],
            '3' : [],
            '4' : []
        }

        RS485NotesByMODBUSInterface = {
            '1' : '',
            '2' : '',
            '3' : '',
            '4' : '',
            '5' : ''
        }

        #Create digital transmiter graphic objects
        transmitterNum = 1
        for digitalTransmitterObject in stateVars.UMLDigitalTransmitterList:
            if(0 != len(digitalTransmitterObject.sensors)):
                self.masterStr += ''
                self.masterStr += '\n'
                self.masterStr += 'object Digital_Transmitters_' 
                self.masterStr += str(transmitterNum)
                self.masterStr += '{\n'
                for sensorID in digitalTransmitterObject.sensors:
                    self.masterStr += '   Sensor ' + sensorID + ' : '
                    self.masterStr += 'Address ' + digitalTransmitterObject.sensors[sensorID]['address'] + ', '
                    self.masterStr += digitalTransmitterObject.sensors[sensorID]['gas label'] + ', '
                    self.masterStr += digitalTransmitterObject.sensors[sensorID]['location'] + ', '
                    self.masterStr += XMRT_PART_REF_DICT[digitalTransmitterObject.sensors[sensorID]['hardware']]
                    if PART_REF__DTR_TRANSMITTER == digitalTransmitterObject.sensors[sensorID]['hardware']:
                        self.masterStr += digitalTransmitterObject.sensors[sensorID]['instance']
                    self.masterStr += '\n'

                self.masterStr += '}\n'
                ObjectsByMODBUSInterface[digitalTransmitterObject.MODBUS_ID].append('Digital_Transmitters_' + str(transmitterNum))
                transmitterNum += 1
            
        #Create analog input module graphic objects
        AnalogInNum = 1
        for analogInObject in stateVars.UMLAnalogInModuleList:
            self.masterStr += '\n'
            self.masterStr += 'object AnalogIn_' + str(AnalogInNum) +  '{\n'
            self.masterStr += '   Address : ' + analogInObject.address + '\n'
            self.masterStr += '   __Sensors__\n'

            for sensorID in analogInObject.sensors:
                self.masterStr += '   Sensor ' + sensorID + ' : '
                self.masterStr += 'Channel ' + analogInObject.sensors[sensorID]['instance'] + ', '
                self.masterStr += analogInObject.sensors[sensorID]['gas label'] + ', '
                self.masterStr += analogInObject.sensors[sensorID]['location'] + '\n'
            
            self.masterStr += '}\n'
            ObjectsByMODBUSInterface[analogInObject.MODBUS_ID].append('AnalogIn_' + str(AnalogInNum)) 
            AnalogInNum = AnalogInNum + 1

        #Create analog output module graphic objects
        AnalogOutNum = 1
        for analogOutObject in stateVars.UMLAnalogOutModuleList:
            self.masterStr += '\n'
            self.masterStr += 'object AnalogOut_' + str(AnalogOutNum) +  '{\n'
            self.masterStr += '   Address : ' + analogOutObject.address + '\n'
            self.masterStr += '   __Outputs__\n'
            for analogOutID in analogOutObject.outputs:
                self.masterStr += '   Analog Out ' + analogOutID + ' : Channel ' + analogOutObject.outputs[analogOutID] + '\n'
            self.masterStr += '}\n'
            ObjectsByMODBUSInterface[analogOutObject.MODBUS_ID].append('AnalogOut_' + str(AnalogOutNum))
            AnalogOutNum = AnalogOutNum + 1

        #Create relay module graphic objects
        RelayModuleNum = 1
        for relayObject in stateVars.UMLRelayModuleList:
            self.masterStr += '\n'
            self.masterStr += 'object RelayModule_' + str(RelayModuleNum) +  '{\n'
            self.masterStr += '   Address : ' + relayObject.address + '\n'
            self.masterStr += '   __Relays__\n'
            for relayID in relayObject.relays:
                self.masterStr += '   Relay ' + relayID + ' : Local ' + relayObject.relays[relayID] + '\n'
            self.masterStr += '}\n'
            ObjectsByMODBUSInterface[relayObject.MODBUS_ID].append('RelayModule_' + str(RelayModuleNum))
            RelayModuleNum = RelayModuleNum + 1

        #Create MODBUS Notes
        NoteNum = 1
        for RS485Interface in stateVars.UMLRS485ConfigList:
            self.masterStr += '\n'
            self.masterStr += 'note as ModbusNote' + str(NoteNum) + '\n'
            self.masterStr += '   MODBUS:       ' + RS485Interface.MODBUS_ID + '\n'
            self.masterStr += '   Protocol:     ' + RS485_PROTOCOL_DICT[RS485Interface.Protocol] + '\n'
            self.masterStr += '   BaudRate:     ' + RS485_BAUD_DICT[RS485Interface.BaudRate] + '\n'
            self.masterStr += '   CharLen:      ' + RS485_CHARLEN_DICT[RS485Interface.CharLen] + '\n'
            self.masterStr += '   Parity:       ' + RS485_PARITY_DICT[RS485Interface.Parity] + '\n'
            self.masterStr += '   StopBits:     ' + RS485_STOPBITS_DICT[RS485Interface.StopBits] + '\n'
            self.masterStr += 'end note' + '\n'
            RS485NotesByMODBUSInterface.update({RS485Interface.MODBUS_ID : 'ModbusNote' + str(NoteNum)})
            NoteNum = NoteNum + 1

        #Create a list of active MODBUS interface
        active_interfaces = []
        for interface in ObjectsByMODBUSInterface:
            if len(ObjectsByMODBUSInterface[interface]) != 0:
                active_interfaces.append(interface)
        
        #Create links between first elements for formatting
        self.masterStr += '\n'
       
        for interface in active_interfaces:
            count = 1
            for graphicObject in ObjectsByMODBUSInterface[interface]:
                if 1 == count:
                    self.masterStr += '1DBX -- ' + RS485NotesByMODBUSInterface[interface] + '\n'
                    self.masterStr += RS485NotesByMODBUSInterface[interface]+  ' -- ' + graphicObject + '\n'
                    if len(ObjectsByMODBUSInterface[interface]) != 1:
                        self.masterStr += graphicObject + ' -- '
                elif len(ObjectsByMODBUSInterface[interface]) == count:
                    self.masterStr += graphicObject + '\n'
                else:
                    self.masterStr += graphicObject + '\n'
                    self.masterStr += graphicObject + ' -- '
                count += 1    

        self.masterStr += '\n'

        #Add plantUML close string
        self.masterStr += '@enduml'

        #Write plantUML to png
        import plantuml
        target_png_file = stateVars.filename.split('.')[0] + '.png'
        plantumlHandle = plantuml.PlantUML()
        png_blob = plantumlHandle.processes(self.masterStr)
        with open(target_png_file, 'wb') as file_handle:
            file_handle.write(png_blob)
        


class createInstructionsInJSONState(State): 

    def next(self):
        return finishState()

    def run(self, stateVars: stateVariables):
        """Uses ADF objects to create an instruction file for technicians"""

        #Update dictionaries with custom labels
        update_label_dictionaries(stateVars.ADFGasLabelList, stateVars.ADFEngLabelList)     

        stateVars.sensorsByRelay  = {}
        #For each programmed relay...

        enabled_relay_list = []
        for relay in stateVars.ADFRelayList:
            enabled_relay_list.append(relay.ID)

        enabled_analogOut_list = []
        for analogOut in stateVars.ADFAnalogOutList:
            enabled_analogOut_list.append(analogOut.ID)

        analogOut_dict = {}
        for analogOut in stateVars.ADFAnalogOutList:
            analogOut_dict.update({analogOut.ID : analogOut })

        zone_dict = {}
        for zone in stateVars.ADFZoneList:
            zone_dict.update({zone.ID : zone})

        num_sensors_in_zone = {}
    
        for zone_sensor in stateVars.ADFZoneSensorList:
            if not zone_sensor.ZoneID in num_sensors_in_zone:
                num_sensors_in_zone.update({zone_sensor.ZoneID : 1})
            else:
                num_sensors_in_zone[zone_sensor.ZoneID] += 1

        allRows = []
        #For each sensors...
        for sensor in stateVars.ADFSensorList:
            #For each alarm level
            for alarmLvl in sorted(sensor.AlarmRelays.keys()):
                row_dict = {
                'sensorID'      : '',
                'relayID'       : [],
                'zoneID'        : [],
                'analogOutID'   : [],
                'alarmC'        : [],
                'analogSig'     : []
                }
                #...write the sensor number
                row_dict['sensorID'] = sensor.ID
                #...write the relay number, and ...
                if sensor.AlarmRelays[alarmLvl] != '':
                    row_dict['relayID'].append(sensor.AlarmRelays[alarmLvl])
                    #...Calculate the alarm concentration
                    if 'fail' == alarmLvl:
                        row_dict['alarmC'] = ['fail']
                    else:
                        transmiterIsAuto = PART_REF__DIGITAL_TRANSMITTER == sensor.PartRef
                        transmiterIs400 = PART_REF__400_TRANSMITTER == sensor.PartRef
                        transmiterIsDTR = PART_REF__DTR_TRANSMITTER == sensor.PartRef

                        if transmiterIsAuto or transmiterIs400 or transmiterIsDTR:
                            concentration = int(sensor.AlarmSetPoints[alarmLvl]) / 100
                            concentration = concentration * (int(sensor.FullScale) / 10)
                            row_dict['alarmC'].append(str(concentration) + ENG_UNIT_DICT[sensor.EngUnit] + ' ' + GAS_TYPE_DICT[sensor.GasLabel])

                        elif PART_REF__AMC_1DBCI_8 == sensor.PartRef:
                            concentration = int(sensor.AlarmSetPoints[alarmLvl]) / 100
                            concentration = (concentration * 16.0) + 4
                            row_dict['alarmC'].append(str(concentration) + 'mA')
                        else:
                            pass
                        
                    for zone_sensor in stateVars.ADFZoneSensorList:
                        if zone_sensor.SensorID == sensor.ID:
                            row_dict['zoneID'].append(zone_sensor.ZoneID)
                    for zoneID in row_dict['zoneID']:
                        if zone_dict[zoneID].AlarmRelays[alarmLvl] in enabled_relay_list:
                            row_dict['relayID'].append(zone_dict[zoneID].AlarmRelays[alarmLvl])
                        if (zone_dict[zoneID].AnalogOut in enabled_analogOut_list) and (['fail'] != row_dict['alarmC']):
                            row_dict['analogOutID'].append(zone_dict[zoneID].AnalogOut)

                        if zone_dict[zoneID].AnalogOut in enabled_analogOut_list and (['fail'] != row_dict['alarmC']):
                            concentration = int(sensor.AlarmSetPoints[alarmLvl]) / 100
                            if '0' == analogOut_dict[zone_dict[zoneID].AnalogOut].Type:
                                concentration = concentration / num_sensors_in_zone[zoneID]

                            if '0' == analogOut_dict[zone_dict[zoneID].AnalogOut].Range:
                                concentration = (concentration * 16.0) + 4
                            else:
                                concentration = concentration * 20                     
                            row_dict['analogSig'].append(str(concentration) + 'mA')
                allRows.append(row_dict)
        
        #Condense rows
        row_list = []

        for row in allRows:
            one_row = {
            'sensorList'    : [],
            'signalSim'     : [],
            'relayList'     : [],
            'analogOutList' : [],
            'analogCurrent' : []
        }
            if len(row['relayID']) != 0:
                one_row['sensorList'].append(row['sensorID'])
                one_row.update({'signalSim'  : row['alarmC']})
                for relayID in row['relayID']:
                    one_row['relayList'].append(relayID)

                if 'fail' != row['alarmC']:
                    for analogOutID in row['analogOutID']:
                        one_row['analogOutList'].append(analogOutID)

                if 'fail' != row['alarmC']:
                    for analogSig in row['analogSig']:
                        one_row['analogCurrent'].append(analogSig)

                row_list.append(one_row)

        #Collapse the rows into groups of sensors with common relays and analog outputs
        row_ind = 0
        duplicate_rows = []
        for row in row_list[:-1]:
            row_to_test_ind = row_ind + 1
            for row_to_test in row_list[row_to_test_ind:]:
                if len(row) > 3 and len(row_to_test) > 3:

                    if (row['relayList'] == row_to_test['relayList']) and (row['analogOutList'] == row_to_test['analogOutList']):
                        row_list[row_ind]['sensorList'] = row_list[row_ind]['sensorList'] + row_to_test['sensorList']
                        row_list[row_ind]['signalSim'] = row_list[row_ind]['signalSim'] + row_to_test['signalSim']
                        duplicate_rows.append(row_to_test)
                else:
                    if (row['relayList'] == row_to_test['relayList']):
                        row_list[row_ind]['sensorList'] = row_list[row_ind]['sensorList'] + row_to_test['sensorList']
                        row_list[row_ind]['signalSim'] = row_list[row_ind]['signalSim'] + row_to_test['signalSim']
                        duplicate_rows.append(row_to_test)
                row_to_test_ind += 1
            row_ind += 1 
        
        #Remove duplicates for duplicate_rows
        final_list = []
        for dup_row in duplicate_rows:
            if dup_row not in final_list:
                final_list.append(dup_row)
        duplicate_rows = final_list

        #remove duplicate rows from row_list
        for dup_row in duplicate_rows:
                row_list.remove(dup_row)

        stateVars.instructionJSONDict['steps'] = row_list


class finishState(State): pass

class adfToDiagramFSM(StateMachine):
    """Extened FSM class"""
    
    def __init__(self, initialState=readyState(), fileName=''):
        self.stateVars = stateVariables()
        self.stateVars.filename = fileName
        self.currentState = initialState

    def runAll(self):
        while not isinstance(self.currentState, finishState):
            self.currentState.run(self.stateVars)
            self.currentState = self.currentState.next()

if __name__ == "__main__":
    adfToDiagramFSM_instance = adfToDiagramFSM()
    adfToDiagramFSM_instance.runAll()
    
    import json

    target_json_file = adfToDiagramFSM_instance.stateVars.filename.split('.')[0]
    target_json_file += '.json'
    with open(target_json_file, 'w') as file_handle:
        json.dump(adfToDiagramFSM_instance.stateVars.instructionJSONDict, file_handle, indent=4)
        
    
