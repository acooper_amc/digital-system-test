import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox

import re
import getpass
import datetime
from pathlib import Path
import os
import json
import textwrap

import adf2diagram

import ctypes
user32 = ctypes.windll.user32
screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1) 

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

VERSION = '1.3'

FILENAME_PATTERN_RE = '.*SO[0-9]{5}.*'

DATE_FORMAT_STR = "%Y-%m-%d"

CB_PALLET   = False

BLACK       = '#000000'
ORANGE      = '#FFA500' #web orange
SKY_BLUE    = '#87CEEB' #web skyblue
BLUE_GREEN  = '#00AD83' #pantone green
YELLOW      = '#FFFF00' #web yellow
BLUE        = '#0000FF' #web blue
VERMILLION  = '#E34234' #web vermillion
PURPLE      = '#800080' #web purple
WHITE       = '#FFFFFF'

if CB_PALLET:
    BLACK       = '#000000'
    ORANGE      = '#E69F00'
    SKY_BLUE    = '#56B4E9'
    BLUE_GREEN  = '#009E9B'
    YELLOW      = '#F0E442'
    BLUE        = '#0072B2'
    VERMILLION  = '#D55E00'
    PURPLE      = '#CC79A7'
    WHITE       = '#FFFFFF'

class mainWindowClass:

#Constructor Methods
    def __init__(self, master):

        master.title("ADF Validation Utility")

        #dims = str(int(screensize[0] * 0.6)) + 'x' + str(int(screensize[1] * 0.6))
        #master.geometry(dims)

        #Create class members
        #self.master             = master
        self.approvalCheckList  = []
        self.approvalCheckVars  = []
        self.relayTestResults   = []
        self.analogTestResults  = []
        self.defaultbg = master.cget('bg')
        self.SOnumber           = ''
        self.adf2DiaFSM         = adf2diagram.adfToDiagramFSM()
        self.wiringDiagram      = None
        self.instructionsJSON = {
            'approvalDate'  : None,
            'salesQC'       : None,
            'sysTestDate'   : None,
            'mfgQC'         : None,
            'stepHeader'    : [
                'Using sensor', 
                'simulate this gas concentration',
                'These relays should chage state.', 
                'These analog outputs',
                'should measure this current'],
            'steps'         : []
        }

        self.archiveInstructionsJSON = {}
        self.diagramAppvlCheckVar = tk.IntVar()

        #Create frames
        self.appFrame           = tk.Frame(master, bd=0)
        self.appFrame.grid()
        self.appCanvas          = tk.Canvas(self.appFrame, 
                                            bd=0, 
                                            width=int(screensize[0] * 0.6), 
                                            height=int(screensize[1] * 0.6),
                                            scrollregion=(0,0, int(screensize[0] * 0.6), screensize[1]*3))
        self.appCanvas.grid(sticky=tk.N+tk.S+tk.E+tk.W)
        self.windowFrame        = tk.Frame(self.appCanvas, bd=0)
        self.appCanvas.create_window((0,0), window=self.windowFrame, anchor=tk.N+tk.W)

        self.appScroll          = tk.Scrollbar(self.appFrame, orient=tk.VERTICAL)
        self.appScroll.grid(row=0, column=1, sticky=tk.N+tk.S)
        self.appCanvas.config(yscrollcommand=self.appScroll.set)
        self.appScroll.config(command=self.appCanvas.yview)


        self.filenameFrame      = tk.Frame(self.windowFrame, bd=0)
        self.instructionFrame   = tk.LabelFrame(self.windowFrame, text="Instruction List", padx=5, pady=5)
        self.salesApprovalFrame = tk.LabelFrame(self.windowFrame, text="Sales Approval", padx=5, pady=5)
        self.systemTestFrame    = tk.LabelFrame(self.windowFrame, text="System Test", padx=5, pady=5)
        self.mfgMetaFrame       = tk.Frame(self.windowFrame, bd=0)
        self.statusFrame        = tk.LabelFrame(self.windowFrame, text='Validation Status', padx=5, pady=5)

        #Create popup windows
        self.popup = tk.Toplevel()
        self.popup.title('Wiring Diagram')
        self.popup.geometry("+0+0")

        #Create wiring diagram canvas
        self.wiringDiagramFrame = tk.Frame(self.popup, bd=2, relief=tk.SUNKEN)

        self.wiringDiagramFrame.grid_rowconfigure(0, weight=1)
        self.wiringDiagramFrame.grid_columnconfigure(0, weight=1)

        self.xscrollbar = tk.Scrollbar(self.wiringDiagramFrame, orient=tk.HORIZONTAL)
        self.xscrollbar.grid(row=1, column=0, sticky=tk.E+tk.W)

        self.yscrollbar = tk.Scrollbar(self.wiringDiagramFrame)
        self.yscrollbar.grid(row=0, column=1, sticky=tk.N+tk.S)

        self.wiringDiagramCanvas = tk.Canvas(self.wiringDiagramFrame, bd=0, 
                                            width = 120, 
                                            height = 120,
                                            xscrollcommand=self.xscrollbar.set,
                                            yscrollcommand=self.yscrollbar.set) 

        self.wiringDiagramCanvas.grid(row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)

        self.xscrollbar.config(command=self.wiringDiagramCanvas.xview)
        self.yscrollbar.config(command=self.wiringDiagramCanvas.yview)

        self.wiringDiagramFrame.pack()


        #Create widgets
        self.statusBar              = tk.Label(self.statusFrame, anchor=tk.W, text='')
        self.ADFfilename            = tk.Label(self.filenameFrame, relief = "sunken", text = '')
        self.approvalDate           = tk.Label(self.salesApprovalFrame, relief = "sunken", text = '')
        self.salesQC                = tk.Label(self.salesApprovalFrame, relief = "sunken", text = '')
        self.sysTestDate            = tk.Label(self.systemTestFrame, relief = 'sunken', text='')
        self.mfgQC                  = tk.Label(self.systemTestFrame, relief = 'sunken', text='')
        self.salesApprovedButton    = tk.Button(self.salesApprovalFrame, text='Approve and Save', state='disabled', command=self.salesApprovedButtonCmd)
        self.systemTestButton       = tk.Button(self.systemTestFrame, text='Start System Test', state='disabled', command=self.systemTestButtonCmd) 
        self.diagramAppvlCheck      = tk.Checkbutton(
            self.salesApprovalFrame, 
            variable=self.diagramAppvlCheckVar, 
            command=self.approvalCheckCmd,
            state='disabled'
            )

        #Create menu bar
        menubar = tk.Menu(master) 
        master.config(menu=menubar) 
        filemenu = tk.Menu(menubar, tearoff = 0) 
        menubar.add_cascade(label='File', menu=filemenu) 
        filemenu.add_command(label='Open ADF...', command=self.fileMenuCmd) 
        filemenu.add_separator() 
        filemenu.add_command(label='Exit', command=master.quit) 

        helpmenu = tk.Menu(menubar, tearoff = 0) 
        menubar.add_cascade(label='Help', menu=helpmenu) 
        helpmenu.add_command(label='About', command=self.helpAboutCmd)
        
        #Create permanant labels
        self.createPermanentLables()
        
        #Layout window
        self.layoutWindow()      

#Command Methods
    def fileMenuCmd(self):
        self.resetUIWidgets()

        #Get filename
        filename = ''
        filename = self.getFilenameDialog()
        
        #Display filename
        self.ADFfilename.config(text=filename)

        #Run ADF2diagram FSM & create sales metadata
        self.createInstructionsJSON()

        #Unlock Sales Validation Mode
        self.defaultSalesMode()

        #Display wiring diagram
        self.displayWiringDiagram()

        #Look for existing .CSV file
        try:
            with open(filename[:-4] + '.json', 'r') as CSVfile:
                #Read Instructions form CSV
                self.loadJSON(CSVfile)
                #Compare CSV insrcustins to ADF instructions
                if(self.instructionsJSON['steps'] == self.archiveInstructionsJSON['steps']):              
                    #Update metadate from CSV
                    self.instructionsJSON['approvalDate'] = self.archiveInstructionsJSON['approvalDate']
                    self.approvalDate.config(text = self.instructionsJSON['approvalDate'])

                    self.instructionsJSON['salesQC'] = self.archiveInstructionsJSON['salesQC']
                    self.salesQC.config(text = self.instructionsJSON['salesQC'])

                    self.instructionsJSON['sysTestDate'] = self.archiveInstructionsJSON['sysTestDate']
                    if self.instructionsJSON['sysTestDate']  != None:
                        self.sysTestDate.config(text = self.instructionsJSON['sysTestDate'])
                    
                    self.instructionsJSON['mfgQC'] = self.archiveInstructionsJSON['mfgQC']
                    if self.instructionsJSON['mfgQC'] != None:
                        self.mfgQC.config(text = self.instructionsJSON['mfgQC'])
                    
                    self.lockSalesMode()
                    self.unlockMfgMode()
                    
        except IOError:   
            pass       

        #Update window
        self.displayMetadata()
        self.displayInstructions()

    def helpAboutCmd(self):
        msgStr = (
            'ADF Validation Tool' + '\n' +
            'Version ' + VERSION + '\n' +
            'Copyright (c) 2019, Armstrong Monitoring Corporation, All Rights Reserved. '
        )

        messagebox.showinfo("About", msgStr)

    def approvalCheckCmd(self):
        total = 0
        for var in self.approvalCheckVars:
            total = total + var.get()

        allTheStepsAreApproved = len(self.approvalCheckVars) == total
        theDiagramIsApproved   = 1 == self.diagramAppvlCheckVar.get()
        theFilenameMatchsThePattern = VERMILLION != self.statusBar.cget('bg')

        if allTheStepsAreApproved and theDiagramIsApproved and theFilenameMatchsThePattern:
            self.salesApprovedButton.config(state='normal')
        else:
            self.salesApprovedButton.config(state='disabled')
    
    def salesApprovedButtonCmd(self):
        #Update metadata

        self.instructionsJSON['salesQC'] = getpass.getuser()
        self.salesQC.config(text=self.instructionsJSON['salesQC'])

        now = datetime.datetime.now()
        self.instructionsJSON['approvalDate'] = now.strftime(DATE_FORMAT_STR)
        self.approvalDate.config(text=self.instructionsJSON['approvalDate'])
        
        
        self.saveInstructionsJSON()
            
        self.statusBar.config(text='JSON Saved')

    def systemTestButtonCmd(self):

        row_index   = 0
        buttonValue = False
        failFlag    = False

        #Reset test results
        """ for index in range(len(self.relayTestResults)):
            self.relayTestResults[index].config(text='', bg=self.defaultbg)
            self.analogTestResults[index].config(text='', bg=self.defaultbg) """
        for relayResultLabel, analogResultLabel in zip(self.relayTestResults, self.analogTestResults):
            relayResultLabel.config(text='', bg=self.defaultbg)
            analogResultLabel.config(text='', bg=self.defaultbg)

        thereAreStillStepsToProcess = row_index < len(self.instructionsJSON['steps'])
        theSystemTestWasNotAborted = failFlag is not None

        while thereAreStillStepsToProcess and theSystemTestWasNotAborted:
            #Propmt the tech to simulate a signal on one sensor
            buttonValue = self.simulationPopup(row_index)
            
            #Prompt the tech to test some relays
            if buttonValue:
                buttonValue = self.relayCheckPopup(row_index)

                if buttonValue:
                    self.relayTestResults[row_index].config(text='relay pass', bg=BLUE_GREEN)
                elif buttonValue is None:
                    self.relayTestResults[row_index].config(text='ABORTED', bg=YELLOW)
                    failFlag = None
                elif not buttonValue:
                    self.relayTestResults[row_index].config(text='relay fail', bg=VERMILLION)
                    failFlag = True
                else:
                    pass
            else:
                failFlag = None
                self.relayTestResults[row_index].config(text='ABORTED', bg=YELLOW)

            #Propmt the tech to test some analog outputs
            
            theSystemTestWasNotAborted = failFlag is not None
            thereAreValuesInAnalogOutList = len(self.instructionsJSON['steps'][row_index]['analogOutList']) != 0

            if theSystemTestWasNotAborted and thereAreValuesInAnalogOutList:
                buttonValue = self.analogCheckPopup(row_index)

                if buttonValue:
                    self.analogTestResults[row_index].config(text='analog pass', bg=BLUE_GREEN)
                elif buttonValue is None:
                    self.analogTestResults[row_index].config(text='ABORTED', bg=YELLOW)
                    failFlag = None
                elif not buttonValue:
                    self.analogTestResults[row_index].config(text='analog fail', bg=VERMILLION)
                    failFlag = True
                else:
                    pass             
            row_index = row_index + 1
            thereAreStillStepsToProcess = row_index < len(self.instructionsJSON['steps'])
            theSystemTestWasNotAborted = failFlag is not None

        
        #Update Status bar
        if failFlag:
            self.statusBar.config(text='System Test Failed', bg=VERMILLION)
        elif failFlag is None:
            self.statusBar.config(text='System Test Aborted', bg=YELLOW)
        elif not failFlag:
            self.instructionsJSON['sysTestDate'] = datetime.datetime.now().strftime(DATE_FORMAT_STR)
            self.sysTestDate.config(text=self.instructionsJSON['sysTestDate'])

            self.instructionsJSON['mfgQC'] = getpass.getuser()
            self.mfgQC.config(text=self.instructionsJSON['mfgQC'])
            
            self.statusBar.config(text='System Test Passed', bg=BLUE_GREEN)
            self.saveInstructionsJSON()    
        else:
            pass
        
        self.saveTestResults(failFlag)

#Constructor Helpers

    def createPermanentLables(self):
        tk.Label(self.filenameFrame,     text='Filename:').grid(row=0, sticky='w')
        tk.Label(self.salesApprovalFrame, text='Date:').grid(row=0, sticky='w')
        tk.Label(self.salesApprovalFrame, text='Sales QC:').grid(row=1, sticky='w')
        tk.Label(self.systemTestFrame,    text='Date:').grid(row=0, sticky='w')
        tk.Label(self.systemTestFrame,    text='Manufacturing QC:').grid(row=1, sticky='w')
        tk.Label(self.salesApprovalFrame, text='Approve Wiring Diagram:').grid(row=2, sticky='w')

        COLOR_TEST = False
        if COLOR_TEST:
            tk.Label(self.filenameFrame, text=' ', bg=BLACK).grid(row=0, column=1)
            tk.Label(self.filenameFrame, text=' ', bg=ORANGE).grid(row=0, column=2)
            tk.Label(self.filenameFrame, text=' ', bg=SKY_BLUE).grid(row=0, column=3)
            tk.Label(self.filenameFrame, text=' ', bg=BLUE_GREEN).grid(row=0, column=4)
            tk.Label(self.filenameFrame, text=' ', bg=YELLOW).grid(row=0, column=5)
            tk.Label(self.filenameFrame, text=' ', bg=BLUE).grid(row=0, column=6)
            tk.Label(self.filenameFrame, text=' ', bg=VERMILLION).grid(row=0, column=7)
            tk.Label(self.filenameFrame, text=' ', bg=PURPLE).grid(row=0, column=8)
            tk.Label(self.filenameFrame, text=' ', bg=WHITE).grid(row=0, column=9)

    def layoutWindow(self):
        self.filenameFrame.grid(row=0, columnspan=3, sticky='w')
        self.mfgMetaFrame.grid(row=0, column=1, sticky='w')
        self.salesApprovalFrame.grid(row=1, column=0, sticky="w")
        self.systemTestFrame.grid(row=1, column=1, sticky='w')
        self.statusFrame.grid(row=1, column=2, sticky='w')
        self.instructionFrame.grid(row=2, columnspan=3, sticky="w")
        
        self.diagramAppvlCheck.grid(row=2, column=1, sticky='w')
        self.salesApprovedButton.grid(row=3, columnspan=2, sticky='w'+'e')


        self.systemTestButton.grid(row=2, columnspan=2, sticky='w'+'e')

        self.statusBar.grid(row=0)

        #self.wiringDiagramCanvas.pack()

#fileMenuCmd Helpers
    def resetUIWidgets(self):
        self.approvalCheckList = []
        self.approvalCheckVars = []
        self.relayTestResults  = []
        self.analogTestResults = []
        self.instructionHeaders_ADF = []
        self.instructionHeaders_CSV = []
        self.instructionList_ADF = []
        self.instructionList_CSV = []
        self.SOnumber = 'NA'

        if self.instructionFrame.winfo_children() != []:
            for child in self.instructionFrame.winfo_children():
                child.destroy()

    def getFilenameDialog(self) -> str:
        filename = filedialog.askopenfilename(title='Select configuration database', filetypes= (("ADF files","*.adf"),("all files","*.*")))

        #Test filename for pattern
        fileNamePattern = re.compile(FILENAME_PATTERN_RE)
        
        if fileNamePattern.match(filename) is None:
            self.statusBar.config(text='Filename does not match pattern.', bg=VERMILLION, fg=WHITE)
        else:
            self.statusBar.config(text='', bg=self.defaultbg, fg=BLACK)
            path = Path(filename)
            self.SOnumber = re.findall('SO[0-9]{5}', path.name)[0]

        return filename
    
    def createInstructionsJSON(self):
        #Create a new set of instructions from ADF file
        self.adf2DiaFSM = adf2diagram.adfToDiagramFSM(adf2diagram.readFileState(), self.ADFfilename.cget('text'))
        self.adf2DiaFSM.runAll()
        self.instructionsJSON = self.adf2DiaFSM.stateVars.instructionJSONDict

        for __ in self.instructionsJSON['steps']:
            self.approvalCheckVars.append(None)
            self.approvalCheckVars[-1] = tk.IntVar()
            self.approvalCheckList.append(tk.Checkbutton(self.instructionFrame, 
                                                              text='Correct',
                                                              command=self.approvalCheckCmd,
                                                              variable=self.approvalCheckVars[-1]))
            self.relayTestResults.append(tk.Label(self.instructionFrame))
            self.analogTestResults.append(tk.Label(self.instructionFrame))

        #Set all metadata to NA
        self.approvalDate.config(text='NA')
        self.salesQC.config(text='NA')
        self.mfgQC.config(text='NA')
        self.sysTestDate.config(text='NA')

    def loadJSON(self, JSONfile):
        self.archiveInstructionsJSON = json.load(JSONfile)

    def lockSalesMode(self):
        self.salesApprovedButton.config(state='disabled')
        self.diagramAppvlCheck.select()
        self.diagramAppvlCheck.config(state='disabled')
        for check in self.approvalCheckList:
            check.select()
            check.config(state='disabled')

    def defaultSalesMode(self):
        self.salesApprovedButton.config(state="disable")
        self.diagramAppvlCheck.config(state='normal')

    def unlockMfgMode(self):
        self.systemTestButton.config(state='normal')

    def displayMetadata(self):
        self.ADFfilename.grid(row=0, column = 1, sticky='w')

        self.approvalDate.grid(row=0, column = 1, sticky='w')
        self.salesQC.grid(row=1, column = 1, sticky='w')

        self.sysTestDate.grid(row=0, column=1, sticky='w')
        self.mfgQC.grid(row=1, column=1, sticky='w')       
    
    def displayInstructions(self):
        #Clear the instruction grid
        for slave in self.instructionFrame.grid_slaves():
            slave.grid_forget()

        #Display instructiond headers
        col_count = 0
        for header in self.instructionsJSON['stepHeader']:
            tk.Label(self.instructionFrame, text=header, relief='groove').grid(row=0, sticky=tk.W+tk.E, column=col_count)
            col_count = col_count + 1
        tk.Label(self.instructionFrame, text='Sales Approval', relief='groove').grid(row=0, sticky=tk.W+tk.E, column=len(self.instructionsJSON['stepHeader']))

        #Display instructions
        row_count = 1

        for instruction in self.instructionsJSON['steps']:
            
            cell = ', '.join(instruction['sensorList'])
            #cell = ',\n'.join(instruction['sensorList'])
            cell = '\n'.join(textwrap.wrap(cell, 32))
            tk.Label(self.instructionFrame, text=cell, relief=tk.SUNKEN, justify=tk.LEFT).grid(row=row_count, column=0, sticky=tk.W+tk.E)

            cell = ', '.join(instruction['signalSim'])
            #cell = ',\n'.join(instruction['signalSim'])
            cell = '\n'.join(textwrap.wrap(cell, 32))
            tk.Label(self.instructionFrame, text=cell, relief=tk.SUNKEN, justify=tk.LEFT).grid(row=row_count, column=1, sticky=tk.W+tk.E)

            cell = ', '.join(instruction['relayList'])
            #cell = ',\n'.join(instruction['relayList'])
            cell = '\n'.join(textwrap.wrap(cell, 32))
            tk.Label(self.instructionFrame, text=cell, relief=tk.SUNKEN, justify=tk.LEFT).grid(row=row_count, column=2, sticky=tk.W+tk.E)

            cell = ', '.join(instruction['analogOutList'])
            #cell = ',\n'.join(instruction['analogOutList'])
            cell = '\n'.join(textwrap.wrap(cell, 32))
            tk.Label(self.instructionFrame, text=cell, relief=tk.SUNKEN, justify=tk.LEFT).grid(row=row_count, column=3, sticky=tk.W+tk.E)

            cell = ', '.join(instruction['analogCurrent'])
            #cell = ',\n'.join(instruction['analogCurrent'])
            cell = '\n'.join(textwrap.wrap(cell, 32))
            tk.Label(self.instructionFrame, text=cell, relief=tk.SUNKEN, justify=tk.LEFT).grid(row=row_count, column=4, sticky=tk.W+tk.E)

            row_count = row_count+1

        #Display sales appvoal check boxes
        for index in range(len(self.approvalCheckList)):
            self.approvalCheckList[index].grid(row=index + 1, column=len(self.instructionsJSON['stepHeader']), sticky='w')

        for index in range(len(self.relayTestResults)):
            self.relayTestResults[index].grid(row=index+1, column=len(self.instructionsJSON['stepHeader'])+1, sticky='w')
            self.analogTestResults[index].grid(row=index+1, column=len(self.instructionsJSON['stepHeader'])+2, sticky='w')

        #Update canvas

    def displayWiringDiagram(self):
        from PIL import ImageTk, Image
        
        self.wiringDiagramCanvas.destroy()

        png_filename = self.ADFfilename.cget('text')[:-4] + '.png'
        self.wiringDiagram = ImageTk.PhotoImage(Image.open(png_filename)) 
        
        width = self.wiringDiagram.width()
        height = self.wiringDiagram.height()

        if width > (screensize[0] * 0.6):
            windowWidth = screensize[0] * 0.6
        else:
            windowWidth = width

        if height > (screensize[1] * 0.6):
            windowHeight = screensize[1] * 0.6
        else:
            windowHeight = height

        dims = str(int(windowWidth)) + 'x' + str(int(windowHeight))

        self.popup.geometry(dims)
        
        self.wiringDiagramCanvas = tk.Canvas(self.wiringDiagramFrame, bd=0,
                                            scrollregion=(0, 0, width, height), 
                                            width = width, 
                                            height = height,
                                            xscrollcommand=self.xscrollbar.set,
                                            yscrollcommand=self.yscrollbar.set) 

        self.wiringDiagramCanvas.grid(row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)

        self.xscrollbar.config(command=self.wiringDiagramCanvas.xview)
        self.yscrollbar.config(command=self.wiringDiagramCanvas.yview)

        self.wiringDiagramCanvas.create_image(0, 0, anchor=tk.NW, image=self.wiringDiagram)
        self.wiringDiagramCanvas.image = self.wiringDiagram  
        #root.mainloop() 

#salesApprovaedButtonCmd Helpers

    def saveInstructionsJSON(self):
        filename = self.ADFfilename.cget('text')

        #Create new directory
        path = Path(filename)
        new_dir_name = re.findall('SO[0-9]{5}', path.name)[0]
        if new_dir_name in path.parts:
            new_filename = filename
        else:
            name_index = filename.find(path.name)
            new_path = filename[:(name_index)] + new_dir_name
            new_filename = new_path + '\\' + path.name
            try:
                os.mkdir(new_path)
            except FileExistsError:
                pass
            try:
                os.rename(filename, new_filename)
            except:
                pass
            try:
                os.rename(filename[:-4] + '.puml', new_filename[:-4] + '.puml')
            except:
                pass
            try:
                os.rename(filename[:-4] + '.png', new_filename[:-4] + '.png')
            except:
                pass

        #Open JSON file
        with open(new_filename[:-4] + '.json', 'w') as JSONfile:
            json.dump(self.instructionsJSON, JSONfile, indent=4) 
            self.salesApprovedButton.config(state='disabled')
            self.diagramAppvlCheck.config(state='disabled')
            for index in range(len(self.approvalCheckList)):
                self.approvalCheckList[index].config(state='disabled')

    def loadOutputStr(self) -> str:
        outputStr = [
            ['salesQC',         self.salesQC.cget('text')],
            ['approvalDate',    self.approvalDate.cget('text')],
            ['sysTestDate',     self.sysTestDate.cget('text')],
            ['mfgQC',           self.mfgQC.cget('text')],
            ['stepHeader',      *self.instructionHeaders_ADF]
        ]
        for row in self.instructionList_ADF:
            outputStr.append(['steps', *row])
        for index in range(len(outputStr)):
            outputStr[index] = ','.join(outputStr[index])
        outputStr = '\n'.join(outputStr)
        return outputStr

#systemTestButtonCmd Helpers
    def simulationPopup(self, row_index):
        row = self.instructionsJSON['steps'][row_index]

        rowStr = row['sensorList'][0]
        #rowStr = ',\n'.join(row['sensorList'])
        #rowStr = '\n'.join(textwrap.wrap(rowStr, 32))

        message_string = (
            self.instructionsJSON['stepHeader'][0] + '\n' + 
            rowStr + '\n' + 
            self.instructionsJSON['stepHeader'][1] + '\n' + 
            row['signalSim'][0]
        )
        return messagebox.askokcancel('Simulate Signal', message_string)

    def relayCheckPopup(self, row_index):
        row = self.instructionsJSON['steps'][row_index]

        #rowStr = ',\n'.join(row['relayList'])
        rowStr = ', '.join(row['relayList'])
        rowStr = '\n'.join(textwrap.wrap(rowStr, 32))

        message_string = (
            self.instructionsJSON['stepHeader'][2] + '\n' + 
            rowStr
        )
        return messagebox.askyesnocancel('Check Relays', message_string)

    def analogCheckPopup(self, row_index):
        row = self.instructionsJSON['steps'][row_index]

        rowStrs = []
        rowStrs.append(',\n'.join(row['analogOutList']))
        #rowStrs[-1] = '\n'.join(textwrap.wrap(rowStrs[-1], 32))

        rowStrs.append(',\n'.join(row['analogCurrent']))
        #rowStrs[-1] = '\n'.join(textwrap.wrap(rowStrs[-1], 32))
            
        message_string = (
            self.instructionsJSON['stepHeader'][3] + '\n' + 
            rowStrs[0] + '\n' + 
            self.instructionsJSON['stepHeader'][4] + '\n' + 
            rowStrs[1]
        )
        return messagebox.askyesnocancel('Check Analog Out', message_string)

    def saveTestResults(self, testStatus):
        
        now = datetime.datetime.now()
        datatime_str = now.strftime('%Y_%m_%d_%H%Mh')
        filename = self.ADFfilename.cget('text')
        path = Path(filename)
        name_index = filename.find(path.name)
        new_path = filename[:(name_index)]
        timestappedFilename = new_path + self.SOnumber + '_' + datatime_str

        if testStatus:
            statusDict = {'testStatus' : 'Fail'}
            timestappedFilename = timestappedFilename + '_FAIL.json'
        elif testStatus is None:
            statusDict = {'testStatus' : 'Abort'}
            timestappedFilename = timestappedFilename + '_ABORT.json'
        elif not testStatus:
            statusDict = {'testStatus' : 'Pass'}
            timestappedFilename = timestappedFilename + '_PASS.json'
        else:
            pass

        with open(timestappedFilename, 'w') as testResultsFile:

            import copy

            testRecordJSON = {**statusDict, **copy.deepcopy(self.instructionsJSON)}
            testRecordJSON['sysTestDate'] = now.strftime("%Y-%m-%d")
            testRecordJSON['mfgQC'] = getpass.getuser()

            for step, relayResultLabel, analogResultLabel in zip(testRecordJSON['steps'], self.relayTestResults, self.analogTestResults):
                step.update({'Relay Test'  : relayResultLabel.cget('text')})
                step.update({'Analog Test' : analogResultLabel.cget('text')})

            json.dump(testRecordJSON, testResultsFile, indent=4)

if __name__ == '__main__':

    root = tk.Tk()
    mainWindow = mainWindowClass(root)
    root.iconbitmap(bitmap=r'favicon.ico')
    mainWindow.popup.iconbitmap(bitmap=r'favicon.ico')
    windowWidth = root.winfo_reqwidth()
    windowHeight = root.winfo_reqheight()
    positionRight = int(root.winfo_screenwidth()/2 - windowWidth/2)
    positionDown = int(root.winfo_screenheight()/2 - windowHeight/2)
    root.geometry("+{}+{}".format(positionRight, positionDown))

    root.mainloop()
    