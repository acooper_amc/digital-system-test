from setuptools import setup

setup(
    name='ADFValidationTool',
    version='0.1',
    packages=['src',],
    install_requires=[
        'plantuml',
        'httplib2',
    ],
)