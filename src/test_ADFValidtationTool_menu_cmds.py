import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import ADFValidationTool as tool
import tkinter as tk
from tkinter import filedialog
import re
from pathlib import Path
from PIL import ImageTk, Image

import json

import adf2diagram

mock = unittest.mock.MagicMock()

#Cheange workign directory
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

test_adf_text = ''
test_instructionList = []

test_filename = 'c:\\somedirectory\\somefile.adf'
demo_adf_filename_noSO  = '..\\test\\demo_DB.adf'
demo_csv_filename_noSO  = '..\\test\\demo_DB.csv'
demo_JSON_filename_noSO  = '..\\test\\demo_DB.json'
demo_adf_filename       = '..\\test\\SO12345_demo_DB.adf'

class Test_file_cmd_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

    #TODO Fix
    def fileCmd_calls_all_methods_if_the_file_is_found(self):
        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(tool.mainWindowClass, 'getFilenameDialog', return_value='test_filename.adf') as _mock_getFilenameDialog,\
            patch.object(tool.mainWindowClass, 'resetUIWidgets', return_value=None) as _mock_resetWidgets,\
            patch.object(tool.mainWindowClass, 'createInstructionsJSON', return_value=None) as _mock_createInstructionsJSON,\
            patch.object(tool.mainWindowClass, 'loadJSON', return_value=None) as _mock_loadJSON,\
            patch.object(tool.mainWindowClass, 'lockSalesMode', return_value=None) as _mock_lockSalesMode,\
            patch.object(tool.mainWindowClass, 'unlockMfgMode', return_value=None) as _mock_unlockMfgMode,\
            patch.object(tool.mainWindowClass, 'displayMetadata', return_value=None) as _mock_displayMetadata,\
            patch.object(tool.mainWindowClass, 'displayInstructions', return_value=None) as _mock_displayInstructions,\
            patch.object(tool.mainWindowClass, 'displayWiringDiagram', return_value=None) as _mock_displayWiringDiagram,\
            patch.object(tool.mainWindowClass, 'defaultSalesMode', return_value=None) as _mock_defaultSalesMode:

                self.mainWindow_instance.fileMenuCmd()

                _mock_resetWidgets.assert_called_once()
                _mock_getFilenameDialog.assert_called_once()
                _mock_resetWidgets.assert_called_once()
                _mock_createInstructionsJSON.assert_called_once()
                _mock_loadJSON.assert_called_once()
                _mock_lockSalesMode.assert_called_once()
                _mock_unlockMfgMode.assert_called_once()
                _mock_displayMetadata.assert_called_once()
                _mock_displayInstructions.assert_called_once()
                _mock_displayWiringDiagram.assert_called_once()
                _mock_defaultSalesMode.assert_called_once()
    
    def test_fileCmd_skips_methods_of_no_file_is_found(self):
        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(tool.mainWindowClass, 'getFilenameDialog', return_value='test_filename.adf') as _mock_getFilenameDialog,\
            patch.object(tool.mainWindowClass, 'resetUIWidgets', return_value=None) as _mock_resetWidgets,\
            patch.object(tool.mainWindowClass, 'createInstructionsJSON', return_value=None) as _mock_createInstructionsJSON,\
            patch.object(tool.mainWindowClass, 'loadJSON', return_value=None) as _mock_loadJSON,\
            patch.object(tool.mainWindowClass, 'lockSalesMode', return_value=None) as _mock_lockSalesMode,\
            patch.object(tool.mainWindowClass, 'unlockMfgMode', return_value=None) as _mock_unlockMfgMode,\
            patch.object(tool.mainWindowClass, 'displayMetadata', return_value=None) as _mock_displayMetadata,\
            patch.object(tool.mainWindowClass, 'displayInstructions', return_value=None) as _mock_displayInstructions,\
            patch.object(tool.mainWindowClass, 'displayWiringDiagram', return_value=None) as _mock_displayWiringDiagram,\
            patch.object(tool.mainWindowClass, 'defaultSalesMode', return_value=None) as _mock_defaultSalesMode:

                _file.side_effect = IOError()
                
                self.mainWindow_instance.fileMenuCmd()

                _mock_resetWidgets.assert_called_once()
                _mock_getFilenameDialog.assert_called_once()
                _mock_resetWidgets.assert_called_once()
                _mock_createInstructionsJSON.assert_called_once()
                _mock_loadJSON.assert_not_called()
                _mock_lockSalesMode.assert_not_called()
                _mock_unlockMfgMode.assert_not_called()
                _mock_displayMetadata.assert_called_once()
                _mock_displayInstructions.assert_called_once()
                _mock_displayWiringDiagram.assert_called_once()
                _mock_defaultSalesMode.assert_called_once()



if __name__ == "__main__":
    unittest.main()