import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys

import ADFobjects
import UMLObjects

import adf2diagram

mock = unittest.mock.MagicMock()

test_relay_objects = [
    ADFobjects.RelayV4('0','1','0','10','0','1','0','1','0','0','0'),
    ADFobjects.RelayV4('1','1','0','10','0','1','0','1','1','0','0'),
    ADFobjects.RelayV4('2','1','0','10','0','1','0','1','2','0','0'),
    ADFobjects.RelayV4('3','1','0','10','1','1','0','1','3','0','0'),
    ADFobjects.RelayV4('4','1','0','10','0','1','0','1','4','0','0'),
    ADFobjects.RelayV4('5','1','0','10','0','1','0','1','5','0','0'),
    ADFobjects.RelayV4('6','1','0','10','0','1','0','1','6','0','0'),
    ADFobjects.RelayV4('7','1','0','10','1','1','0','1','7','0','0'),
    ADFobjects.RelayV4('8','1','0','10','0','1','2','102','0','0','5'),
    ADFobjects.RelayV4('9','1','0','10','0','1','2','102','1','0','5'),
    ADFobjects.RelayV4('10','1','0','10','0','1','2','102','2','0','5'),
    ADFobjects.RelayV4('11','1','0','10','1','1','2','102','3','0','5'),
    ADFobjects.RelayV4('12','1','0','10','0','1','2','102','4','0','5'),
    ADFobjects.RelayV4('13','1','0','10','0','1','2','102','5','0','5'),
    ADFobjects.RelayV4('14','1','0','10','0','1','2','102','6','0','5'),
    ADFobjects.RelayV4('15','1','0','10','1','1','2','102','7','0','5')
]

TOTAL_LOCAL_RELAYS = 8
TOTAL_REMOTE_RELAYS = 8
RELAYS_ON_INTERFACE_1 = 0
RELAYS_ON_INTERFACE_2 = 8
RELAYS_ON_INTERFACE_3 = 0
RELAYS_ON_INTERFACE_4 = 0


test_analogOut_objects = [
    ADFobjects.AnalogOutV4('0','1','0','1','0','0','10','1','60','0'),
    ADFobjects.AnalogOutV4('1','1','0','1','1','0','10','0','60','0'),
    ADFobjects.AnalogOutV4('2','1','0','1','2','1','10','1','60','0'),
    ADFobjects.AnalogOutV4('3','1','0','1','3','1','10','0','60','0'),
    ADFobjects.AnalogOutV4('4','1','1','101','0','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('5','1','1','101','1','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('6','1','1','101','2','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('7','1','1','101','3','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('8','1','1','101','4','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('9','1','1','101','5','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('10','1','1','101','6','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('11','1','1','101','7','0','10','1','60','6')
]

TOTAL_LOCAL_ANALOG_OUT = 4
TOTAL_REMOTE_ANALOG_OUT = 8
ANALOG_OUT_ON_INTERFACE_1 = 8
ANALOG_OUT_ON_INTERFACE_2 = 0
ANALOG_OUT_ON_INTERFACE_3 = 0
ANALOG_OUT_ON_INTERFACE_4 = 0


test_sensor_objects = [
    ADFobjects.SensorV4('0','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','1','0','1'),
    ADFobjects.SensorV4('1','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','2','0','1'),
    ADFobjects.SensorV4('2','12','0','100','0','1','2','4','10','0','5','30','0','','','','7','20','0','','1','1','3','0','1'),
    ADFobjects.SensorV4('3','12','0','100','0','1','2','4','10','0','5','30','0','','','','7','20','0','','1','1','4','0','1'),
    ADFobjects.SensorV4('4','1','0','50','0','1','3','8','10','0','9','20','0','10','75','0','11','20','0','','1','1','5','0','3'),
    ADFobjects.SensorV4('5','1','0','50','0','1','3','8','10','0','9','20','0','10','75','0','11','20','0','','1','1','6','1','3'),
    ADFobjects.SensorV4('6','24','0','50000','0','1','3','8','20','0','9','100','0','10','75','0','11','20','0','','1','2','101','2','10'),
    ADFobjects.SensorV4('7','24','0','50000','0','1','3','8','20','0','9','100','0','10','75','0','11','20','0','','1','2','101','3','10'),
    ADFobjects.SensorV4('8','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','4','10'),
    ADFobjects.SensorV4('9','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','5','10'),
    ADFobjects.SensorV4('10','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','6','10'),
    ADFobjects.SensorV4('11','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','7','10')
]

SENSORS_ON_INTERFACE_1 = 4
SENSORS_ON_INTERFACE_2 = 0
SENSORS_ON_INTERFACE_3 = 0
SENSORS_ON_INTERFACE_4 = 0

ANALOG_IN_ON_INTERFACE_1 = 0
ANALOG_IN_ON_INTERFACE_2 = 4
ANALOG_IN_ON_INTERFACE_3 = 4
ANALOG_IN_ON_INTERFACE_4 = 0




class Test_State_class_next_methods(unittest.TestCase):

    def test_creatUMLObjectsState_advance_to_wrtieFileState(self):
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        self.assertTrue(isinstance(createUMLObjectsState_instance.next(), adf2diagram.writeFileState))
            
class Test_State_class_run_methods(unittest.TestCase):

    def test_createUMLObjectState_run_adds_relays_to_monitor_object(self):
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFRelayList = test_relay_objects

        test_relay_dict = {
            '0' : '0',
            '1' : '1',
            '2' : '2',
            '3' : '3',
            '4' : '4',
            '5' : '5',
            '6' : '6',
            '7' : '7'
        }

        createUMLObjectsState_instance.run(stateVars_instance)
        self.assertEqual(test_relay_dict, stateVars_instance.UMLmonitor.localRelays)
    
    def test_createUMLObjectState_run_adds_analogOut_to_monitor_object(self):
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFRelayList = test_relay_objects
        stateVars_instance.ADFAnalogOutList = test_analogOut_objects

        test_analogOut_dict = {
            '0' : '0',
            '1' : '1',
            '2' : '2',
            '3' : '3'
        }

        createUMLObjectsState_instance.run(stateVars_instance)
        self.assertEqual(test_analogOut_dict, stateVars_instance.UMLmonitor.localAnalogOut)


    def test_creatUMLObjectsState_run_groups_sensors_by_MODBUS_interface(self):
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFRelayList = test_relay_objects
        stateVars_instance.ADFAnalogOutList = test_analogOut_objects
        stateVars_instance.ADFSensorList = test_sensor_objects

        test_transmitter_MODBUS_ID = ['1']
        test_tramsmitter_sensors_dict = [{
            '0' : {'address' : '1', 'gas label': 'CO', 'location': '', 'hardware' : '1'},
            '1' : {'address' : '2', 'gas label': 'CO', 'location': '', 'hardware' : '1'},
            '2' : {'address' : '3', 'gas label': 'NO2', 'location': '', 'hardware' : '1'},
            '3' : {'address' : '4', 'gas label': 'NO2', 'location': '', 'hardware' : '1'},
            '4' : {'address' : '5', 'gas label': 'Cl2', 'location': '', 'hardware' : '3', 'instance' : '0'},
            '5' : {'address' : '6', 'gas label': 'Cl2', 'location': '', 'hardware' : '3', 'instance' : '1'}
        }]

        createUMLObjectsState_instance.run(stateVars_instance)

        self.assertEqual(test_transmitter_MODBUS_ID[0], stateVars_instance.UMLDigitalTransmitterList[0].MODBUS_ID )
        self.assertEqual(test_tramsmitter_sensors_dict[0], stateVars_instance.UMLDigitalTransmitterList[0].sensors)

    def test_creatUMLObjectsState_run_groups_relays_by_MODBUS_interface(self):
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFRelayList = test_relay_objects
        stateVars_instance.ADFAnalogOutList = test_analogOut_objects
        stateVars_instance.ADFSensorList = test_sensor_objects

        test_module_MODBUS_ID   = ['2']
        test_module_address     = ['102']
        test_module_relays_dict = [{
            '8'  : '0' ,
            '9'  : '1',
            '10' : '2',
            '11' : '3',
            '12' : '4',
            '13' : '5',
            '14' : '6',
            '15' : '7'
        }]

        createUMLObjectsState_instance.run(stateVars_instance)

        self.assertEqual(test_module_MODBUS_ID[0], stateVars_instance.UMLRelayModuleList[0].MODBUS_ID)
        self.assertEqual(test_module_address[0], stateVars_instance.UMLRelayModuleList[0].address)
        print(stateVars_instance.UMLRelayModuleList[0].relays)
        self.assertEqual(test_module_relays_dict[0], stateVars_instance.UMLRelayModuleList[0].relays)
    
    def test_creatUMLObjectsState_run_groups_analogInModules_by_MODBUS_interface(self): 
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFRelayList = test_relay_objects
        stateVars_instance.ADFAnalogOutList = test_analogOut_objects
        stateVars_instance.ADFSensorList = test_sensor_objects

        test_module_MODBUS_ID   = ['2', '3']
        test_module_address     = ['101', '101']
        test_module_sensors_dict = [
            {
                '6'  : {'instance' : '2', 'gas label': 'CO2', 'location': ''},
                '7'  : {'instance' : '3', 'gas label': 'CO2', 'location': ''}
            },
            {
                '8'   : {'instance' : '4', 'gas label': 'SO2', 'location': ''},
                '9'   : {'instance' : '5', 'gas label': 'SO2', 'location': ''},
                '10'  : {'instance' : '6', 'gas label': 'SO2', 'location': ''},
                '11'  : {'instance' : '7', 'gas label': 'SO2', 'location': ''}

            }]

        createUMLObjectsState_instance.run(stateVars_instance)

        self.assertEqual(test_module_MODBUS_ID[0], stateVars_instance.UMLAnalogInModuleList[0].MODBUS_ID)
        self.assertEqual(test_module_address[0], stateVars_instance.UMLAnalogInModuleList[0].address)
        self.assertEqual(test_module_sensors_dict[0], stateVars_instance.UMLAnalogInModuleList[0].sensors)

        self.assertEqual(test_module_MODBUS_ID[1], stateVars_instance.UMLAnalogInModuleList[1].MODBUS_ID)
        self.assertEqual(test_module_address[1], stateVars_instance.UMLAnalogInModuleList[1].address)
        self.assertEqual(test_module_sensors_dict[1], stateVars_instance.UMLAnalogInModuleList[1].sensors) 
      
    def test_creatUMLObjectsState_run_groups_analogOutModules_by_MODBUS_interface(self): 
        createUMLObjectsState_instance = adf2diagram.createUMLObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFRelayList = test_relay_objects
        stateVars_instance.ADFAnalogOutList = test_analogOut_objects
        stateVars_instance.ADFSensorList = test_sensor_objects

        test_module_MODBUS_ID   = ['1']
        test_module_address     = ['101']
        test_module_outputs_dict = [
            {
                '4'   : '0',
                '5'   : '1',
                '6'   : '2',
                '7'   : '3',
                '8'   : '4',
                '9'   : '5',
                '10'  : '6',
                '11'  : '7'
            }]

        createUMLObjectsState_instance.run(stateVars_instance)
        
        self.assertEqual(test_module_MODBUS_ID[0], stateVars_instance.UMLAnalogOutModuleList[0].MODBUS_ID)
        self.assertEqual(test_module_address[0], stateVars_instance.UMLAnalogOutModuleList[0].address)
        self.assertEqual(test_module_outputs_dict[0], stateVars_instance.UMLAnalogOutModuleList[0].outputs)

if __name__ == "__main__":
    unittest.main()


        


