import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys
import datetime
import ADFobjects
import adf2diagram
import getpass

mock = unittest.mock.MagicMock()

test_relay_objects = [
    ADFobjects.RelayV4('0','1','0','10','0','1','0','1','0','0','0'),
    ADFobjects.RelayV4('1','1','0','10','0','1','0','1','1','0','0'),
    ADFobjects.RelayV4('2','1','0','10','0','1','0','1','2','0','0'),
    ADFobjects.RelayV4('3','1','0','10','1','1','0','1','3','0','0'),
    ADFobjects.RelayV4('4','1','0','10','0','1','0','1','4','0','0'),
    ADFobjects.RelayV4('5','1','0','10','0','1','0','1','5','0','0'),
    ADFobjects.RelayV4('6','1','0','10','0','1','0','1','6','0','0'),
    ADFobjects.RelayV4('7','1','0','10','1','1','0','1','7','0','0'),
    ADFobjects.RelayV4('8','1','0','10','0','1','2','102','0','0','1'),
    ADFobjects.RelayV4('9','1','0','10','0','1','2','102','1','0','1'),
    ADFobjects.RelayV4('10','1','0','10','0','1','2','102','2','0','1'),
    ADFobjects.RelayV4('11','1','0','10','1','1','2','102','3','0','1'),
    ADFobjects.RelayV4('12','1','0','10','0','1','2','102','4','0','1'),
    ADFobjects.RelayV4('13','1','0','10','0','1','2','102','5','0','1'),
    ADFobjects.RelayV4('14','1','0','10','0','1','2','102','6','0','1'),
    ADFobjects.RelayV4('15','1','0','10','1','1','2','102','7','0','1')
]

TOTAL_LOCAL_RELAYS = 8
TOTAL_REMOTE_RELAYS = 8
RELAYS_ON_INTERFACE_1 = 0
RELAYS_ON_INTERFACE_2 = 8
RELAYS_ON_INTERFACE_3 = 0
RELAYS_ON_INTERFACE_4 = 0


test_analogOut_objects = [
    ADFobjects.AnalogOutV4('0','1','0','1','0','0','10','1','60','0'),
    ADFobjects.AnalogOutV4('1','1','0','1','1','0','10','0','60','0'),
    ADFobjects.AnalogOutV4('2','1','0','1','2','1','10','1','60','0'),
    ADFobjects.AnalogOutV4('3','1','0','1','3','1','10','0','60','0'),
    ADFobjects.AnalogOutV4('4','1','1','101','0','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('5','1','1','101','1','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('6','1','1','101','2','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('7','1','1','101','3','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('8','1','1','101','4','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('9','1','1','101','5','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('10','1','1','101','6','0','10','1','60','6'),
    ADFobjects.AnalogOutV4('11','1','1','101','7','0','10','1','60','6')
]

TOTAL_LOCAL_ANALOG_OUT = 4
TOTAL_REMOTE_ANALOG_OUT = 8
ANALOG_OUT_ON_INTERFACE_1 = 8
ANALOG_OUT_ON_INTERFACE_2 = 0
ANALOG_OUT_ON_INTERFACE_3 = 0
ANALOG_OUT_ON_INTERFACE_4 = 0


test_sensor_objects = [
    ADFobjects.SensorV4('0','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','1','0','0'),
    ADFobjects.SensorV4('1','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','2','0','0'),
    ADFobjects.SensorV4('2','12','0','100','0','1','2','4','10','0','5','30','0','','','','7','20','0','','1','1','3','0','0'),
    ADFobjects.SensorV4('3','12','0','100','0','1','2','4','10','0','5','30','0','','','','7','20','0','','1','1','4','0','0'),
    ADFobjects.SensorV4('4','1','0','50','0','1','3','8','10','0','9','20','0','10','75','0','11','20','0','','1','2','101','0','9'),
    ADFobjects.SensorV4('5','1','0','50','0','1','3','8','10','0','9','20','0','10','75','0','11','20','0','','1','2','101','1','9'),
    ADFobjects.SensorV4('6','24','0','50000','0','1','3','8','20','0','9','100','0','10','75','0','11','20','0','','1','2','101','2','9'),
    ADFobjects.SensorV4('7','24','0','50000','0','1','3','8','20','0','9','100','0','10','75','0','11','20','0','','1','2','101','3','9'),
    ADFobjects.SensorV4('8','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','4','9'),
    ADFobjects.SensorV4('9','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','5','9'),
    ADFobjects.SensorV4('10','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','6','9'),
    ADFobjects.SensorV4('11','14','0','100','0','1','3','12','20','0','13','50','0','14','75','0','15','20','0','','1','3','101','7','9')
]

SENSORS_ON_INTERFACE_1 = 4
SENSORS_ON_INTERFACE_2 = 0
SENSORS_ON_INTERFACE_3 = 0
SENSORS_ON_INTERFACE_4 = 0

ANALOG_IN_ON_INTERFACE_1 = 0
ANALOG_IN_ON_INTERFACE_2 = 4
ANALOG_IN_ON_INTERFACE_3 = 4
ANALOG_IN_ON_INTERFACE_4 = 0

test_zone_objects = [
    ADFobjects.ZoneV3('0','1','9','23','59','23','59','0','255','255','255','255','0'),
    ADFobjects.ZoneV3('1','1','9','23','59','23','59','0','255','255','255','255','1'),
    ADFobjects.ZoneV3('2','1','9','23','59','23','59','0','255','255','255','255','2'),
    ADFobjects.ZoneV3('3','1','9','23','59','23','59','0','255','255','255','255','3'),
    ADFobjects.ZoneV3('4','1','9','23','59','23','59','0','255','255','255','255','4'),
    ADFobjects.ZoneV3('5','1','9','23','59','23','59','0','255','255','255','255','5'),
    ADFobjects.ZoneV3('6','1','9','23','59','23','59','0','255','255','255','255','6'),
    ADFobjects.ZoneV3('7','1','9','23','59','23','59','0','255','255','255','255','7'),
    ADFobjects.ZoneV3('8','1','9','23','59','23','59','1','2','255','255','255','127'),
    ADFobjects.ZoneV3('9','1','9','23','59','23','59','1','255','6','255','255','127')
]

test_zone_sensor_objects = [
    ADFobjects.ZoneSensorV3('0','0','0'),
    ADFobjects.ZoneSensorV3('1','0','1'),
    ADFobjects.ZoneSensorV3('0','1','0'),
    ADFobjects.ZoneSensorV3('1','1','1'),
    ADFobjects.ZoneSensorV3('0','2','0'),
    ADFobjects.ZoneSensorV3('1','2','1'),
    ADFobjects.ZoneSensorV3('0','3','0'),
    ADFobjects.ZoneSensorV3('1','3','1'),
    ADFobjects.ZoneSensorV3('0','4','2'),
    ADFobjects.ZoneSensorV3('1','4','3'),
    ADFobjects.ZoneSensorV3('0','5','4'),
    ADFobjects.ZoneSensorV3('1','5','5'),
    ADFobjects.ZoneSensorV3('0','6','6'),
    ADFobjects.ZoneSensorV3('1','6','7'),
    ADFobjects.ZoneSensorV3('0','7','8'),
    ADFobjects.ZoneSensorV3('1','7','9'),
    ADFobjects.ZoneSensorV3('2','7','10'),
    ADFobjects.ZoneSensorV3('3','7','11'),
    ADFobjects.ZoneSensorV3('0','8','0'),
    ADFobjects.ZoneSensorV3('1','8','1'),
    ADFobjects.ZoneSensorV3('2','8','2'),
    ADFobjects.ZoneSensorV3('3','8','3'),
    ADFobjects.ZoneSensorV3('0','9','0'),
    ADFobjects.ZoneSensorV3('1','9','1'),
    ADFobjects.ZoneSensorV3('2','9','2'),
    ADFobjects.ZoneSensorV3('3','9','3'),
    ADFobjects.ZoneSensorV3('4','9','4'),
    ADFobjects.ZoneSensorV3('5','9','5'),
    ADFobjects.ZoneSensorV3('6','9','6'),
    ADFobjects.ZoneSensorV3('7','9','7'),
    ADFobjects.ZoneSensorV3('8','9','8'),
    ADFobjects.ZoneSensorV3('9','9','9'),
    ADFobjects.ZoneSensorV3('10','9','10'),
    ADFobjects.ZoneSensorV3('11','9','11')
]

class Test_State_class_next_methods(unittest.TestCase):

    def test_createInstructionState_advances_to_readFileState(self):
        createInstructionsState_instance = adf2diagram.createInstructionsInJSONState()
        self.assertTrue(isinstance(createInstructionsState_instance.next(), adf2diagram.finishState))

class Test_State_class_run_methods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        
        cls.state_var_instance = adf2diagram.stateVariables()
        cls.state_var_instance.ADFAnalogOutList     = test_analogOut_objects
        cls.state_var_instance.ADFRelayList         = test_relay_objects
        cls.state_var_instance.ADFSensorList        = test_sensor_objects
        cls.state_var_instance.ADFZoneList          = test_zone_objects
        cls.state_var_instance.ADFZoneSensorList    = test_zone_sensor_objects

        cls.state_instance = adf2diagram.createInstructionsInJSONState()

    def test_createInsctructionState_adds_column_headers(self):
        
        self.state_instance = adf2diagram.createInstructionsInJSONState()
        self.state_instance.run(self.state_var_instance)

        test_stepHeader = [
                'Using sensor', 
                'simulate this gas concentration',
                'These relays should change state.', 
                'These analog outputs',
                'should measure this current']

        self.assertEqual(test_stepHeader, self.state_var_instance.instructionJSONDict['stepHeader'])
    
    def test_createInsctructionState_correct_keywords_to_rows(self):

        self.state_instance = adf2diagram.createInstructionsInJSONState()
        self.state_instance.run(self.state_var_instance)

        keyword_list = [
            'sensorList',
            'signalSim',
            'relayList',
            'analogOutList',
            'analogCurrent'
        ]

        for keyword in keyword_list:
            self.assertTrue(keyword in self.state_var_instance.instructionJSONDict['steps'][0])
        
    def test_createInstructionsInJSONState_uses_the_corret_types(self):

        self.state_instance = adf2diagram.createInstructionsInJSONState()
        self.state_instance.run(self.state_var_instance)

        self.assertTrue(isinstance(self.state_var_instance.instructionJSONDict['steps'][0]['sensorList'], list))
        self.assertTrue(isinstance(self.state_var_instance.instructionJSONDict['steps'][0]['signalSim'], list))
        self.assertTrue(isinstance(self.state_var_instance.instructionJSONDict['steps'][0]['relayList'], list))
        self.assertTrue(isinstance(self.state_var_instance.instructionJSONDict['steps'][0]['analogOutList'], list))
        self.assertTrue(isinstance(self.state_var_instance.instructionJSONDict['steps'][0]['analogCurrent'], list))



if __name__ == "__main__":
    unittest.main()


        


