import unittest
import unittest.mock
from unittest.mock import patch, mock_open, call, Mock
import ADFValidationTool as tool
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import adf2diagram
import getpass
import datetime
import re
from pathlib import Path
import datetime
import json

mock = unittest.mock.MagicMock()

#Cheange workign directory
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

test_filename = 'c:\\somedirectory\\SO12345_somefile.adf'
new_dir_name = re.findall('SO[0-9]{5}', test_filename)[0]
path = Path(test_filename)
name_index = test_filename.find(path.name)
new_path = test_filename[:(name_index)] + new_dir_name
new_filename = new_path+'\\'+path.name

demo_adf_filename_noSO = '..\\test\\demo_DB.adf'
demo_adf_filename_SO = '..\\test\\SO12345_demo_DB.adf'
demo_json_filename_noSO = '..\\test\\demo_DB.json'

class Test_helper_simulatePopup_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO) as JSON_file:
            cls.test_JSONDict = json.load(JSON_file)

    def CTEF(self):
        self.assertTrue(0)
    
    def test_simulatePopup_calls_askokcannel(self):
        test_call_args = []
        row = self.mainWindow_instance.instructionsJSON['steps'][0]
        test_message_string = (
            self.mainWindow_instance.instructionsJSON['stepHeader'][0] + '\n' + 
            row['sensorList'][0] + '\n' + 
            self.mainWindow_instance.instructionsJSON['stepHeader'][1] + '\n' + 
            row['signalSim'][0]
        )
        test_call_args.append(call('Simulate Signal', test_message_string))

        with patch.object(messagebox, 'askokcancel', return_value=True) as _mock_dialog:

            self.mainWindow_instance.simulationPopup(0)

            self.assertEqual(test_call_args, _mock_dialog.call_args_list)

class Test_helper_relayCheckPopup_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO) as JSON_file:
            cls.test_JSONDict = json.load(JSON_file)

    def CTEF(self):
        self.assertTrue(0)
    
    def test_relayCheckPopup_calls_askyesnocancel(self):
        test_call_args = []
        row = self.mainWindow_instance.instructionsJSON['steps'][0]
        test_message_string = (
            self.mainWindow_instance.instructionsJSON['stepHeader'][2] + '\n' + 
            ', '.join(row['relayList'])
        )
        test_call_args.append(call('Check Relays', test_message_string))

        with patch.object(messagebox, 'askyesnocancel', return_value=True) as _mock_dialog:

            self.mainWindow_instance.relayCheckPopup(0)

            self.assertEqual(test_call_args, _mock_dialog.call_args_list)

class Test_helper_analogCheckPopup_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO) as JSON_file:
            cls.test_JSONDict = json.load(JSON_file)

    def CTEF(self):
        self.assertTrue(0)

    def test_analogCheckPopup_calls_askyesnocancel(self):
        test_call_args = []
        row = self.mainWindow_instance.instructionsJSON['steps'][0]
        test_message_string = (
            self.mainWindow_instance.instructionsJSON['stepHeader'][3] + '\n' + 
            ',\n'.join(row['analogOutList']) + '\n' + 
            self.mainWindow_instance.instructionsJSON['stepHeader'][4] + '\n' + 
            ',\n'.join(row['analogCurrent'])
        )
        test_call_args.append(call('Check Analog Out', test_message_string))

        with patch.object(messagebox, 'askyesnocancel', return_value=True) as _mock_dialog:

            self.mainWindow_instance.analogCheckPopup(0)

            self.assertEqual(test_call_args, _mock_dialog.call_args_list)

class Test_saveTestResults_method(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO) as JSON_file:
            cls.test_JSONDict = json.load(JSON_file)

    def preloadTestResults(self, testStatus):
        now = datetime.datetime.now()
        self.mainWindow_instance.SOnumber = 'SO12345'

        self.mainWindow_instance.relayTestResults = []
        self.mainWindow_instance.analogTestResults = []

        self.mainWindow_instance.salesQC.config(text=getpass.getuser())
        self.mainWindow_instance.approvalDate.config(text=now.strftime("%Y-%m-%d"))

        relayTxt = ''
        analogTxt = ''

        if False == testStatus:
            relayTxt = 'Relay Pass'
            analogTxt = 'Analog Pass'
        elif True == testStatus:
            relayTxt = 'Relay Fail'
            analogTxt = 'Analog Fail'
        elif None == testStatus:
            relayTxt = 'ABORTED'
            analogTxt = 'ABORTED'
        else:
            pass

        for row in self.mainWindow_instance.instructionsJSON['steps']:
            self.mainWindow_instance.relayTestResults.append(tk.Label(text=relayTxt))
            if row['analogOutList'] != []:
                self.mainWindow_instance.analogTestResults.append(tk.Label(text=analogTxt))

        import copy

        if testStatus:
            statusDict = {'testStatus' : 'Fail'}
        elif testStatus is None:
            statusDict = {'testStatus' : 'Abort'}
        elif not testStatus:
            statusDict = {'testStatus' : 'Pass'}
        else:
            pass

        testRecordJSON = {**statusDict, **copy.deepcopy(self.mainWindow_instance.instructionsJSON)}
        testRecordJSON['sysTestDate'] = now.strftime("%Y-%m-%d")
        testRecordJSON['mfgQC'] = getpass.getuser()

        for step, relayResultLabel, analogResultLabel in zip(testRecordJSON['steps'], self.mainWindow_instance.relayTestResults, self.mainWindow_instance.analogTestResults):
            step.update({'Relay Test'  : relayResultLabel.cget('text')})
            step.update({'Analog Test' : analogResultLabel.cget('text')})

        return testRecordJSON

    def CTEF(self):
        self.assertTrue(0)

    def test_saveTestResults_opens_a_file_with_the_correct_filename(self):

        now = datetime.datetime.now()
        datatime_str = now.strftime('%Y_%m_%d_%H%Mh')
        filename = self.mainWindow_instance.ADFfilename.cget('text')
        path = Path(filename)
        name_index = filename.find(path.name)
        new_path = filename[:(name_index)]
        self.mainWindow_instance.SOnumber = 'SO12345'
        test_value = new_path + self.mainWindow_instance.SOnumber + '_' + datatime_str + '_FAIL.json'

        with patch('builtins.open', new=mock_open(read_data='')) as _file:
            
            self.mainWindow_instance.saveTestResults(True)
            _file.assert_called_once_with(test_value, 'w')

    def test_saveTestResults_writes_good_results_to_file(self):
      
        test_results_list = self.preloadTestResults(True)

        print(test_results_list)

        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(json, 'dump', return_value=None) as _mock_dump:
            
            self.mainWindow_instance.saveTestResults(True)

            _mock_dump.assert_called_once_with(test_results_list, _file(), indent=4)

    def test_saveTestResults_writes_bad_results_to_file(self):
      
        test_results_list = self.preloadTestResults(False)

        print(test_results_list)

        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(json, 'dump', return_value=None) as _mock_dump:
            
            self.mainWindow_instance.saveTestResults(False)

            _mock_dump.assert_called_once_with(test_results_list, _file(), indent=4)

    def saveTestResults_writes_abort_results_to_file(self):
      
        test_results_list = self.preloadTestResults(None)

        print(test_results_list)

        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(json, 'dump', return_value=None) as _mock_dump:
            
            self.mainWindow_instance.saveTestResults(None)

            _mock_dump.assert_called_once_with(test_results_list, _file(), indent=4)

class Test_file_restUIWidgets_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def CTEF(self):
        self.assertTrue(0)

    def test_restUIWidgets_Resets_widgests_to_empty_lists(self):
        self.mainWindow_instance.approvalCheckList      = ['test']
        self.mainWindow_instance.approvalCheckVars      = ['test']
        self.mainWindow_instance.relayTestResults       = ['test']
        self.mainWindow_instance.analogTestResults      = ['test']
        self.mainWindow_instance.instructionHeaders_ADF = ['test']
        self.mainWindow_instance.instructionHeaders_CSV = ['test']
        self.mainWindow_instance.instructionList_ADF    = ['test']
        self.mainWindow_instance.instructionList_CSV    = ['test']
        self.mainWindow_instance.SOnumber               = 'test'

        self.mainWindow_instance.resetUIWidgets()

        self.assertEqual([], self.mainWindow_instance.approvalCheckList)
        self.assertEqual([], self.mainWindow_instance.approvalCheckVars)
        self.assertEqual([], self.mainWindow_instance.relayTestResults)
        self.assertEqual([], self.mainWindow_instance.analogTestResults)
        self.assertEqual([], self.mainWindow_instance.instructionHeaders_ADF)
        self.assertEqual([], self.mainWindow_instance.instructionHeaders_CSV)
        self.assertEqual([], self.mainWindow_instance.instructionList_ADF)
        self.assertEqual([], self.mainWindow_instance.instructionList_CSV)
        self.assertEqual('NA', self.mainWindow_instance.SOnumber)

    def test_restUIWidgets_calls_distructer_for_contents_of_instruction_frame(self):

        test_value=5

        for index in range(test_value):
            tk.Label(self.mainWindow_instance.instructionFrame, text='test'+str(index)).grid()

        self.mainWindow_instance.resetUIWidgets()

        self.assertEqual([], self.mainWindow_instance.instructionFrame.winfo_children())

class Test_file_getFilenameDialog_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def test_getFilenameDialog_calls_askopenfilename(self):
        with patch.object(filedialog, 'askopenfilename', return_value=demo_adf_filename_noSO) as _mock_filedialog:

            self.mainWindow_instance.getFilenameDialog()

            _mock_filedialog.assert_called_once_with(title='Select configuration database', filetypes= (("ADF files","*.adf"),("all files","*.*")))

    def test_getFilenameDialog_updates_statusbar_if_filename_does_not_match_pattern(self):
        with patch.object(filedialog, 'askopenfilename', return_value=demo_adf_filename_noSO) as _mock_filedialog:

            self.mainWindow_instance.getFilenameDialog()

            self.assertEqual('Filename does not match pattern.', self.mainWindow_instance.statusBar.cget('text'))
            self.assertEqual(tool.VERMILLION, self.mainWindow_instance.statusBar.cget('bg'))
            self.assertEqual(tool.WHITE, self.mainWindow_instance.statusBar.cget('fg'))

    def test_getFilenameDialog_updates_statusbar_if_filename_matchs_pattern(self):
        with patch.object(filedialog, 'askopenfilename', return_value=demo_adf_filename_SO) as _mock_filedialog:

            self.mainWindow_instance.getFilenameDialog()

            self.assertEqual('', self.mainWindow_instance.statusBar.cget('text'))
            self.assertEqual(self.mainWindow_instance.defaultbg, self.mainWindow_instance.statusBar.cget('bg'))
            self.assertEqual(tool.BLACK, self.mainWindow_instance.statusBar.cget('fg'))
    
    def test_getFilenameDialog_does_not_sets_SOnumber_if_filename_does_not_match_pattern(self):
        with patch.object(filedialog, 'askopenfilename', return_value=demo_adf_filename_noSO) as _mock_filedialog:

            self.mainWindow_instance.SOnumber = 'NA'
            test_value = 'NA'

            self.mainWindow_instance.getFilenameDialog()

            self.assertEqual(test_value, self.mainWindow_instance.SOnumber)

    def test_getFilenameDialog_sets_SOnumber_if_filename_matchs_pattern(self):
        with patch.object(filedialog, 'askopenfilename', return_value=demo_adf_filename_SO) as _mock_filedialog:

            self.mainWindow_instance.SOnumber = 'NA'
            path = Path(demo_adf_filename_SO)
            test_value = re.findall('SO[0-9]{5}', path.name)[0]

            self.mainWindow_instance.getFilenameDialog()

            self.assertEqual(test_value, self.mainWindow_instance.SOnumber)

class Test_file_createInstructionsJSON_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def test_createInstructionsJSON_rests_FSM_object(self):
        self.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        self.mainWindow_instance.adf2DiaFSM.currentState = None
        self.mainWindow_instance.adf2DiaFSM.stateVars.filename = 'NA'

        with patch.object(tool.adf2diagram.adfToDiagramFSM, 'runAll', return_value = None) as _mock_runAll,\
            patch.object(tool.adf2diagram, 'readFileState', return_value = None) as _mock_readFileState:

            self.mainWindow_instance.createInstructionsJSON()

            _mock_readFileState.assert_called_once()

    def test_crateInstructionsJSON_creates_checkbuttons(self):

        self.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        self.mainWindow_instance.adf2DiaFSM.currentState = None
        self.mainWindow_instance.adf2DiaFSM.stateVars.filename = 'NA'

        self.mainWindow_instance.resetUIWidgets()

        with patch.object(tk, 'Checkbutton', return_value=None) as _mock_Checkbutton:

            self.mainWindow_instance.createInstructionsJSON()

            self.assertEqual(len(self.mainWindow_instance.instructionsJSON['steps']), _mock_Checkbutton.call_count )
            self.assertEqual(
                len(self.mainWindow_instance.instructionsJSON['steps']), 
                len(self.mainWindow_instance.approvalCheckList)
            )
            self.assertEqual(
                len(self.mainWindow_instance.instructionsJSON['steps']), 
                len(self.mainWindow_instance.approvalCheckVars)
            )

    def test_crateInstructionsJSON_creates_test_labels(self):

        self.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        self.mainWindow_instance.adf2DiaFSM.currentState = None
        self.mainWindow_instance.adf2DiaFSM.stateVars.filename = 'NA'

        self.mainWindow_instance.resetUIWidgets()

        with patch.object(tk, 'Label', return_value=None) as _mock_Label:

            self.mainWindow_instance.createInstructionsJSON()

            self.assertEqual(len(self.mainWindow_instance.instructionsJSON['steps'])*2, _mock_Label.call_count)
            self.assertEqual(
                len(self.mainWindow_instance.instructionsJSON['steps']), 
                len(self.mainWindow_instance.relayTestResults)
            )
            self.assertEqual(
                len(self.mainWindow_instance.instructionsJSON['steps']), 
                len(self.mainWindow_instance.analogTestResults)
            )

    def test_crateInstructionsJSON_resets_metadata(self):

        self.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        self.mainWindow_instance.adf2DiaFSM.currentState = None
        self.mainWindow_instance.adf2DiaFSM.stateVars.filename = 'NA'

        self.mainWindow_instance.resetUIWidgets()
        self.mainWindow_instance.approvalDate.config(text="test_value")
        self.mainWindow_instance.salesQC.config(text="test_value")
        self.mainWindow_instance.sysTestDate.config(text="test_value")
        self.mainWindow_instance.mfgQC.config(text="test_value")

        with patch.object(tk, 'Label', return_value=None) as _mock_Label:

            self.mainWindow_instance.createInstructionsJSON()

            self.assertEqual('NA', self.mainWindow_instance.approvalDate.cget('text'))
            self.assertEqual('NA', self.mainWindow_instance.salesQC.cget('text'))
            self.assertEqual('NA', self.mainWindow_instance.sysTestDate.cget('text'))
            self.assertEqual('NA', self.mainWindow_instance.mfgQC.cget('text'))

class Test_file_loadJSON_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def test_loadJSON_calls_read_file(self):
        with patch.object(json, 'load', return_value = None) as _mock_load:

            self.mainWindow_instance.loadJSON(demo_json_filename_noSO)

            _mock_load.assert_called_once_with(demo_json_filename_noSO)

    def test_loadJSON_assigns_JSON_to_dict(self):
        with open(demo_json_filename_noSO, 'r') as JSONfile:
            test_value = json.load(JSONfile)
      
        with open(demo_json_filename_noSO, 'r') as JSONfile:
            self.mainWindow_instance.loadJSON(JSONfile)

        self.assertTrue(test_value, self.mainWindow_instance.instructionsJSON)

class Test_file_loackSalesMode_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def test_lockSalesMode_disables_buttons_and_checkboxes(self):

        test_value1 = 'disabled'
        test_value2 = 1

        self.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        self.mainWindow_instance.resetUIWidgets()
        self.mainWindow_instance.createInstructionsJSON()
        self.mainWindow_instance.salesApprovedButton.config(state='normal')
        for index in range(len(self.mainWindow_instance.approvalCheckList)):
            #self.mainWindow_instance.approvalCheckList[index].deselect()
            self.mainWindow_instance.approvalCheckList[index].config(state='normal')

        self.mainWindow_instance.lockSalesMode()

        for index in range(len(self.mainWindow_instance.approvalCheckList)):
            print(self.mainWindow_instance.approvalCheckList[index].cget('variable'))

        self.assertEqual(test_value1, self.mainWindow_instance.salesApprovedButton.cget('state'))
        for index in range(len(self.mainWindow_instance.approvalCheckList)):
            self.assertEqual(test_value1, self.mainWindow_instance.approvalCheckList[index].cget('state'))
            self.assertEqual(test_value2, self.mainWindow_instance.approvalCheckVars[index].get())

class Test_file_defaultSalesMode_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def test_defaultSalesMode_disables_button(self):
        test_value1 = 'disabled'

        self.mainWindow_instance.salesApprovedButton.config(state='normal')

        self.mainWindow_instance.lockSalesMode()

        self.assertEqual(test_value1, self.mainWindow_instance.salesApprovedButton.cget('state'))

class Test_file_unlockMfgMode_method(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.resetUIWidgets()
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO, 'r') as JSON_file_handle:
            cls.test_JSON_dict = json.load(JSON_file_handle)

    def test_unlockMfgMode_enables_button(self):

        test_value1 = 'normal'
        
        self.mainWindow_instance.systemTestButton.config(state='disabled')
        
        self.mainWindow_instance.unlockMfgMode()

        self.assertEqual(test_value1, self.mainWindow_instance.systemTestButton.cget('state'))

if __name__ == "__main__":
    unittest.main()