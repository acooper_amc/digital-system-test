import re
import pyodbc
import sys
import AMCManagerEnums
import UTx_EEPROM_tools

'''GLOBAL CONSTANTS'''
IS_400_REGEX = r"^{-2,-2,ADFSensorV4,AMC-1DBr2,\d*,(?:3|\b12\b)(?:,\d*){17},.{0,7},1(?:,\d*){3},(?:0|1)}$"

'''GLOBAL CONSTANT LISTS'''
All_xpaths = [
            r"'data_service_type__sensor_meta_a'",
            r"'data_service_type__sensor_meta_b'",
            r"'data_service_type__sensor_meta_c'",
            r"'data_service_type__sensor_meta_d'"
        ]

xpath_keys = [
    'sensor_part',
    'short_name',
    'max_gas_concentration'
]

UTX_CONFIG_KEYS = [
                    'ID',
                    'Sales_Order',
                    'MPN_REF',
                    'Serial_Number',
                    'PATH'
]

modbus_config_KEYS = [
                    'ID',
                    'UTX_CONFIG',
                    'model_number_a',
                    'model_number_b',
                    'version',
                    'tx_status',
                    'tx_mode',
                    'gas_number_a',
                    'gas_number_b',
                    'address_a',
                    'address_b',
                    'sensor_count',
                    'rs485_ab_invert',
                    'rs485_parity'
]

sensor_meta_KEYS = [
            'ID',
            'UTX_CONFIG',
            'sensor_part',
            'sensor_sn',
            'sensor_ipn',
            'short_name',
            'long_name',
            'max_gas_concentration',
            'min_gas_concentration',
            'alarm_1',
            'alarm_2',
            'alarm_3',
            'alarm_hysteresis',
            'last_cal_time_stamp',
            'next_cal_time_stamp',
            'cal_frequency',
            'last_cal_attempt_result',
            'sensor_life',
            'gas_type',
            'eng_units',
            'alarm_type'
]

'''GLOBAL VARIABLES'''


ListOfConfig = []

ListOfSensors = []

ListOf400ModbusConfigs = []

default_400_modbus_config_dict = {}

'''***** Public Functions *****'''

def importADF (MPN, ADF_path) :
    '''Runs the importer to add records to the database
    
    MPN = The marketing part number of the UTx transmitters 
        used to emulate 400 transmitters
        
    ADF_path = the file path to the ADF file containing 
        1DBX configuration'''

    ListOf400 = readADFtoList(ADF_path)

    '''IMPORTANT FUCKING NOTE! GORD! THIS MEANS YOU!
    THE LOCATION OF THE XML FILES IS ON THE AMC_DATA SERVER!
    IF THEY MOVE, YOU NEED TO UPDATE THIS!
    
    LOVE,
    ANDY
    
    '''
    xmtr_and_sensor_data = fetchMPNData(MPN, r"\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\amc-releases\Configs")

    if isinstance(xmtr_and_sensor_data, str):
        return((-1, xmtr_and_sensor_data))

    merged = merge400toUTx(MPN, xmtr_and_sensor_data, ListOf400, ADF_path)
    
    ListOfConfig = merged[0]
    ListOfSensors = merged[1]
    ListOf400ModbusConfigs = merged[2]

    for index in range(len(ListOfConfig)) :
        AddRecordsToDatabase(ListOfConfig[index], 
                            ListOfSensors[index], 
                            ListOf400ModbusConfigs[index])
    result_msg = 'Import Complete\n'
    result_msg += str(len(ListOfConfig)) + " " + MPN + " added to database.\n"
    result_msg += report_on_current_records_for_SO(ListOfConfig[0]['Sales_Order'])
    return((1, result_msg))

def getMPNList() :
    '''Connect to Parts Database and fecth the XML file path'''
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\3550403-DOC, Master Test Sequence, Supported Part List.accdb;')
    cursor = conn.cursor()
    SQL_str =  r"SELECT PartsTable.MPN FROM PartsTable WHERE PartsTable.MPN LIKE ('AMC-UTx-M-%-0400');"
    cursor.execute( SQL_str)

    fetchall = cursor.fetchall()
    
    flat_list = [item for sublist in fetchall for item in sublist]

    return (flat_list)

def ADFinDatabase(MPN, SO_number) :
    return_val = False
    
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\DOC, Transmitter Configurations.accdb;')
    cursor = conn.cursor()
    SQL_str = r"SELECT UTX_CONFIG.ID, UTX_CONFIG.Sales_Order, UTX_CONFIG.MPN_REF FROM UTX_CONFIG "
    SQL_str += r"WHERE UTX_CONFIG.Sales_Order='" + SO_number + r"' "
    SQL_str += r"AND UTX_CONFIG.MPN_REF='" + MPN + r"' "
    cursor.execute( SQL_str)

    fetchall = cursor.fetchall()
    
    if len(fetchall) > 0 :
        return_val = True

    return (return_val)

def deleteRecords(MPN, SO_number) :
    #Get the list of IDs to delete
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\DOC, Transmitter Configurations.accdb;')
    cursor = conn.cursor()
    SQL_str = r"SELECT UTX_CONFIG.ID FROM UTX_CONFIG "
    SQL_str += r"WHERE UTX_CONFIG.Sales_Order='" + SO_number + r"' "
    SQL_str += r"AND UTX_CONFIG.MPN_REF='" + MPN + r"' "
    cursor.execute( SQL_str)

    fetchall = cursor.fetchall()
    flat_list = [item for sublist in fetchall for item in sublist]
    flat_list = list(map(str, flat_list))

    #Delete IDs from UTx_Config
    SQL_str =  r"DELETE FROM UTX_CONFIG WHERE ID IN ("
    SQL_str += ', '.join(flat_list)
    SQL_str += r")"
    cursor.execute(SQL_str)
    cursor.commit()

    #Delete IDs from 400_modbus_config
    SQL_str =  r"DELETE FROM 400_modbus_config WHERE UTX_CONFIG IN ("
    SQL_str += ', '.join(flat_list)
    SQL_str += r")"
    cursor.execute(SQL_str)
    cursor.commit()

    #Delete IDs from sensor_config
    SQL_str =  r"DELETE FROM sensor_meta WHERE UTX_CONFIG IN ("
    SQL_str += ', '.join(flat_list)
    SQL_str += r")"
    cursor.execute(SQL_str)
    cursor.commit()

def readADFtoList(filePath) :
    '''Reads in a list of records from an ADF file and filters out the
    400 transmitter data.
    
    filePath - A file path to an ADF file
    
    Returns a list of dictionaries for each 400 transmitter found'''

    ListOf400 = []

    fp = open(filePath)
    Lines = fp.readlines()
    for line in Lines:
        line = line.strip()
        is400 = re.search(IS_400_REGEX, line)
        if is400 is not None:
            line = ADFtoList(line)
            temp_dict = {AMCManagerEnums.SENSOR_CONFIG_KEYS[i]: line[i+4] for i in range(len(AMCManagerEnums.SENSOR_CONFIG_KEYS))}   
            ListOf400.append(temp_dict)             


    return ListOf400

def fetchMPNData(MPN, XMLPath) :
    '''This function uses the Supported Parts Database and existing deaful tXML files to 
    provide sensor information based on MPN.
    
    MPN - the marketing part number for the UTx-400 being processes
    XMLPath - the path to the XML config files
    
    Returns a list of dictionaries for each sensor in the config'''    

    '''Connect to Parts Database and fecth the XML file path'''
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\3550403-DOC, Master Test Sequence, Supported Part List.accdb;')
    cursor = conn.cursor()
    cursor.execute( r"SELECT PartsTable.MPN, DefaultXMLTable.XMLPath " +
                    r"FROM DefaultXMLTable INNER JOIN PartsTable ON DefaultXMLTable.ID = PartsTable.DefaultXMLRef " +
                    r"WHERE (((PartsTable.MPN)='" + MPN + r"'));"
                    )

    fetchall = cursor.fetchall() 
    XML_file_path = XMLPath + "/" + fetchall[0][1]

    xpath_list = []
    xmtr_data_sorted = []
    '''Search the XML config file for the number of sensors'''
    for register in modbus_config_KEYS[2:] :

        xpath_list.append(r".//register/[name='" + register + r"']/value")
    
    list_from_xml = UTx_EEPROM_tools.readFromXML(XML_file_path, xpath_list)[2]
    xmtr_dict = {modbus_config_KEYS[i+2] : list_from_xml[i] for i in range(len(list_from_xml))}
    xmtr_data_sorted.append(xmtr_dict) 
    sensor_count = int(xmtr_dict["sensor_count"])

    '''For each sensor, get the part number, range, etc'''

    for index in range(sensor_count) :
        xpath_list = []
        for register in sensor_meta_KEYS[2:]:
            xpath_str =  r".//structure/[name=" + All_xpaths[index] 
            xpath_str += r"]//register/[name='" + register + r"']/value"
            xpath_list.append(xpath_str)
        sensor_data_unsorted = UTx_EEPROM_tools.readFromXML(XML_file_path, xpath_list)[2]
        sensor_dict = {sensor_meta_KEYS[i+2]:sensor_data_unsorted[i] for i in range(len(sensor_data_unsorted))}
        xmtr_data_sorted.append(sensor_dict)    

    return xmtr_data_sorted

def merge400toUTx(MPN, sensor_data, List400xmtrs, ADFFilePath):
    '''This function takes data about the UTx transmitter from the part number and 
    scans the list of 400 xmts for matches. If the MPN calls up two sensors, it
    will attempt to find matches.
    
    MPN - marketing part number the function will try to match up with utx transmitters
    sensor_data - a list of dictionaries of default data fetched from XML based on MPN
    List400xmtrs - a list of dictionaries of 400 data from ADF file
    ADFFilePath - the file path to the ADF file to be recorded in the database
    
    Returns a list of lists of dictionary formated to the database schema'''

    num_sensors = int(sensor_data[0]['sensor_count'])

    sensor_data_list = []

    sensor_data_dict = {
        "GAS_LABEL" : '',
        'FULL_SCALE' : ''
    }
    
    key_list = list(AMCManagerEnums.GAS_LABEL_DICT.keys())
    val_list = list(AMCManagerEnums.GAS_LABEL_DICT.values())
    
    '''Redefine sensor data values as AMC Manager enums'''
    for sensor in sensor_data[1:] :
        
        dict_index = val_list.index(sensor['short_name'])

        sensor_data_dict['GAS_LABEL'] = key_list[dict_index]
        sensor_data_dict['FULL_SCALE'] = str(int(sensor['max_gas_concentration']) * 10 )

        sensor_data_list.append(sensor_data_dict.copy())
    
    '''Start making a list of transmitters and sensors'''
  
    utx_record_list = []
    sensor_record_list = []
    modbus_config_list = []
    xmtrB = {'ADDRESS':''}

    for xmtr in List400xmtrs:
        sensor_record_short_list = []
        isTargetGas = xmtr['GAS_LABEL'] == sensor_data_list[0]['GAS_LABEL']
        isTargetRange = xmtr['FULL_SCALE'] == sensor_data_list[0]['FULL_SCALE']
        if (isTargetGas and isTargetRange):
            sensor_record_short_list.append(load_sensor_record(xmtr, sensor_data[1])) 
            
            '''Search for second sensor if needed'''
            if num_sensors == 2:
                loop_index = List400xmtrs.index(xmtr) + 1
                for xmtrB in List400xmtrs[loop_index:]:
                    isTargetGas = xmtrB['GAS_LABEL'] == sensor_data_list[1]['GAS_LABEL']
                    isTargetRange = xmtrB['FULL_SCALE'] == sensor_data_list[1]['FULL_SCALE']
                    if (isTargetGas and isTargetRange):      
                        sensor_record_short_list.append(load_sensor_record(xmtrB, sensor_data[2]))
                        xmtrB['GAS_LABEL'] = '0' #This removes the xmtr from the list for future searches
                        break

            '''Only create records if enough matching 400s where found.'''
            if len(sensor_record_short_list) == num_sensors:
                utx_record = [
                    '',
                    get_SO_from_path(ADFFilePath)[0],
                    MPN,
                    '',
                    ADFFilePath
                ]
                utx_record_list.append({UTX_CONFIG_KEYS[i]:utx_record[i] for i in range(len(UTX_CONFIG_KEYS))})
                sensor_record_list.append(sensor_record_short_list.copy())

                modbus_config_list.append(load_400_modbus_config_record(xmtr, xmtrB, sensor_data[0]))


    return(utx_record_list, sensor_record_list, modbus_config_list)

def AddRecordsToDatabase(utx_record, sensor_record_list, modbus_config_record) :
    '''Loads records of one transmitter into database tables
    utx_record - a dictionary with data for UTX_CONFIG table
    sensor_record_list - a list of dictionaries with data for sensor_meta table
    modbus_config_record - a dictonary with data for 400_modbus_config table'''
    
    #Connect to database and create a new record in UTX_CONFIG table
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\DOC, Transmitter Configurations.accdb;')
    cursor = conn.cursor()
    columns = ",".join(UTX_CONFIG_KEYS[1:])
    values = r"', '".join(list(utx_record.values())[1:])
    SQL_str = r"INSERT INTO UTX_CONFIG (" + columns + ")" 
    SQL_str += r" VALUES ('" + values + r"');"
    cursor.execute(SQL_str)
    cursor.commit()
    
    #Read back UTX_CONFIG ID
    SQL_str = r"SELECT UTX_CONFIG.ID, UTX_CONFIG.Sales_Order, UTX_CONFIG.MPN_REF FROM UTX_CONFIG "
    SQL_str += r"WHERE UTX_CONFIG.Sales_Order='" + utx_record["Sales_Order"] + r"' "
    SQL_str += r"AND UTX_CONFIG.MPN_REF='" + utx_record["MPN_REF"] + r"' "
    cursor.execute(SQL_str)
    last_record = cursor.fetchall()[-1]

    #Pack and add sensor records
    for sensor_record in sensor_record_list :
        sensor_record["UTX_CONFIG"] = str(last_record[0])
        columns = ",".join(list(sensor_record.keys())[1:])
        values = sensor_record["UTX_CONFIG"] + r",'"
        values += r"','".join(list(sensor_record.values())[2:])
        SQL_str = r"INSERT INTO sensor_meta (" + columns + r")" 
        SQL_str += r" VALUES (" + values + r"');"
        cursor.execute(SQL_str)
        cursor.commit()

    #Pack and add 400 modbus config
    modbus_config_record["UTX_CONFIG"] = str(last_record[0])
    columns = ",".join(list(modbus_config_record.keys())[1:])
    values = modbus_config_record["UTX_CONFIG"] + r",'"
    values += r"','".join(list(modbus_config_record.values())[2:])
    SQL_str = r"INSERT INTO 400_modbus_config (" + columns + r")" 
    SQL_str += r" VALUES (" + values + r"');"
    cursor.execute(SQL_str)
    cursor.commit()

    cursor.close()

##### Helper FNs #####

def get_SO_from_path (path) :
    '''Returns the sales order number from the ADF file path'''
    return (re.findall(r"SO\d{5}", path ))


def ADFtoList(ADFString) :
    '''Returns a list given an ADF record string'''
    ADFList = ADFString.replace('{', '')
    ADFList = ADFList.replace('}', '')
    ADFList = ADFList.split(',')
    return ADFList

def load_sensor_record(xmtr_data, sensor_data) :
    '''Returns a dictionary using the snesor record schema using a 400 xmtr dictionay.'''
    
    if '' == xmtr_data['ALARM_SET_POINT_1']: xmtr_data['ALARM_SET_POINT_1'] = '100'
    if '' == xmtr_data['ALARM_SET_POINT_2']: xmtr_data['ALARM_SET_POINT_2'] = '100'
    if '' == xmtr_data['ALARM_SET_POINT_3']: xmtr_data['ALARM_SET_POINT_3'] = '100'

    record_list = [
        '',
        '', 
        sensor_data['sensor_part'],
        sensor_data['sensor_sn'],
        sensor_data['sensor_ipn'],
        sensor_data['long_name'],
        sensor_data['short_name'],
        sensor_data['max_gas_concentration'],
        sensor_data['min_gas_concentration'],
        str(int(sensor_data['max_gas_concentration'])*int(xmtr_data['ALARM_SET_POINT_1'])/100),
        str(int(sensor_data['max_gas_concentration'])*int(xmtr_data['ALARM_SET_POINT_2'])/100),
        str(int(sensor_data['max_gas_concentration'])*int(xmtr_data['ALARM_SET_POINT_3'])/100),
        sensor_data['alarm_hysteresis'],
        sensor_data['last_cal_time_stamp'],
        sensor_data['next_cal_time_stamp'],
        sensor_data['cal_frequency'],
        sensor_data['last_cal_attempt_result'],
        sensor_data['sensor_life'],
        sensor_data['gas_type'],
        sensor_data['eng_units'],
        sensor_data['alarm_type']
    ]

    return({sensor_meta_KEYS[i]: record_list[i] for i in range(len(sensor_meta_KEYS))} )

def load_400_modbus_config_record(xmtr_data_a, xmtr_data_b, modbus_data) :
    '''Returns a dictionary using the 400 modbus config record schema 
    using a 400 xmtr dictionay.'''
    
    record_list = [
        '',
        '', 
        modbus_data['model_number_a'],
        modbus_data['model_number_b'],
        modbus_data['version'],
        modbus_data['tx_status'],
        modbus_data['tx_mode'],
        modbus_data['gas_number_a'],
        modbus_data['gas_number_b'],
        xmtr_data_a['ADDRESS'],
        xmtr_data_b['ADDRESS'],
        modbus_data['sensor_count'],
        modbus_data['rs485_ab_invert'],
        modbus_data['rs485_parity'],
    ]

    return({modbus_config_KEYS[i]: record_list[i] for i in range(len(modbus_config_KEYS))} )

def report_on_current_records_for_SO(SO_number) :
    '''Returns a string with a report on the count of transmitters int eh database for a given SO number'''
    conn = pyodbc.connect(r'Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=\\AMC-DATA\Operations\Manufacturing\manufacturing test\TestStand Support File\DOC, Transmitter Configurations.accdb;')
    cursor = conn.cursor()
    SQL_str  = r"SELECT UTX_CONFIG.Sales_Order, UTX_CONFIG.MPN_REF, COUNT(UTX_CONFIG.MPN_REF) AS CountOfMPN_REF "
    SQL_str += r"FROM UTX_CONFIG "
    SQL_str += r"GROUP BY UTX_CONFIG.Sales_Order, UTX_CONFIG.MPN_REF "
    SQL_str += r"HAVING (((UTX_CONFIG.Sales_Order)='" + SO_number + r"'));"
    cursor.execute(SQL_str)
    all_records = cursor.fetchall()
    report = SO_number + " currently contains the following transmitters:\n"
    for record in all_records :
        report += str(record[2]) + ' ' + record[1] + '\n'
    return(report)

if __name__ == "__main__" :
    #file_path = r"\\AMC-DATA\Operations\Manufacturing\Configuration Sheets\1DBx Program Files\SO75249\SO75249 amc-1db.adf"
    #MPN = "AMC-UTx-M-91A01-98A01-R-0400"
    #MPN = sys.argv[1]
    #file_path = sys.argv[2]
    
    #importADF(MPN, file_path)
    #print (report_on_current_records_for_SO('SO74239'))
    desktop_path = r'C:/Users/acooper/Desktop/SO66669-amc-1db.adf'
    print (get_SO_from_path(desktop_path))
