import unittest
import ADFobjects

sample_sensor_list = ['-2','-2','ADFSensorV4','AMC-1DBr2','0','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','1','0','0']

sample_relay_list = ['-2','-2','ADFRelayV4','AMC-1DBr2','0','1','0','10','0','1','0','1','0','0','0']

sample_zone_list = ['-2','-2','ZoneV3','AMC-1DBr2','0','1','9','23','59','23','59','0','255','255','255','255','0']

sample_zone_sensor_list = ['-2','-2','ZoneSensorV3','AMC-1DBr2','0','0','0']

sample_analog_out_list = ['-2','-2','ADFAnalogOutV4','AMC-1DBr2','0','1','0','1','0','0','10','1','60','0']

sample_RS485Parameter_list = ['-2','-2','ADFRS485ParametersV4','AMC-1DBr2','0','0','3','1','1','0','1']

sample_UserGasLabel_list = ['-2','-2','UserGasLabel','201','Gas1']

sample_UserEngLabel_list = ['-2','-2','UserEngLabel','100','eng1']

class Test_ADFobject_constructors(unittest.TestCase):

    def test_SensorV4_constructor(self):
        sensor_instance = ADFobjects.SensorV4(*sample_sensor_list[4:])
        self.assertEqual(sample_sensor_list[4],  sensor_instance.ID)
        self.assertEqual(sample_sensor_list[5],  sensor_instance.GasLabel)
        self.assertEqual(sample_sensor_list[6],  sensor_instance.EngUnit)
        self.assertEqual(sample_sensor_list[7],  sensor_instance.FullScale)
        self.assertEqual(sample_sensor_list[8],  sensor_instance.Zero)
        self.assertEqual(sample_sensor_list[9],  sensor_instance.AlarmCondition)
        self.assertEqual(sample_sensor_list[10], sensor_instance.NumAlarms)
        self.assertEqual(sample_sensor_list[11], sensor_instance.AlarmRelays['1'])
        self.assertEqual(sample_sensor_list[12], sensor_instance.AlarmSetPoints['1'])
        self.assertEqual(sample_sensor_list[13], sensor_instance.AlarmDelay['1'])
        self.assertEqual(sample_sensor_list[14], sensor_instance.AlarmRelays['2'])
        self.assertEqual(sample_sensor_list[15], sensor_instance.AlarmSetPoints['2'])
        self.assertEqual(sample_sensor_list[16], sensor_instance.AlarmDelay['2'])
        self.assertEqual(sample_sensor_list[17], sensor_instance.AlarmRelays['3'])
        self.assertEqual(sample_sensor_list[18], sensor_instance.AlarmSetPoints['3'])
        self.assertEqual(sample_sensor_list[19], sensor_instance.AlarmDelay['3'])
        self.assertEqual(sample_sensor_list[20], sensor_instance.AlarmRelays['fail'])
        self.assertEqual(sample_sensor_list[21], sensor_instance.AlarmSetPoints['fail'])
        self.assertEqual(sample_sensor_list[22], sensor_instance.AlarmDelay['fail'])
        self.assertEqual(sample_sensor_list[23], sensor_instance.Location)
        self.assertEqual(sample_sensor_list[24], sensor_instance.AdminState)
        self.assertEqual(sample_sensor_list[25], sensor_instance.Interface)
        self.assertEqual(sample_sensor_list[26], sensor_instance.Address)
        self.assertEqual(sample_sensor_list[27], sensor_instance.Instance)
        self.assertEqual(sample_sensor_list[28], sensor_instance.PartRef)

    def test_RelayV4_constructor(self):
        relay_instance = ADFobjects.RelayV4(*sample_relay_list[4:])
        self.assertEqual(sample_relay_list[4],  relay_instance.ID)
        self.assertEqual(sample_relay_list[5],  relay_instance.AlarmCount)
        self.assertEqual(sample_relay_list[6],  relay_instance.Delay)
        self.assertEqual(sample_relay_list[7],  relay_instance.MinRunTime)
        self.assertEqual(sample_relay_list[8],  relay_instance.Config)
        self.assertEqual(sample_relay_list[9],  relay_instance.AdminState)
        self.assertEqual(sample_relay_list[10], relay_instance.Interface)
        self.assertEqual(sample_relay_list[11], relay_instance.Address)
        self.assertEqual(sample_relay_list[12], relay_instance.Instance)
        self.assertEqual(sample_relay_list[13], relay_instance.PostRunTime)
        self.assertEqual(sample_relay_list[14], relay_instance.PartRef)

    def test_Zone3V_constructor(self):
        zone_instance = ADFobjects.ZoneV3(*sample_zone_list[4:])
        self.assertEqual(sample_zone_list[4],  zone_instance.ID)
        self.assertEqual(sample_zone_list[5],  zone_instance.AdminState)
        self.assertEqual(sample_zone_list[6],  zone_instance.ScheduleDay)
        self.assertEqual(sample_zone_list[7],  zone_instance.ScheduleStartHr)
        self.assertEqual(sample_zone_list[8],  zone_instance.ScheduleStartMin)
        self.assertEqual(sample_zone_list[9],  zone_instance.ScheduleEndHr)
        self.assertEqual(sample_zone_list[10], zone_instance.ScheduleEndMin)
        self.assertEqual(sample_zone_list[11], zone_instance.ScheduleRelay)
        self.assertEqual(sample_zone_list[12], zone_instance.AlarmRelays['1'])
        self.assertEqual(sample_zone_list[13], zone_instance.AlarmRelays['2'])
        self.assertEqual(sample_zone_list[14], zone_instance.AlarmRelays['3'])
        self.assertEqual(sample_zone_list[15], zone_instance.AlarmRelays['fail'])
        self.assertEqual(sample_zone_list[16], zone_instance.AnalogOut)
    
    def test_ZoneSensorV3(self):
        zone_sensor_instance = ADFobjects.ZoneSensorV3(*sample_zone_sensor_list[4:])
        self.assertEqual(sample_zone_sensor_list[4],  zone_sensor_instance.ID)
        self.assertEqual(sample_zone_sensor_list[5],  zone_sensor_instance.ZoneID)
        self.assertEqual(sample_zone_sensor_list[6],  zone_sensor_instance.SensorID)

    def test_AlanolgOutV4(self):
        analog_out_instance = ADFobjects.AnalogOutV4(*sample_analog_out_list[4:])
        self.assertEqual(sample_analog_out_list[4],  analog_out_instance.ID)
        self.assertEqual(sample_analog_out_list[5],  analog_out_instance.AdminState)
        self.assertEqual(sample_analog_out_list[6],  analog_out_instance.Interface)
        self.assertEqual(sample_analog_out_list[7],  analog_out_instance.Address)
        self.assertEqual(sample_analog_out_list[8],  analog_out_instance.Instance)
        self.assertEqual(sample_analog_out_list[9],  analog_out_instance.Range)
        self.assertEqual(sample_analog_out_list[10], analog_out_instance.Scale10X)
        self.assertEqual(sample_analog_out_list[11], analog_out_instance.Type)
        self.assertEqual(sample_analog_out_list[12], analog_out_instance.SamplePeriod)
        self.assertEqual(sample_analog_out_list[13], analog_out_instance.PartRef)

    def test_ADFRS485ParametersV4(self):
        ADFRS485ParametersV4_instance = ADFobjects.ADFRS485ParametersV4(*sample_RS485Parameter_list[4:])
        self.assertEqual(sample_RS485Parameter_list[4], ADFRS485ParametersV4_instance.RS485ID)
        self.assertEqual(sample_RS485Parameter_list[5], ADFRS485ParametersV4_instance.Protocol)
        self.assertEqual(sample_RS485Parameter_list[6], ADFRS485ParametersV4_instance.BaudRate)
        self.assertEqual(sample_RS485Parameter_list[7], ADFRS485ParametersV4_instance.CharLen)
        self.assertEqual(sample_RS485Parameter_list[8], ADFRS485ParametersV4_instance.Parity)
        self.assertEqual(sample_RS485Parameter_list[9], ADFRS485ParametersV4_instance.StopBits)
        self.assertEqual(sample_RS485Parameter_list[10], ADFRS485ParametersV4_instance.AdminState)

    def test_UserGasLabel(self):
        UserGasLabel_instance = ADFobjects.UserGasLabel(*sample_UserGasLabel_list[3:])
        self.assertEqual(sample_UserGasLabel_list[3], UserGasLabel_instance.GasID)
        self.assertEqual(sample_UserGasLabel_list[4], UserGasLabel_instance.GasLabel)
    
    def test_UserEngLabel(self):
        UserEngLabel_instance = ADFobjects.UserEngLabel(*sample_UserEngLabel_list[3:])
        self.assertEqual(sample_UserEngLabel_list[3], UserEngLabel_instance.UnitID)
        self.assertEqual(sample_UserEngLabel_list[4], UserEngLabel_instance.UnitLabel)

if __name__ == '__main__':
    unittest.main()

