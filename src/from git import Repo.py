
import git
import os

XMLPath = r"C:/git_repos/amc-releases/Configs"

try :
    if not os.path.isdir(XMLPath):
        git.Repo.clone_from(r'https://acooper_amc:1.6021766208x10^-19@bitbucket.org/armstrongmonitoring/amc-releases.git', XMLPath.replace('Configs',''))
    else:
        git.Repo(XMLPath.replace('Configs','')).remotes.origin.pull()
except git.GitCommandError:
    print('A git command error occured. Check user name, password, or repo URL')

#myRepo = Repo.clone_from(r'https://acooper_amc:1.6021766208x10^-19@bitbucket.org/armstrongmonitoring/amc-release.git', 'C:/git_repos/test/')
