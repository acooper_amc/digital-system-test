import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys

import plantuml

import UMLObjects

import adf2diagram

mock = unittest.mock.MagicMock()

test_UMLMonitor_object = UMLObjects.monitor()
test_UMLMonitor_object.localRelays = {
        '0' : '0',
        '1' : '1',
        '2' : '2',
        '3' : '3',
        '4' : '4',
        '5' : '5',
        '6' : '6',
        '7' : '7'
    }
test_UMLMonitor_object.localAnalogOut = {
        '0' : '0',
        '1' : '1',
        '2' : '2',
        '3' : '3'
    }

test_UMLTransmitter_objects = [
    UMLObjects.digitalTransmitter('1', 
    {
        '0' : {'address' : '1', 'gas label': 'CO', 'location': '', 'hardware' : '1'},
        '1' : {'address' : '2', 'gas label': 'CO', 'location': '', 'hardware' : '1'},
        '2' : {'address' : '3', 'gas label': 'NO2', 'location': '', 'hardware' : '1'},
        '3' : {'address' : '4', 'gas label': 'NO2', 'location': '', 'hardware' : '1'},
        '4' : {'address' : '5', 'gas label': 'Cl2', 'location': '', 'hardware' : '3', 'instance' : '0'},
        '5' : {'address' : '6', 'gas label': 'Cl2', 'location': '', 'hardware' : '3', 'instance' : '1'}
    })
]

test_UMLRelayModule_objects = [
    UMLObjects.relayModule('2', '102', 
    {
        '8'  : '0',
        '9'  : '1',
        '10' : '2',
        '11' : '3',
        '12' : '4',
        '13' : '5',
        '14' : '6',
        '15' : '7'
    })
]

test_UMLAnalogInModule_objects = [
    UMLObjects.analogInputModule('2', '101', 
    {
        '6'  : {'instance' : '2', 'gas label': 'CO2', 'location': ''},
        '7'  : {'instance' : '3', 'gas label': 'CO2', 'location': ''}
    }),
    UMLObjects.analogInputModule('3', '101',
    {
        '8'   : {'instance' : '4', 'gas label': 'SO2', 'location': ''},
        '9'   : {'instance' : '5', 'gas label': 'SO2', 'location': ''},
        '10'  : {'instance' : '6', 'gas label': 'SO2', 'location': ''},
        '11'  : {'instance' : '7', 'gas label': 'SO2', 'location': ''}
    })
]

test_UMLAnalogOutModule_objects = [
    UMLObjects.analogOutputModule('1', '101',
    {
        '4'  : '0',
        '5'  : '1',
        '6'  : '2',
        '7'  : '3',
        '8'  : '4',
        '9'  : '5',
        '10' : '6',
        '11' : '7'
    })
]

test_UMLRS485Notes_Objects = [
    UMLObjects.RS485Configuration('0','0','3','1','1','0'),
    UMLObjects.RS485Configuration('1','0','3','1','1','0'),
    UMLObjects.RS485Configuration('2','0','3','1','1','0'),
    UMLObjects.RS485Configuration('3','0','3','1','1','0'),
    UMLObjects.RS485Configuration('4','0','3','1','1','0')
]

class Test_State_class_constructor_methods(unittest.TestCase):

    def test_writeFileState_creates_an_empty_string(self):
        writeFileState_instance = adf2diagram.writeFileState()
        self.assertEqual('', writeFileState_instance.masterStr)

class Test_State_class_next_methods(unittest.TestCase):

    def test_writeFileState_advance_to_finishState(self):
        writeFileState_instance = adf2diagram.writeFileState()
        self.assertTrue(isinstance(writeFileState_instance.next(), adf2diagram.createInstructionsInJSONState))

class Test_State_class_run_methods(unittest.TestCase):

    def test_writeFileState_run_method_writes_the_UML_open_string(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()

                writeFileState_instance.run(stateVars_instance)

                self.assertEqual(0, writeFileState_instance.masterStr.find('@startuml\n'))

    def test_writeFileState_run_method_writes_the_UML_end_string(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()

                writeFileState_instance.run(stateVars_instance)

                self.assertEqual(len(writeFileState_instance.masterStr) - 7, writeFileState_instance.masterStr.find('@enduml'))

    def test_writeFileState_run_method_writes_a_title(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'

                test_str = '\n' + 'title ' + stateVars_instance.filename + '\\n' + 'Block Wiring Diagram' + '\n'

                writeFileState_instance.run(stateVars_instance)

                self.assertEqual(10, writeFileState_instance.masterStr.find(test_str))

    def test_writeFileState_run_method_writes_a_monitor_object(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object

                test_str = ''
                test_str += '\n'
                test_str += 'object 1DBX {\n'
                test_str += '   __Local Relays__\n'
                for relayID in test_UMLMonitor_object.localRelays:
                    test_str += '   Relay ' + relayID + ' : Local ' + test_UMLMonitor_object.localRelays[relayID] + '\n'
                test_str += '   __Local Analog Out__\n'
                for analogOutID in test_UMLMonitor_object.localAnalogOut:
                    test_str += '   AnalogOut ' + analogOutID + ' : Local ' + test_UMLMonitor_object.localAnalogOut[analogOutID] + '\n'
                test_str += '}\n'

                writeFileState_instance.run(stateVars_instance)

                self.assertNotEqual(-1, writeFileState_instance.masterStr.find(test_str))

    def test_writeFileState_run_method_writes_digital_transmitter_objects(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects

                tansmitterNum = 1
                for digitalTransmitterObject in test_UMLTransmitter_objects:
                    if 0 != len(digitalTransmitterObject.sensors):
                        test_str = ''
                        test_str += '\n'
                        test_str += 'object Digital_Transmitters_' + str(tansmitterNum) +  '{\n'
                        for sensorID in digitalTransmitterObject.sensors:
                            test_str += '   Sensor ' + sensorID + ' : Address ' + digitalTransmitterObject.sensors[sensorID]['address'] + ', '
                            test_str += digitalTransmitterObject.sensors[sensorID]['gas label'] + ', '
                            test_str += digitalTransmitterObject.sensors[sensorID]['location'] + ', '
                            test_str += adf2diagram.XMRT_PART_REF_DICT[digitalTransmitterObject.sensors[sensorID]['hardware']]
                            if adf2diagram.PART_REF__DTR_TRANSMITTER == digitalTransmitterObject.sensors[sensorID]['hardware']:
                                test_str += digitalTransmitterObject.sensors[sensorID]['instance']
                            test_str += '\n'
                        test_str += '}\n'
                        tansmitterNum + 1

                writeFileState_instance.run(stateVars_instance)

                self.assertNotEqual(-1, writeFileState_instance.masterStr.find(test_str))
    
    def test_writeFileState_run_method_writes_analogIn_objects(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects
                stateVars_instance.UMLAnalogInModuleList = test_UMLAnalogInModule_objects

                AnalogInNum = 1
                test_str = ''
                for analogInObject in test_UMLAnalogInModule_objects:
                    test_str += '\n'
                    test_str += 'object AnalogIn_' + str(AnalogInNum) +  '{\n'
                    test_str += '   Address : ' + analogInObject.address + '\n'
                    test_str += '   __Sensors__\n'
                    for sensorID in analogInObject.sensors:
                        test_str += '   Sensor ' + sensorID + ' : '
                        test_str += 'Channel ' + analogInObject.sensors[sensorID]['instance'] + ', '
                        test_str += analogInObject.sensors[sensorID]['gas label'] + ', ' 
                        test_str += analogInObject.sensors[sensorID]['location'] +'\n'
                    test_str += '}\n'
                    AnalogInNum = AnalogInNum + 1   

                writeFileState_instance.run(stateVars_instance)

                self.assertNotEqual(-1, writeFileState_instance.masterStr.find(test_str))
    
    def test_writeFileState_run_method_writes_analogOut_objects(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects
                stateVars_instance.UMLAnalogInModuleList = test_UMLAnalogInModule_objects
                stateVars_instance.UMLAnalogOutModuleList = test_UMLAnalogOutModule_objects

                AnalogOutNum = 1
                test_str = ''
                for analogOutObject in test_UMLAnalogOutModule_objects:
                    test_str += '\n'
                    test_str += 'object AnalogOut_' + str(AnalogOutNum) +  '{\n'
                    test_str += '   Address : ' + analogOutObject.address + '\n'
                    test_str += '   __Outputs__\n'
                    for analogOutID in analogOutObject.outputs:
                        test_str += '   Analog Out ' + analogOutID + ' : Channel ' + analogOutObject.outputs[analogOutID] + '\n'
                    test_str += '}\n'
                    AnalogOutNum = AnalogOutNum + 1

                writeFileState_instance.run(stateVars_instance)

                self.assertNotEqual(-1, writeFileState_instance.masterStr.find(test_str))

    def test_writeFileState_run_method_writes_relay_module_objects(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects
                stateVars_instance.UMLAnalogInModuleList = test_UMLAnalogInModule_objects
                stateVars_instance.UMLAnalogOutModuleList = test_UMLAnalogOutModule_objects
                stateVars_instance.UMLRelayModuleList = test_UMLRelayModule_objects

                test_str = ''
                RelayModuleNum = 1
                for relayObject in test_UMLRelayModule_objects:
                    test_str += '\n'
                    test_str += 'object RelayModule_' + str(RelayModuleNum) +  '{\n'
                    test_str += '   Address : ' + relayObject.address + '\n'
                    test_str += '   __Relays__\n'
                    for relayID in relayObject.relays:
                        test_str += '   Relay ' + relayID + ' : Local ' + relayObject.relays[relayID] + '\n'
                    test_str += '}\n'
                    RelayModuleNum = RelayModuleNum + 1

                print(test_str)

                writeFileState_instance.run(stateVars_instance)

                self.assertNotEqual(-1, writeFileState_instance.masterStr.find(test_str))
    
    def test_writeFileState_run_method_links_objects_by_MODBUS(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects
                stateVars_instance.UMLAnalogInModuleList = test_UMLAnalogInModule_objects
                stateVars_instance.UMLAnalogOutModuleList = test_UMLAnalogOutModule_objects
                stateVars_instance.UMLRelayModuleList = test_UMLRelayModule_objects
                stateVars_instance.UMLRS485ConfigList = test_UMLRS485Notes_Objects

                test_str = ''
                test_str += '1DBX -- ModbusNote2\n'
                test_str += 'ModbusNote2 -- Digital_Transmitters_1\n'
                test_str += 'Digital_Transmitters_1 -- AnalogOut_1\n'
                test_str += '1DBX -- ModbusNote3\n'
                test_str += 'ModbusNote3 -- AnalogIn_1\n'
                test_str += 'AnalogIn_1 -- RelayModule_1\n'
                test_str += '1DBX -- ModbusNote4\n'
                test_str += 'ModbusNote4 -- AnalogIn_2\n'

                writeFileState_instance.run(stateVars_instance)
                print(writeFileState_instance.masterStr)

                self.assertNotEqual(-1, writeFileState_instance.masterStr.find(test_str))

    def test_writeFileState_run_method_calls_file_open_method(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects
                stateVars_instance.UMLAnalogInModuleList = test_UMLAnalogInModule_objects
                stateVars_instance.UMLAnalogOutModuleList = test_UMLAnalogOutModule_objects
                stateVars_instance.UMLRelayModuleList = test_UMLRelayModule_objects
                writeFileState_instance.run(stateVars_instance)

                test_filename = 'filename.png'

                _file.assert_called_with(test_filename, 'wb')
    
    def test_writeFileState_run_method_calls_file_write_method(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file,\
            patch.object(plantuml.PlantUML, '__init__', return_value=None) as _mock_plantuml,\
            patch.object(plantuml.PlantUML, 'processes', return_value=0) as _mock_process:
                writeFileState_instance = adf2diagram.writeFileState()
                stateVars_instance = adf2diagram.stateVariables()
                stateVars_instance.filename = 'filename.adf'
                stateVars_instance.UMLmonitor = test_UMLMonitor_object
                stateVars_instance.UMLDigitalTransmitterList = test_UMLTransmitter_objects
                stateVars_instance.UMLAnalogInModuleList = test_UMLAnalogInModule_objects
                stateVars_instance.UMLAnalogOutModuleList = test_UMLAnalogOutModule_objects
                stateVars_instance.UMLRelayModuleList = test_UMLRelayModule_objects
                writeFileState_instance.run(stateVars_instance)

                handle = _file()
                handle.write.assert_called_once()

    

if __name__ == "__main__":
    unittest.main()


        


