import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys
import ADFobjects


import adf2diagram

mock = unittest.mock.MagicMock()
test_ADFMsgLists = [
            ['-2','-2','ADFAnalogOutV4','AMC-1DBr2','0','1','0','1','0','0','10','1','60','0'],
            ['-2','-2','ADFAnalogOutV4','AMC-1DBr2','12','0','0','1','0','0','10','1','60','0'],
            ['-2','-2','ADFRelayV4','AMC-1DBr2','0','1','0','10','0','1','0','1','0','0','0'],
            ['-2','-2','ADFRelayV4','AMC-1DBr2','16','1','0','10','0','0','0','1','0','0','0'],
            ['-2','-2','ADFSensorV4','AMC-1DBr2','0','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','1','0','0'],
            ['-2','-2','ADFSensorV4','AMC-1DBr2','13','0'],
            ['-2','-2','ZoneV3','AMC-1DBr2','0','1','9','23','59','23','59','0','255','255','255','255','0'],
            ['-2','-2','ZoneV3','AMC-1DBr2','10','0','9','23','59','23','59','1','40','41','42','43','0'],
            ['-2','-2','ZoneSensorV3','AMC-1DBr2','1','0','1'],
            ['-2','-2','ZoneSensorV3','AMC-1DBr2','2','0','-'],
            ['-2','-2','ADFRS485ParametersV4','AMC-1DBr2','0','0','3','1','1','0','1'],
            ['-2','-2','ADFRS485ParametersV4','AMC-1DBr2','1','0','3','1','1','0','1'],
            ['-2','-2','ADFRS485ParametersV4','AMC-1DBr2','2','0','3','1','1','0','1'],
            ['-2','-2','ADFRS485ParametersV4','AMC-1DBr2','3','0','3','1','1','0','1'],
            ['-2','-2','ADFRS485ParametersV4','AMC-1DBr2','4','0','3','1','1','0','1'],
            ['-2','-2','UserGasLabel','201','Gas1'],
            ['-2','-2','UserGasLabel','202','Gas2'],
            ['-2','-2','UserEngLabel','100','eng1'],
            ['-2','-2','UserEngLabel','101','eng2']
            ]

ENABLED_ANALOGOUT   = 0
DISALBED_ANALOGOUT  = 1
ENABLED_RELAY       = 2
DISALBED_RELAY      = 3
ENABLED_SENSOR      = 4
DISALBED_SENSOR     = 5
ENABLED_ZONE        = 6
DISALBED_ZONE       = 7
ENABLED_ZONESENSOR  = 8
DISALBED_ZONESENSOR = 9

TOTAL_ENABLED_MSGS  = 10
START_OF_ARGS       = 4
START_OF_LABEL_ARG  = 3
GAS_LABEL           = 15
ENG_LABEL           = 17


class Test_State_class_next_methods(unittest.TestCase):

    def test_convertToObjectsState_advacnes_to_createUMLObjectsState(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        self.assertTrue(isinstance(convertToObjectsState_instance.next(), adf2diagram.createUMLObjectsState))

class Test_State_class_run_method(unittest.TestCase):

    def test_convertToObjectsState_run_skips_disabled_analogOut_objects(self):

        with patch.object(ADFobjects.AnalogOutV4, "__init__", return_value=None) as m:
            convertToObjectsState_instance = adf2diagram.convertToObjectsState()
            stateVars_instance = adf2diagram.stateVariables()
            stateVars_instance.ADFMsgLists = [test_ADFMsgLists[DISALBED_ANALOGOUT]]
            convertToObjectsState_instance.run(stateVars_instance)

            m.assert_not_called()
    
    def test_convertToObjectsState_run_adds_analogOut_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[ENABLED_ANALOGOUT]]
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFAnalogOutList[0], ADFobjects.AnalogOutV4))

    def test_convertToObjectsState_run_skips_disabled_relay_objects(self):
        
        with patch.object(ADFobjects.RelayV4, "__init__", return_value=None) as m:
            convertToObjectsState_instance = adf2diagram.convertToObjectsState()
            stateVars_instance = adf2diagram.stateVariables()
            stateVars_instance.ADFMsgLists = [test_ADFMsgLists[DISALBED_RELAY]]
            convertToObjectsState_instance.run(stateVars_instance)

            m.assert_not_called()
    
    def test_convertToObjectsState_run_adds_relay_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[ENABLED_RELAY]]
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFRelayList[0], ADFobjects.RelayV4))

    def test_convertToObjectsState_run_skips_disabled_sensor_objects(self):
        
        with patch.object(ADFobjects.SensorV4, "__init__", return_value=None) as m:
            convertToObjectsState_instance = adf2diagram.convertToObjectsState()
            stateVars_instance = adf2diagram.stateVariables()
            stateVars_instance.ADFMsgLists = [test_ADFMsgLists[DISALBED_SENSOR]]
            convertToObjectsState_instance.run(stateVars_instance)

            m.assert_not_called()
    
    def test_convertToObjectsState_run_adds_sensor_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[ENABLED_SENSOR]]
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFSensorList[0], ADFobjects.SensorV4))

    def test_convertToObjectsState_run_skips_disabled_zone_objects(self):
        
        with patch.object(ADFobjects.ZoneV3, "__init__", return_value=None) as m:
            convertToObjectsState_instance = adf2diagram.convertToObjectsState()
            stateVars_instance = adf2diagram.stateVariables()
            stateVars_instance.ADFMsgLists = [test_ADFMsgLists[DISALBED_ZONE]]
            convertToObjectsState_instance.run(stateVars_instance)

            m.assert_not_called()
    
    def test_convertToObjectsState_run_adds_zone_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[ENABLED_ZONE]]
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFZoneList[0], ADFobjects.ZoneV3))

    def test_convertToObjectsState_run_skips_disabled_zoneSensors_objects(self):
        
        with patch.object(ADFobjects.ZoneSensorV3, "__init__", return_value=None) as m:
            convertToObjectsState_instance = adf2diagram.convertToObjectsState()
            stateVars_instance = adf2diagram.stateVariables()
            stateVars_instance.ADFMsgLists = [test_ADFMsgLists[DISALBED_ZONESENSOR]]
            convertToObjectsState_instance.run(stateVars_instance)

            m.assert_not_called()
    
    def test_convertToObjectsState_run_adds_zoneSensor_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[ENABLED_ZONESENSOR]]
        stateVars_instance.ADFZoneList = [ADFobjects.ZoneV3(*test_ADFMsgLists[ENABLED_ZONE][4:])]
        stateVars_instance.ADFZoneList[0].ID = '1'
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFZoneSensorList[0], ADFobjects.ZoneSensorV3))

    def test_converToObjectState_run_adds_gaslabel_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[GAS_LABEL]]        
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFGasLabelList[0], ADFobjects.UserGasLabel))

    def test_converToObjectState_run_adds_englabel_object_to_list(self):
        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = [test_ADFMsgLists[ENG_LABEL]]        
        convertToObjectsState_instance.run(stateVars_instance)

        self.assertTrue(isinstance(stateVars_instance.ADFEngLabelList[0], ADFobjects.UserEngLabel))

    def test_convertToObjectsState_run_works_over_all_msgs_in_list(self):

        convertToObjectsState_instance = adf2diagram.convertToObjectsState()
        stateVars_instance = adf2diagram.stateVariables()
        stateVars_instance.ADFMsgLists = test_ADFMsgLists
        
        convertToObjectsState_instance.run(stateVars_instance)

        total_objects = 0
        total_objects += len(stateVars_instance.ADFAnalogOutList) 
        total_objects += len(stateVars_instance.ADFRelayList)
        total_objects += len(stateVars_instance.ADFSensorList)
        total_objects += len(stateVars_instance.ADFZoneList)
        total_objects += len(stateVars_instance.ADFZoneSensorList)
        total_objects += len(stateVars_instance.ADFRS485ConfigurationList)

        self.assertEqual(TOTAL_ENABLED_MSGS, total_objects)
        

            



            
if __name__ == "__main__":
    unittest.main()


        


