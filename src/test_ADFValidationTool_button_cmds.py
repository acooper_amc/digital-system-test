import unittest
import unittest.mock
from unittest.mock import patch, mock_open, call, Mock
import ADFValidationTool as tool
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import adf2diagram
import getpass
import datetime
import re
from pathlib import Path
import datetime
import json

mock = unittest.mock.MagicMock()

#Cheange workign directory
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

test_filename = 'c:\\somedirectory\\SO12345_somefile.adf'
new_dir_name = re.findall('SO[0-9]{5}', test_filename)[0]
path = Path(test_filename)
name_index = test_filename.find(path.name)
new_path = test_filename[:(name_index)] + new_dir_name
new_filename = new_path+'\\'+path.name

demo_adf_filename_noSO = '..\\test\\demo_DB.adf'
demo_adf_filename_SO = '..\\test\\demo_DB.adf'
demo_json_filename_noSO = '..\\test\\demo_DB.json'

class Test_salesApprovalButtonCmd(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)
        cls.mainWindow_instance.createInstructionsJSON()

        with open(demo_json_filename_noSO) as JSON_file:
            cls.test_JSONDict = json.load(JSON_file)
    
    def CTEF(self):
        self.assertTrue(0)
    
    def test_salesApprovaleButtonCmd_attached_to_widget(self):
        
        self.assertNotEqual(-1, self.mainWindow_instance.salesApprovedButton.cget('command').find('salesApprovedButtonCmd'))

    def test_save_updates_metadata(self):
        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(os, 'mkdir', return_value=None) as _mock1:
                self.mainWindow_instance.salesQC.config(text='NA')
                self.mainWindow_instance.approvalDate.config(text='NA')
                now = datetime.datetime.now()
                
                test_value = [
                    getpass.getuser(),
                    now.strftime("%Y-%m-%d")
                ]

                self.mainWindow_instance.salesApprovedButtonCmd()

                self.assertEqual(test_value[0], self.mainWindow_instance.salesQC.cget('text'))
                self.assertEqual(test_value[1], self.mainWindow_instance.approvalDate.cget('text'))


    def test_save_calls_open_method(self):

        with patch.object(os, 'mkdir', return_value=None) as _mock1,\
                patch('builtins.open', new=mock_open(read_data='')) as _file:
                self.mainWindow_instance.ADFfilename.config(text=test_filename)
                
                self.mainWindow_instance.salesApprovedButtonCmd()

                _file.assert_called_once_with(new_filename[:-4]+'.json', 'w')
    
    def test_salesApprovedButton_sets_sales_metadata(self):
        now = datetime.datetime.now()

        self.mainWindow_instance.instructionsJSON['salesQC'] = 'NA'
        self.mainWindow_instance.instructionsJSON['approvalDate'] = 'NA'
        self.mainWindow_instance.instructionsJSON['mfgQC'] = 'NA'
        self.mainWindow_instance.instructionsJSON['sysTestDate'] = 'NA'
        
        self.test_JSONDict['salesQC'] = getpass.getuser()
        self.test_JSONDict['approvalDate'] = now.strftime("%Y-%m-%d")
        self.test_JSONDict['mfgQC'] = 'NA'
        self.test_JSONDict['sysTestDate'] = 'NA'

        with patch.object(tool.mainWindowClass, "saveInstructionsJSON", return_value=None):

            self.mainWindow_instance.salesApprovedButtonCmd()

            self.assertEqual(self.test_JSONDict['salesQC'], self.mainWindow_instance.instructionsJSON['salesQC'])
            self.assertEqual(self.test_JSONDict['approvalDate'], self.mainWindow_instance.instructionsJSON['approvalDate'])
            self.assertEqual(self.test_JSONDict['mfgQC'], self.mainWindow_instance.instructionsJSON['mfgQC'])
            self.assertEqual(self.test_JSONDict['sysTestDate'], self.mainWindow_instance.instructionsJSON['sysTestDate'])



    def test_save_locks_save_button(self):
        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(os, 'mkdir', return_value=None) as _mock1:
                self.mainWindow_instance.salesApprovedButton.config(state='normal')
                test_value = 'disabled'

                self.mainWindow_instance.salesApprovedButtonCmd()

                self.assertEqual(test_value, self.mainWindow_instance.salesApprovedButton.cget('state'))
    
    def test_save_locks_checkbuttons(self):
        with patch('builtins.open', new=mock_open(read_data='')) as _file,\
            patch.object(os, 'mkdir', return_value=None) as _mock1:
                for index in range(len(self.mainWindow_instance.approvalCheckList)):
                    self.mainWindow_instance.approvalCheckList[index].config(state='normal')
                test_value = 'disabled'

                self.mainWindow_instance.salesApprovedButtonCmd()

                for index in range(len(self.mainWindow_instance.approvalCheckList)):
                    self.assertEqual(test_value, self.mainWindow_instance.approvalCheckList[index].cget('state'))

class Test_systemTestButtonCmd(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

        cls.mainWindow_instance.createInstructionsJSON()
   
    ##### Test Paths #####

    def test_systemTestButtonCmd_calls_simulationPopup_Happy_path(self):

        test_call_args = []

        for row_index in range(len(self.mainWindow_instance.instructionsJSON['steps'])):
            test_call_args.append(call(row_index))

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(test_call_args, _mock_dialog.call_args_list)

    def test_systemTestButtonCmd_calls_simulationPopup_once_if_cancelled(self):

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=None) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(1, _mock_dialog.call_count)
                self.assertEqual(0, _mock_dialog2.call_count)
                self.assertEqual(0, _mock_dialog3.call_count)
    
    def test_systemTestButtonCmd_calls_relayCheckPopup_Happy_path(self):

        test_call_args = []
        for row_index in range(len(self.mainWindow_instance.instructionsJSON['steps'])):
            test_call_args.append(call(row_index))

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(test_call_args, _mock_dialog2.call_args_list)

    def test_systemTestButtonCmd_calls_RelayCheckPopup_once_if_cancelled(self):
        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=None) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(1, _mock_dialog.call_count)
                self.assertEqual(1, _mock_dialog2.call_count)
                self.assertEqual(0, _mock_dialog3.call_count)

    def test_systemTestButtonCmd_calls_analogCheckPopup_Happy_path(self):

        test_call_args = []
        for row_index in range(len(self.mainWindow_instance.instructionsJSON['steps'])):
            if self.mainWindow_instance.instructionsJSON['steps'][row_index]['analogOutList'] != [] :
                test_call_args.append(call(row_index))

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(test_call_args, _mock_dialog3.call_args_list)

    def test_systemTestButtonCmd_calls_analogCheckPopup_once_if_cancelled(self):
        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=None) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(1, _mock_dialog.call_count)
                self.assertEqual(1, _mock_dialog2.call_count)
                self.assertEqual(1, _mock_dialog3.call_count)

    def test_systemTestButtonCmd_call_order_Happy_path(self):

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                manager = Mock()
                manager.attach_mock(_mock_dialog, 'simulationPopup')
                manager.attach_mock(_mock_dialog2, 'relayCheckPopup')
                manager.attach_mock(_mock_dialog3, 'analogCheckPopup')

                expected_calls = []
                for row_index in range(len(self.mainWindow_instance.instructionsJSON['steps'])):
                    expected_calls.append(call.simulationPopup(row_index))
                    expected_calls.append(call.relayCheckPopup(row_index))
                    if self.mainWindow_instance.instructionsJSON['steps'][row_index]['analogOutList'] != []:
                        expected_calls.append(call.analogCheckPopup(row_index))
    
                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(expected_calls, manager.mock_calls)
    
    def test_systemTestButtonCmd_call_order_UnHappy_path(self):

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=False) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=False) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save:

                manager = Mock()
                manager.attach_mock(_mock_dialog, 'simulationPopup')
                manager.attach_mock(_mock_dialog2, 'relayCheckPopup')
                manager.attach_mock(_mock_dialog3, 'analogCheckPopup')

                expected_calls = []
                for row_index in range(len(self.mainWindow_instance.instructionsJSON['steps'])):
                    expected_calls.append(call.simulationPopup(row_index))
                    expected_calls.append(call.relayCheckPopup(row_index))
                    if self.mainWindow_instance.instructionsJSON['steps'][row_index]['analogOutList'] != []:
                        expected_calls.append(call.analogCheckPopup(row_index))
    
                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(expected_calls, manager.mock_calls)

    def test_systemTestButtonCmd_calls_save_results_method(self):
        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=False) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=False) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=False) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                _mock_save1.assert_called_once()

    def test_systemTestButtonCmd_calls_save_instruction_method_on_pass(self):
        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                _mock_save2.assert_called_once()
    
    def test_systemTestButtonCmd_does_not_calls_save_instruction_method_on_fail(self):
        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=False) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(0, _mock_save2.call_count)

    def test_systemTestButtonCmd_updates_metadata_on_pass(self):

        self.mainWindow_instance.mfgQC.config(text='NA')
        self.mainWindow_instance.sysTestDate.config(text='NA')

        test_value1 = getpass.getuser()
        now = datetime.datetime.now()
        test_value2 = now.strftime("%Y-%m-%d")

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=True) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(test_value1, self.mainWindow_instance.mfgQC.cget('text'))
                self.assertEqual(test_value2, self.mainWindow_instance.sysTestDate.cget('text'))

    def test_systemTestButtonCmd_updates_metadata_only_on_pass(self):

        self.mainWindow_instance.mfgQC.config(text='NA')
        self.mainWindow_instance.sysTestDate.config(text='NA')

        test_value1 = 'NA'
        test_value2 = 'NA'

        with patch.object(tool.mainWindowClass, 'simulationPopup', return_value=True) as _mock_dialog,\
            patch.object(tool.mainWindowClass, 'relayCheckPopup', return_value=False) as _mock_dialog2,\
            patch.object(tool.mainWindowClass, 'analogCheckPopup', return_value=True) as _mock_dialog3,\
            patch.object(tool.mainWindowClass, 'saveTestResults', return_value=None) as _mock_save1,\
            patch.object(tool.mainWindowClass, 'saveInstructionsJSON', return_value=None) as _mock_save2:

                self.mainWindow_instance.systemTestButtonCmd()

                self.assertEqual(test_value1, self.mainWindow_instance.mfgQC.cget('text'))
                self.assertEqual(test_value2, self.mainWindow_instance.sysTestDate.cget('text')) 
          
if __name__ == "__main__":
    unittest.main()