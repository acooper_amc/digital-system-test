import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys
import UMLObjects

import adf2diagram

mock = unittest.mock.MagicMock()

class Test_stateVariables_class_constructor(unittest.TestCase):

    def test_stateVariables_init_sets_all_to_default(self):
        stateVaraibles_insntace = adf2diagram.stateVariables()
        self.assertEqual('', stateVaraibles_insntace.filename)
        self.assertEqual([], stateVaraibles_insntace.ADFMsgLists)
        self.assertEqual([], stateVaraibles_insntace.ADFAnalogOutList)
        self.assertEqual([], stateVaraibles_insntace.ADFRelayList)
        self.assertEqual([], stateVaraibles_insntace.ADFSensorList)
        self.assertEqual([], stateVaraibles_insntace.ADFRS485ConfigurationList)
        self.assertEqual([], stateVaraibles_insntace.ADFGasLabelList)
        self.assertEqual([], stateVaraibles_insntace.ADFEngLabelList)
        self.assertTrue(isinstance(stateVaraibles_insntace.UMLmonitor, UMLObjects.monitor ))
        self.assertEqual([], stateVaraibles_insntace.UMLDigitalTransmitterList)
        self.assertEqual([], stateVaraibles_insntace.UMLRelayModuleList)
        self.assertEqual([], stateVaraibles_insntace.UMLAnalogInModuleList)

class Test_StateMachine_constructor_method(unittest.TestCase):

    def test_StateMahcine_init_ceates_a_state_varaible_object(self):
        with patch.object(adf2diagram.stateVariables, "__init__", return_value = None) as m:
            StateMachine_instance = adf2diagram.adfToDiagramFSM()

            m.assert_called_once()
    
    def test_StateMahcine_init_sets_initial_state_to_ReadyState(self):
        StateMachine_instance = adf2diagram.adfToDiagramFSM()
        
        self.assertTrue(isinstance(StateMachine_instance.currentState, adf2diagram.readyState))

    def test_StateMachine_init_sets_initial_state_to_param(self):
        StateMachine_instance = adf2diagram.adfToDiagramFSM(adf2diagram.readFileState())
        
        self.assertTrue(isinstance(StateMachine_instance.currentState, adf2diagram.readFileState))

    def test_StateMachine_init_sets_filename_to_param(self):
        test_filename = 'c:\\some_dir\\some_file.adf'
        StateMachine_instance = adf2diagram.adfToDiagramFSM(fileName=test_filename)
        
        self.assertEqual(test_filename, StateMachine_instance.stateVars.filename)
    

class Test_StateMachine_runAll_method(unittest.TestCase):

    def test_StateMachine_runAll_method_calls_run_then_next(self):
        with patch.object(adf2diagram.readyState, "run", return_value = None) as m1, patch.object(adf2diagram.readyState, "next", return_value = adf2diagram.finishState()) as m2:
            StateMachine_instance = adf2diagram.adfToDiagramFSM()
            
            StateMachine_instance.runAll()
            
            m1.assert_called()
            m2.assert_called()
    
    def test_StateMachine_runAll_method_updates_current_state(self):
        with patch.object(adf2diagram.readyState, "run", return_value = None) as m1, patch.object(adf2diagram.readyState, "next", return_value = adf2diagram.finishState()) as m2:
            StateMachine_instance = adf2diagram.adfToDiagramFSM()
            
            StateMachine_instance.runAll()
            
            self.assertTrue(isinstance(StateMachine_instance.currentState, adf2diagram.finishState))

    def test_StateMachine_runAll_method_halts_if_current_state_is_finishState(self):

        with patch.object(adf2diagram.finishState, "run", return_value = None) as m1, patch.object(adf2diagram.finishState, "next", return_value = None) as m2:
            StateMachine_instance = adf2diagram.adfToDiagramFSM()
            StateMachine_instance.currentState = adf2diagram.finishState()
            
            StateMachine_instance.runAll()
            
            m1.assert_not_called()
            m2.assert_not_called()

if __name__ == "__main__":
    unittest.main()


        


