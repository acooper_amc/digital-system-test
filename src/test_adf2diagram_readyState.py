import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys

import adf2diagram

mock = unittest.mock.MagicMock()

class Test_State_class_next_methods(unittest.TestCase):

    def test_readyState_advances_to_readFileState(self):
        readyState_instance = adf2diagram.readyState()
        self.assertTrue(isinstance(readyState_instance.next(), adf2diagram.readFileState))
  
class Test_readyState_run_method(unittest.TestCase): 

    def test_readyState_run_gets_filename_from_arg(self):
        test_filename = ''
        test_argv = ["adf2diagram.py", "test_file.adf"]

        with patch('builtins.input', return_value=test_filename), patch.object(sys, 'argv', test_argv):
            readyState_instance = adf2diagram.readyState()
            stateVaraibles_instance = adf2diagram.stateVariables()
            
            readyState_instance.run(stateVaraibles_instance)

            self.assertEquals(test_argv[1], stateVaraibles_instance.filename)

    def test_readyState_run_prompts_user_for_filename_if_no_args(self):
        test_filename = "test_file.adf"

        test_argv = ["adf2diagram.py"]

        with patch('builtins.input', return_value=test_filename), patch.object(sys, 'argv', test_argv):
            readyState_instance = adf2diagram.readyState()
            stateVaraibles_instance = adf2diagram.stateVariables()
            
            readyState_instance.run(stateVaraibles_instance)

            self.assertEquals(test_filename, stateVaraibles_instance.filename)

if __name__ == "__main__":
    unittest.main()


        


