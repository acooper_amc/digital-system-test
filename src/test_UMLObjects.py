import unittest
import UMLObjects

class Test_UMLobject_monitor_constructors(unittest.TestCase):

    def test_monitor_init_sets_localRelays_to_default(self):
        monitor_instance = UMLObjects.monitor()

        self.assertEqual({}, monitor_instance.localRelays)    
    def test_monitor_init_sets_localAnalogOut_to_default(self):
        monitor_instance = UMLObjects.monitor()
        self.assertEqual({}, monitor_instance.localAnalogOut)

class Test_UMLobject_monitor_makeUMLString(unittest.TestCase):

    def test_method_generates_string(self):
        monitor_instance = UMLObjects.monitor()
        monitor_instance.localRelays = {
        '0' : '0',
        '1' : '1',
        '2' : '2',
        '3' : '3',
        '4' : '4',
        '5' : '5',
        '6' : '6',
        '7' : '7'
        }
        monitor_instance.localAnalogOut = {
        '0' : '0',
        '1' : '1',
        '2' : '2',
        '3' : '3'
        }

        testStr = ''
        testStr += '\n'
        testStr += 'object 1DBX {\n'
        testStr += '   __Local Relays__\n'
        for relayID in monitor_instance.localRelays:
            testStr += '   Relay ' 
            testStr += relayID 
            testStr += ' : Local ' 
            testStr += monitor_instance.localRelays[relayID] 
            testStr += '\n'
        testStr += '   __Local Analog Out__\n'
        for analogOutID in monitor_instance.localAnalogOut:
            testStr += '   AnalogOut ' 
            testStr += analogOutID 
            testStr += ' : Local ' 
            testStr += monitor_instance.localAnalogOut[analogOutID] 
            testStr += '\n'
        testStr += '}\n'

        self.assertEqual(testStr, monitor_instance.makeUMLString())
        
class Test_UMLobject_MODBUSInterface_constructors(unittest.TestCase):

    def test_MODBUSInterface_init_sets_fields_to_param(self):
        MODBUSInterface_ID = '1'
        MODBUSInterface_instance = UMLObjects.MODBUSInterface(MODBUSInterface_ID)

        self.assertEqual(MODBUSInterface_ID, MODBUSInterface_instance.ID)

class Test_UMLobject_digitalDevice_constructors(unittest.TestCase):
    def test_digitalDevice_init_sets_MODBUS_ID_to_param(self):
        MODBUSInterface_ID = '1'
        digitialDevice_instance = UMLObjects.digitalDevice(MODBUSInterface_ID)

        self.assertEqual(MODBUSInterface_ID, digitialDevice_instance.MODBUS_ID)
     
class Test_UMLobject_digitalTransmitter_constructors(unittest.TestCase):
    def test_digitalTransmitter_MODBUSInterface_to_param(self):
        MODBUSInterface_ID = '1'
        test_sensor_list = [
            {'0': '1'},
            {'1': '2'},
            {'2': '3'}
        ]
        digitialTransmitter_instance = UMLObjects.digitalTransmitter(MODBUSInterface_ID, test_sensor_list)

        self.assertEqual(MODBUSInterface_ID, digitialTransmitter_instance.MODBUS_ID)

    def test_digitalTransmitter_sensors_to_default(self):
        MODBUSInterface_ID = '1'
        test_sensor_list = {
            '0': '1',
            '1': '2',
            '2': '3'
            }
        

        digitialTransmitter_instance = UMLObjects.digitalTransmitter(MODBUSInterface_ID, test_sensor_list)

       
        self.assertEqual(test_sensor_list, digitialTransmitter_instance.sensors)

class Test_UMLobject_digitalModule_constructors(unittest.TestCase):
    def test_digitalModule_init_sets_MODBUSInterface_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        digitalModule_instance = UMLObjects.digitalModules(MODBUSInterface_ID, address)

        self.assertEqual(MODBUSInterface_ID, digitalModule_instance.MODBUS_ID)

    def test_digitalModule_init_sets_address_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        digitalModule_instance = UMLObjects.digitalModules(MODBUSInterface_ID, address)

        self.assertEqual(address, digitalModule_instance.address)

class Test_UMLobject_relayModule_constructor(unittest.TestCase):
    def test_relayModule_init_sets_MODBUSInterface_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        relayModule_instance = UMLObjects.relayModule(MODBUSInterface_ID, address, [])

        self.assertEqual(MODBUSInterface_ID, relayModule_instance.MODBUS_ID)

    def test_relayModule_init_sets_address_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        relayModule_instance = UMLObjects.relayModule(MODBUSInterface_ID, address, [])

        self.assertEqual(address, relayModule_instance.address)

    def test_relayModule_init_sets_relays_to_args(self):
        MODBUSInterface_ID = '1'
        address = '1'
        test_relay_list = {
            '0': '1',
            '1': '2',
            '2': '3'
            }
        
        relayModule_instance = UMLObjects.relayModule(MODBUSInterface_ID, address, test_relay_list)

        self.assertEqual(test_relay_list, relayModule_instance.relays)

class Test_UMLobject_analogInputModule_constructor(unittest.TestCase):
    def test_analogInputModule_init_sets_MODBUSInterface_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        analogInputModule_instance = UMLObjects.analogInputModule(MODBUSInterface_ID, address, {})

        self.assertEqual(MODBUSInterface_ID, analogInputModule_instance.MODBUS_ID)

    def test_analogInputModule_init_sets_address_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        analogInputModule_instance = UMLObjects.analogInputModule(MODBUSInterface_ID, address, {})

        self.assertEqual(address, analogInputModule_instance.address)

    def test_analogInputModule_init_sets_sensors_to_args(self):
        MODBUSInterface_ID = '1'
        address = '1'
        test_sensor_list = {
            '0': '1',
            '1': '2',
            '2': '3'
            }
        analogInputModule_instance = UMLObjects.analogInputModule(MODBUSInterface_ID, address, test_sensor_list)

        self.assertEqual(test_sensor_list, analogInputModule_instance.sensors)

class Test_UMLobject_analogOutputModule_constructor(unittest.TestCase):
    def test_analogOutputModule_init_sets_MODBUSInterface_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        analogOutputModule_instance = UMLObjects.analogOutputModule(MODBUSInterface_ID, address, {})

        self.assertEqual(MODBUSInterface_ID, analogOutputModule_instance.MODBUS_ID)

    def test_analogOutputModule_init_sets_address_to_arg(self):
        MODBUSInterface_ID = '1'
        address = '1'
        analogOutputModule_instance = UMLObjects.analogOutputModule(MODBUSInterface_ID, address, {})

        self.assertEqual(address, analogOutputModule_instance.address)

    def test_analogOutputModule_init_sets_outputs_to_args(self):
        MODBUSInterface_ID = '1'
        address = '1'
        test_output_list = {
            '0': '1',
            '1': '2',
            '2': '3'
            }
        analogOutputModule_instance = UMLObjects.analogOutputModule(MODBUSInterface_ID, address, test_output_list)

        self.assertEqual(test_output_list, analogOutputModule_instance.outputs) 

class Test_UMLObject_RS485Configuration_constructor(unittest.TestCase):
    def test_RS485Configuration_sets_members_to_arg(self):
        test_strs = ['1', '2', '3', '4', '5', '6']
        RS485Configuration_instance = UMLObjects.RS485Configuration(*test_strs)

        self.assertEqual(test_strs[0], RS485Configuration_instance.MODBUS_ID)
        self.assertEqual(test_strs[1], RS485Configuration_instance.Protocol)
        self.assertEqual(test_strs[2], RS485Configuration_instance.BaudRate)
        self.assertEqual(test_strs[3], RS485Configuration_instance.CharLen)
        self.assertEqual(test_strs[4], RS485Configuration_instance.Parity)
        self.assertEqual(test_strs[5], RS485Configuration_instance.StopBits)

if __name__ == "__main__":
    unittest.main()