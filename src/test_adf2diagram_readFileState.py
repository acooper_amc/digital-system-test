import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import builtins
import sys

import adf2diagram

mock = unittest.mock.MagicMock()

class Test_State_class_next_methods(unittest.TestCase):

    def test_readFileState_advances_to_convertToObjectsState(self):
        readFileState_instance = adf2diagram.readFileState()
        self.assertTrue(isinstance(readFileState_instance.next(), adf2diagram.convertToObjectsState))
   
class Test_readFileState_run_method(unittest.TestCase):

    def test_readFileState_run_opens_file(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file:

            test_filename = 'test_file.adf'

            readFileState_instance = adf2diagram.readFileState()
            stateVaraibles_instance = adf2diagram.stateVariables()
            stateVaraibles_instance.filename = test_filename
            
            readFileState_instance.run(stateVaraibles_instance)

            _file.assert_called_once_with(test_filename, 'r')

    def test_readFileState_run_reads_line_from_file(self):
        with patch('builtins.open', new=mock_open(read_data='Foo')) as _file:

            test_filename = 'test_file.adf'

            readFileState_instance = adf2diagram.readFileState()
            stateVaraibles_instance = adf2diagram.stateVariables()
            stateVaraibles_instance.filename = test_filename
            
            readFileState_instance.run(stateVaraibles_instance)

            _file.assert_called_once_with(test_filename, 'r')
    
    def test_readFileState_run_FSM(self):
        sample_msg  = '{-2,-2,ADFRelayV4,AMC-1DBr2,0,1,0,10,0,1,0,1,0,0,0}'
        sample_list = ['-2','-2','ADFRelayV4','AMC-1DBr2','0','1','0','10','0',
        '1','0','1','0','0','0']

        with patch('builtins.open', new=mock_open(read_data=sample_msg)) as _file:

            test_filename = 'test_file.adf'

            readFileState_instance = adf2diagram.readFileState()
            stateVaraibles_instance = adf2diagram.stateVariables()
            stateVaraibles_instance.filename = test_filename
            
            readFileState_instance.run(stateVaraibles_instance)

            self.assertEqual(sample_list, stateVaraibles_instance.ADFMsgLists[0])
        
    def test_readFileState_run_works_until_EOF(self):
        sample_msg  = '{-2,-2,ADFRelayV4,AMC-1DBr2,0,1,0,10,0,1,0,1,0,0,0}\n{-2,-2,ADFSensorV4,AMC-1DBr2,0,3,0,1000,0,1,2,0,25,0,1,100,0,,,,3,20,0,,1,1,1,0,0}'

        sample_list = [
            ['-2','-2','ADFRelayV4','AMC-1DBr2','0','1','0','10','0','1','0','1','0','0','0'],
            ['-2','-2','ADFSensorV4','AMC-1DBr2','0','3','0','1000','0','1','2','0','25','0','1','100','0','','','','3','20','0','','1','1','1','0','0']]

        with patch('builtins.open', new=mock_open(read_data=sample_msg)) as _file:

            test_filename = 'test_file.adf'

            readFileState_instance = adf2diagram.readFileState()
            stateVaraibles_instance = adf2diagram.stateVariables()
            stateVaraibles_instance.filename = test_filename
            
            readFileState_instance.run(stateVaraibles_instance)

            self.assertEqual(sample_list[0], stateVaraibles_instance.ADFMsgLists[0])
                        
if __name__ == "__main__":
    unittest.main()


        


