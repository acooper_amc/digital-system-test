import unittest
import unittest.mock
from unittest.mock import patch, mock_open
import ADFValidationTool as tool
import tkinter as tk
from tkinter import filedialog
import adf2diagram

mock = unittest.mock.MagicMock()

#Cheange workign directory
import os
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

test_filename = 'c:\\somedirectory\\somefile.adf'
demo_adf_filename_noSO  = '..\\test\\demo_DB.adf'
demo_csv_filename_noSO  = '..\\test\\demo_DB.csv'
demo_adf_filename       = '..\\test\\SO12345_demo_DB.adf'

class Test_salesApprovalCBcmd(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        root = tk.Tk()
        cls.mainWindow_instance = tool.mainWindowClass(root)
        cls.mainWindow_instance.ADFfilename.config(text=demo_adf_filename_noSO)

        cls.mainWindow_instance.createInstructionsJSON()
    
    def test_approvalCheckVars_defaults_to_empty(self):
        root = tk.Tk()
        test_value = []

        mainWindow_instance = tool.mainWindowClass(root)

        self.assertEqual(test_value, mainWindow_instance.approvalCheckVars)
       
    def test_salesApprovalCBcmd_added_as_checkbutton_callback(self):
               
        for checkbutton in self.mainWindow_instance.approvalCheckList:
            self.assertNotEqual(-1, checkbutton.cget('command').find('approvalCheckCmd'))

    def test_salesApprovalCBcmd_unlocks_approve_and_save_button_of_all_checkbuttons_are_checked(self):
      
        for index in range(len(self.mainWindow_instance.approvalCheckList)):
            self.mainWindow_instance.approvalCheckList[index].select()
        self.mainWindow_instance.diagramAppvlCheck.select()
        
        self.mainWindow_instance.salesApprovedButton.config(state='disable')
        self.mainWindow_instance.statusBar.config(text='', bg='white', fg='black')
        
        test_value = 'normal'

        self.mainWindow_instance.approvalCheckCmd()

        self.assertEqual(test_value, self.mainWindow_instance.salesApprovedButton.cget('state'))

    def test_salesApprovalCBcmd_locks_approve_and_save_button_if_not_all_checkbuttons_are_checked(self):

        for index in range(len(self.mainWindow_instance.approvalCheckList)):
            self.mainWindow_instance.approvalCheckList[index].select()
        self.mainWindow_instance.approvalCheckList[0].deselect()
        self.mainWindow_instance.salesApprovedButton.config(state='normal')
        test_value = 'disabled'
        
        self.mainWindow_instance.approvalCheckCmd()
       
        self.assertEqual(test_value, self.mainWindow_instance.salesApprovedButton.cget('state'))
            
if __name__ == "__main__":
    unittest.main()