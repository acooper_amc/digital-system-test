"""Objects to store data from .ADF files"""

#Sensor Object
class SensorV4:
    """An object representing the SensorV4 message in 1DBX .ADF files"""
    
    def __init__(self, P1, P2, P3, P4, P16, P5, P6, P7, P8, P21, P9, P10, P22, P11, P12, P23, P13, P14, P24, P15, P17, P18, P19, P20, P25):
        self.ID                 = P1 #P1
        self.GasLabel           = P2 #P2
        self.EngUnit            = P3 #P3
        self.FullScale          = P4 #P4
        self.Zero               = P16 #P16
        self.AlarmCondition     = P5 #P5
        self.NumAlarms          = P6 #P6
        self.AlarmRelays        = {'1': P7,  '2': P9,  '3': P11, "fail": P13} #P7,  P9,  P11, P13
        self.AlarmSetPoints     = {'1': P8,  '2': P10, '3': P12, "fail": P14} #P8,  P10, P12, P14
        self.AlarmDelay         = {'1': P21, '2': P22, '3': P23, "fail": P24} #P21, P22, P23, P24
        self.Location           = P15 #P15
        self.AdminState         = P17 #P17
        self.Interface          = P18 #P18
        self.Address            = P19 #P19
        self.Instance           = P20 #P20
        self.PartRef            = P25 #P25

#Relay Object
class RelayV4:
    """An object representing the RelayV4 message in 1DBX .ADF files"""

    def __init__(self, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11):
        self.ID                 = P1 #P1
        self.AlarmCount         = P2 #P2
        self.Delay              = P3 #P3
        self.MinRunTime         = P4 #P4
        self.Config             = P5 #P5
        self.AdminState         = P6 #P6
        self.Interface          = P7 #P7
        self.Address            = P8 #P8
        self.Instance           = P9 #P9
        self.PostRunTime        = P10 #P10
        self.PartRef            = P11 #P11

#Zone Objects
class ZoneV3:
    """An object representing the Zonev3 message in 1DBX .ADF files"""

    def __init__(self, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11, P12, P13):
        self.ID                 = P1 #P1
        self.AdminState         = P2 #P2
        self.ScheduleDay        = P3 #P3
        self.ScheduleStartHr    = P4 #P4
        self.ScheduleStartMin   = P5 #P5
        self.ScheduleEndHr      = P6 #P6
        self.ScheduleEndMin     = P7 #P7
        self.ScheduleRelay      = P8 #P8
        self.AlarmRelays        = {'1': P9, '2': P10, '3': P11, "fail": P12} #P9, P10, P11, P12
        self.AnalogOut          = P13 #P13

class ZoneSensorV3:
    """An object representing the ZoneSensorV3 message in 1DBX .ADF files"""

    def __init__(self, P1, P2, P3):
        self.ID                 = P1 #P1
        self.ZoneID             = P2 #P2
        self.SensorID           = P3 #P3

#Analog Output Module Object
class AnalogOutV4:
    """An object representing the AnalogOutV4 message in 1DBX .ADF files"""

    def __init__(self, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10):
        self.ID                 = P1 #P1
        self.AdminState         = P2 #P2
        self.Interface          = P3 #P3
        self.Address            = P4 #P4
        self.Instance           = P5 #P5
        self.Range              = P6 #P6
        self.Scale10X           = P7 #P7
        self.Type               = P8 #P8
        self.SamplePeriod       = P9 #P9
        self.PartRef            = P10 #P10

