###############################################################################
# UML Classes

class monitor:
    
    def __init__(self):
        self.localRelays    = {}
        self.localAnalogOut = {}

class MODBUSInterface:

    def __init__(self, ID):
        self.ID = ID

class digitalDevice:

    def __init__(self, MODBUS_ID):
        self.MODBUS_ID = MODBUS_ID

class digitalTransmitter( digitalDevice ):

    def __init__(self, MODBUS_ID, sensor_list):
        digitalDevice.__init__(self, MODBUS_ID)
        self.sensors = sensor_list

class digitalModules( digitalDevice ): 

    def __init__(self, MODBUS_ID, address):
        digitalDevice.__init__(self, MODBUS_ID)
        self.address = address

class relayModule( digitalModules ):

    def __init__(self, MODBUS_ID, address, relay_list):
        digitalModules.__init__(self, MODBUS_ID, address)
        self.relays = relay_list

class analogInputModule( digitalModules ):

    def __init__(self, MODBUS_ID, address, sensor_list):
        digitalModules.__init__(self, MODBUS_ID, address)
        self.sensors = sensor_list

class analogOutputModule( digitalModules ):

    def __init__(self, MODBUS_ID, address, output_list):
        digitalModules.__init__(self, MODBUS_ID, address)
        self.outputs = output_list