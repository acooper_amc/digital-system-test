import sys
import builtins
from parseADFMsgFSM import State, StateMachine, ADFMsgStateMachine, ReadFirstBracket
import parseADFMsgFSM
import ADFobjects
import UMLObjects

version = [
    0, #Major Version
    1  #Minor Version
]

###############################################################################
# State Machine Classes


class stateVariables:

    def __init__(self):
        self.filename                   = ''
        self.ADFMsgLists                = []
        self.ADFAnalogOutList           = []
        self.ADFRelayList               = []
        self.ADFSensorList              = []
        self.ADFZoneList                = []
        self.ADFZoneSensorList          = []
        self.UMLmonitor                 = UMLObjects.monitor()
        self.UMLDigitalTransmitterList  = []
        self.UMLRelayModuleList         = []
        self.UMLAnalogInModuleList      = []
        self.UMLAnalogOutModuleList     = []

class readyState(State):

    def next(self):
        return readFileState()
    
    def run(self, stateVars: stateVariables):
        if len(sys.argv) == 2:
            stateVars.filename = sys.argv[1]    
        else:
            stateVars.filename = input("Enter filename: ")


class readFileState(State):

    def __init__(self):
        self.fileObject = ''
        self.lineTxt    = ''
        self.ADFMsgFSM  = ADFMsgStateMachine(ReadFirstBracket())

    def next(self):
        return convertToObjectsState()

    def run(self, stateVars: stateVariables):
        try:
            self.fileObject = open(stateVars.filename, 'r')
        except IOError:
            print("Error opening file " + stateVars.filename)
        self.lineTxt = self.fileObject.readline()
        while (self.lineTxt != ''):
            stateVars.ADFMsgLists.append(self.ADFMsgFSM.runAll(self.lineTxt))
            self.lineTxt = self.fileObject.readline()


class convertToObjectsState(State):

    def next(self):
        return createUMLObjectsState()

    def run(self, stateVars: stateVariables):

        START_OF_ARGS = 4
        MSG_CLASS     = 2
        ENABLED       = '1'
        GAS_TYPE__UNDEF = '0'
        ADF_ANALOG_OUT_V4__ADMIN_STATUS = 5
        ADF_RELAY_V4__ADMIN_STATUS = 9
        ADF_SENSOR_V4__GAS_TYPE = 5
        ADF_ZONE_V3__ADMIN_STATUS = 5
        ADF_ZONE_SENSOR_V3__SENSOR_ID = 6

        for msg in stateVars.ADFMsgLists:

            if ('ADFAnalogOutV4' == msg[MSG_CLASS]) and (ENABLED == msg[ADF_ANALOG_OUT_V4__ADMIN_STATUS]):
                stateVars.ADFAnalogOutList.append(ADFobjects.AnalogOutV4(*msg[START_OF_ARGS:]))

                #Rebase zero indexed variables to 1 index
                stateVars.ADFAnalogOutList[-1].ID = str(int(stateVars.ADFAnalogOutList[-1].ID) + 1)
                stateVars.ADFAnalogOutList[-1].Instance = str(int(stateVars.ADFAnalogOutList[-1].Instance) + 1)

            elif ('ADFRelayV4' == msg[MSG_CLASS]) and (ENABLED == msg[ADF_RELAY_V4__ADMIN_STATUS]):
                stateVars.ADFRelayList.append(ADFobjects.RelayV4(*msg[START_OF_ARGS:]))

                #Rebase zero indexed variables to 1 index
                stateVars.ADFRelayList[-1].ID = str(int(stateVars.ADFRelayList[-1].ID) + 1)
                stateVars.ADFRelayList[-1].Instance = str(int(stateVars.ADFRelayList[-1].Instance) + 1)

        
            elif ('ADFSensorV4' == msg[MSG_CLASS]) and (msg[ADF_SENSOR_V4__GAS_TYPE] != GAS_TYPE__UNDEF):
                stateVars.ADFSensorList.append(ADFobjects.SensorV4(*msg[START_OF_ARGS:]))

                #Rebase zero indexed variables to 1 index
                stateVars.ADFSensorList[-1].ID = str(int(stateVars.ADFSensorList[-1].ID) + 1)
                stateVars.ADFSensorList[-1].Instance = str(int(stateVars.ADFSensorList[-1].Instance) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['1'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['1'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['1']) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['2'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['2'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['2']) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['3'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['3'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['3']) + 1)
                if (stateVars.ADFSensorList[-1].AlarmRelays['fail'] != ''):
                    stateVars.ADFSensorList[-1].AlarmRelays['fail'] = str(int(stateVars.ADFSensorList[-1].AlarmRelays['fail']) + 1)

            elif ('ZoneV3' == msg[MSG_CLASS]) and (ENABLED == msg[ADF_ZONE_V3__ADMIN_STATUS]):
                stateVars.ADFZoneList.append(ADFobjects.ZoneV3(*msg[START_OF_ARGS:]))
                
                #Rebase zero indexed variables to 1 index
                stateVars.ADFZoneList[-1].ID = str(int(stateVars.ADFZoneList[-1].ID) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['1'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['1']) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['2'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['2']) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['3'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['3']) + 1)
                stateVars.ADFZoneList[-1].AlarmRelays['fail'] = str(int(stateVars.ADFZoneList[-1].AlarmRelays['fail']) + 1)
                stateVars.ADFZoneList[-1].AnalogOut = str(int(stateVars.ADFZoneList[-1].AnalogOut) + 1)
        
            elif ('ZoneSensorV3' == msg[MSG_CLASS]) and (msg[ADF_ZONE_SENSOR_V3__SENSOR_ID]) != '-':
                stateVars.ADFZoneSensorList.append(ADFobjects.ZoneSensorV3(*msg[START_OF_ARGS:])) 

                #Rebase zero indexed variables to 1 index
                stateVars.ADFZoneSensorList[-1].ID = str(int(stateVars.ADFZoneSensorList[-1].ID) + 1)
                stateVars.ADFZoneSensorList[-1].ZoneID = str(int(stateVars.ADFZoneSensorList[-1].ZoneID) + 1)
                stateVars.ADFZoneSensorList[-1].SensorID = str(int(stateVars.ADFZoneSensorList[-1].SensorID) + 1)

            
            else:
                pass


class createUMLObjectsState(State):

    def next(self):
        return writeFileState()

    def run(self, stateVars: stateVariables): 

        PART_REF__LOCAL = '0'
        PART_REF__DIGITAL_TRANSMITTER = '0'
        PART_REF__AMC_1D8R = '1'
        PART_REF__AMC_1DDA1_8 = '9'
        PART_REF__AMC_1DBCO_8 = '6'
     
        number2MODBUS = {
            "1" : "MODBUS_1",
            "2" : "MODBUS_2",
            "3" : "MODBUS_3",
            "4" : "MODBUS_4"
        }

        RelaysByModebusInterface = {
            'MODBUS_1' : [],
            'MODBUS_2' : [],
            'MODBUS_3' : [],
            'MODBUS_4' : []
        }
        relayModule_set = set()

        #For each ADFRelay object, group in either into local (by part_ref) 
        # or by MODBUS interface
        for relay in stateVars.ADFRelayList:
            if relay.PartRef == PART_REF__LOCAL:
                stateVars.UMLmonitor.localRelays.update({relay.ID : relay.Instance})
            elif PART_REF__AMC_1D8R == relay.PartRef:
                if '1' == relay.Interface:
                    RelaysByModebusInterface['MODBUS_1'].append(relay)
                elif '2' == relay.Interface:
                    RelaysByModebusInterface['MODBUS_2'].append(relay)
                elif '3' == relay.Interface:
                    RelaysByModebusInterface['MODBUS_3'].append(relay)
                elif '4' == relay.Interface:
                    RelaysByModebusInterface['MODBUS_4'].append(relay)
                else:
                    pass
                relayModule_set.add((relay.Interface, relay.Address))
            else:
                pass

        #For each module in the relay set, create a UML object
        relayModule_set = sorted(relayModule_set) 
        for module in relayModule_set:
            relay_list = {}
            for relay in RelaysByModebusInterface[number2MODBUS[module[0]]]:
                if (relay.Address == module[1]):
                    relay_list.update({relay.ID: relay.Instance})
            stateVars.UMLRelayModuleList.append(UMLObjects.relayModule(number2MODBUS[module[0]], module[1], relay_list) )
        
        AnalogOutByModebusInterface = {
            'MODBUS_1' : [],
            'MODBUS_2' : [],
            'MODBUS_3' : [],
            'MODBUS_4' : []
        }
        analogOutModule_set = set()

        #For each ADFAnalogOut object, group it either in local (by part_ref) or
        #by MODBUS interface
        for analogOut in stateVars.ADFAnalogOutList:
            if analogOut.PartRef == PART_REF__LOCAL:
                stateVars.UMLmonitor.localAnalogOut.update({analogOut.ID : analogOut.Instance})
            elif PART_REF__AMC_1DBCO_8 == analogOut.PartRef:
                if '1' == analogOut.Interface:
                    AnalogOutByModebusInterface['MODBUS_1'].append(analogOut)
                elif '2' == analogOut.Interface:
                    AnalogOutByModebusInterface['MODBUS_2'].append(analogOut)
                elif '3' == analogOut.Interface:
                    AnalogOutByModebusInterface['MODBUS_3'].append(analogOut)
                elif '4' == analogOut.Interface:
                    AnalogOutByModebusInterface['MODBUS_4'].append(analogOut)
                else:
                    pass
                analogOutModule_set.add((analogOut.Interface, analogOut.Address))
            else:
                pass
        
        #For each module in the analongOut set, create a UML object
        analogOutModule_set = sorted(analogOutModule_set)
        for module in analogOutModule_set:
            analogOutList = {}
            for analogOut in AnalogOutByModebusInterface[number2MODBUS[module[0]]]:
                if (analogOut.Address == module[1]):
                    analogOutList.update({analogOut.ID : analogOut.Instance})
            stateVars.UMLAnalogOutModuleList.append(UMLObjects.analogOutputModule(number2MODBUS[module[0]], module[1], analogOutList))
        
        AnalogInByModebusInterface = {
            'MODBUS_1' : [],
            'MODBUS_2' : [],
            'MODBUS_3' : [],
            'MODBUS_4' : []
        }    
        SensorsByModebusInterface = {
            'MODBUS_1' : [],
            'MODBUS_2' : [],
            'MODBUS_3' : [],
            'MODBUS_4' : []
        }

        #Group digital transmitters by MODBUS interface and
        #group analog inouts by MODBUS interface
        analogInModule_set = set()
        for sensor in stateVars.ADFSensorList: 

            if PART_REF__DIGITAL_TRANSMITTER == sensor.PartRef:
                if('1' == sensor.Interface):
                    SensorsByModebusInterface['MODBUS_1'].append(sensor)
                elif('2' == sensor.Interface):
                    SensorsByModebusInterface['MODBUS_2'].append(sensor)
                elif('3' == sensor.Interface):
                    SensorsByModebusInterface['MODBUS_3'].append(sensor)
                elif('4' == sensor.Interface):
                    SensorsByModebusInterface['MODBUS_4'].append(sensor)
                else:
                    pass
            elif PART_REF__AMC_1DDA1_8 == sensor.PartRef:
                if('1' == sensor.Interface):
                    AnalogInByModebusInterface['MODBUS_1'].append(sensor)
                elif('2' == sensor.Interface):
                    AnalogInByModebusInterface['MODBUS_2'].append(sensor)
                elif('3' == sensor.Interface):
                    AnalogInByModebusInterface['MODBUS_3'].append(sensor)
                elif('4' == sensor.Interface):
                    AnalogInByModebusInterface['MODBUS_4'].append(sensor)
                else:
                    pass
                analogInModule_set.add((sensor.Interface, sensor.Address))

        #for each group of sensors sharing a MODBUS interface, create one UMLSensor object
        analogInModule_set = sorted(analogInModule_set)
        for MODBUSinterface in SensorsByModebusInterface:
            sensor_list = {}
            for sensor in SensorsByModebusInterface[MODBUSinterface]:
                if PART_REF__DIGITAL_TRANSMITTER == sensor.PartRef:
                    sensor_list.update({sensor.ID : sensor.Address})
            stateVars.UMLDigitalTransmitterList.append(UMLObjects.digitalTransmitter(MODBUSinterface, sensor_list))
        
        for module in analogInModule_set:
            sensor_list = {}
            for sensor in AnalogInByModebusInterface[number2MODBUS[module[0]]]:
                if (sensor.Address == module[1]):
                    sensor_list.update({sensor.ID : sensor.Instance})
            stateVars.UMLAnalogInModuleList.append(UMLObjects.analogInputModule(number2MODBUS[module[0]], module[1], sensor_list))


class writeFileState(State):

    def __init__(self):
        self.masterStr = ''

    def next(self):
        return finishState()

    def run(self, stateVars: stateVariables) :
        #Add plantUML open string
        self.masterStr += '@startuml\n'

        #Add a title
        self.masterStr += ('\n' + 'title ' + stateVars.filename + '\\n' + 'Block Wiring Diagram' + '\n')

        #Create a monitor graphic object
        self.masterStr += ''
        self.masterStr += '\n'
        self.masterStr += 'object 1DBX {\n'
        self.masterStr += '   __Local Relays__\n'
        for relayID in stateVars.UMLmonitor.localRelays:
            self.masterStr += '   Relay ' 
            self.masterStr += relayID 
            self.masterStr += ' : Local ' 
            self.masterStr += stateVars.UMLmonitor.localRelays[relayID] 
            self.masterStr += '\n'
        self.masterStr += '   __Local Analog Out__\n'
        for analogOutID in stateVars.UMLmonitor.localAnalogOut:
            self.masterStr += '   AnalogOut ' 
            self.masterStr += analogOutID 
            self.masterStr += ' : Local ' 
            self.masterStr += stateVars.UMLmonitor.localAnalogOut[analogOutID] 
            self.masterStr += '\n'
        self.masterStr += '}\n'

        ObjectsByMODBUSInterface = {
            'MODBUS_1' : [],
            'MODBUS_2' : [],
            'MODBUS_3' : [],
            'MODBUS_4' : []
        }

        #Create digital transmiter graphic objects
        transmitterNum = 1
        for digitalTransmitterObject in stateVars.UMLDigitalTransmitterList:
            if(0 != len(digitalTransmitterObject.sensors)):
                self.masterStr += ''
                self.masterStr += '\n'
                self.masterStr += 'object Digital_Transmitters_' 
                self.masterStr += str(transmitterNum)
                self.masterStr += '{\n'
                for sensorID in digitalTransmitterObject.sensors:
                    self.masterStr += '   Sensor ' 
                    self.masterStr += sensorID 
                    self.masterStr += ' : Address ' 
                    self.masterStr += digitalTransmitterObject.sensors[sensorID] 
                    self.masterStr += '\n'
                self.masterStr += '}\n'
                ObjectsByMODBUSInterface[digitalTransmitterObject.MODBUS_ID].append('Digital_Transmitters_' + str(transmitterNum))
                transmitterNum += 1
            
        #Create analog input module graphic objects
        AnalogInNum = 1
        for analogInObject in stateVars.UMLAnalogInModuleList:
            self.masterStr += '\n'
            self.masterStr += 'object AnalogIn_' + str(AnalogInNum) +  '{\n'
            self.masterStr += '   Address : ' + analogInObject.address + '\n'
            self.masterStr += '   __Sensors__\n'

            for sensorID in analogInObject.sensors:
                self.masterStr += '   Sensor ' + sensorID + ' : Channel ' + analogInObject.sensors[sensorID] + '\n'
            
            self.masterStr += '}\n'
            ObjectsByMODBUSInterface[analogInObject.MODBUS_ID].append('AnalogIn_' + str(AnalogInNum)) 
            AnalogInNum = AnalogInNum + 1

        #Create analog output module graphic objects
        AnalogOutNum = 1
        for analogOutObject in stateVars.UMLAnalogOutModuleList:
            self.masterStr += '\n'
            self.masterStr += 'object AnalogOut_' + str(AnalogOutNum) +  '{\n'
            self.masterStr += '   Address : ' + analogOutObject.address + '\n'
            self.masterStr += '   __Outputs__\n'
            for analogOutID in analogOutObject.outputs:
                self.masterStr += '   Analog Out ' + analogOutID + ' : Channel ' + analogOutObject.outputs[analogOutID] + '\n'
            self.masterStr += '}\n'
            ObjectsByMODBUSInterface[analogOutObject.MODBUS_ID].append('AnalogOut_' + str(AnalogOutNum))
            AnalogOutNum = AnalogOutNum + 1

        #Create relay module graphic objects
        RelayModuleNum = 1
        for relayObject in stateVars.UMLRelayModuleList:
            self.masterStr += '\n'
            self.masterStr += 'object RelayModule_' + str(RelayModuleNum) +  '{\n'
            self.masterStr += '   Address : ' + relayObject.address + '\n'
            self.masterStr += '   __Relays__\n'
            for relayID in relayObject.relays:
                self.masterStr += '   Relay ' + relayID + ' : Local ' + relayObject.relays[relayID] + '\n'
            self.masterStr += '}\n'
            ObjectsByMODBUSInterface[relayObject.MODBUS_ID].append('RelayModule_' + str(RelayModuleNum))
            RelayModuleNum = RelayModuleNum + 1

        #Create a list of active MODBUS interface
        active_interfaces = []
        for interface in ObjectsByMODBUSInterface:
            if len(ObjectsByMODBUSInterface[interface]) != 0:
                active_interfaces.append(interface)
        
        #Create links between first elements for formatting
        self.masterStr += '\n'
       
        for interface in active_interfaces:
            count = 1
            for graphicObject in ObjectsByMODBUSInterface[interface]:
                if 1 == count:
                    self.masterStr += '1DBX -- ' + graphicObject + ' : ' + interface + '\n'
                    if len(ObjectsByMODBUSInterface[interface]) != 1:
                        self.masterStr += graphicObject + ' -- '
                elif len(ObjectsByMODBUSInterface[interface]) == count:
                    self.masterStr += graphicObject + ' : ' + interface + '\n'
                else:
                    self.masterStr += graphicObject + ' : ' + interface + '\n'
                    self.masterStr += graphicObject + ' -- '
                count += 1    
        self.masterStr += '\n'

        #Add plantUML close string
        self.masterStr += '@enduml'

        #Write plantUML to file
        target_puml_file = stateVars.filename.split('.')[0]
        target_puml_file += '.puml'
        file_handle = open(target_puml_file, 'w')
        file_handle.write(self.masterStr)
        file_handle.close()
        
     

class finishState(State): pass

class adfToDiagramFSM(StateMachine): 
    
    def __init__(self):
        self.stateVars = stateVariables()
        self.currentState = readyState()

    def runAll(self):
        while not isinstance(self.currentState, finishState):
            self.currentState.run(self.stateVars)
            self.currentState = self.currentState.next()

if __name__ == "__main__":
    adfToDiagramFSM_instance = adfToDiagramFSM()
    adfToDiagramFSM_instance.runAll()
