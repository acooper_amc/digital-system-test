###############################################################################
# Global Variables

class listBuilder:
    """A set of varialbes to build strings from chars and lists from string"""
    def __init__(self):
        self.builder_str = ''
        self.builder_list = []

###############################################################################
# Default classes

class StateMachine:
    """Parent class for state machine"""
    def __init__(self, initialState):
        pass
    # Template method:
    def runAll(self, inputs):
        pass

class State:
    """Parent class for all states"""

    def run(self):
        pass
    
    def next(self, input):
        pass

###############################################################################
# State Classes

class ReadFirstBracket (State): 
    """Idle state"""

    def run(self, input, listBuilder): pass

    def next(self, input):
        #State only advances when '{' is read
        if ('{' == input):
            return AddCharToStr()
        else:
            return ReadFirstBracket()

class AddCharToStr (State):
    """Parser State"""
    def run(self, input, listBuilder):
        #Test so that the state machine won't add a ',' to the bulder string
        #if the message parameter is blank i.e ,,
        if(input != ',' and input != '{'):
            listBuilder.builder_str += input

    def next(self, input):
        
        if (',' == input):
            return AddStrToList()
        elif ('}' == input):
            return AddLastStrToList()
        else:
            return AddCharToStr()

class AddLastStrToList (State):
    """Terminal State - does not advance"""
    def run(self, input, listBuilder):
        if('\n' == input):
            pass
        else:
            listBuilder.builder_list.append(listBuilder.builder_str)
            listBuilder.builder_str = ''
    def next(self, input) : 
        return AddLastStrToList()

class AddStrToList (AddLastStrToList): 
    """Deal with ',' seperators"""
    def next(self, input):
        if(input != ','):
            return AddCharToStr()
        else:
            return AddStrToList()

###############################################################################
# State Machine Class

class ADFMsgStateMachine (StateMachine) : 

    def __init__(self, initialState):
        self.currentState = initialState
        self.listBuilderFields = listBuilder()

    def runAll(self, inputs):
        for character in inputs:
            self.currentState = self.currentState.next(character)
            self.currentState.run(character, self.listBuilderFields)
        self.currentState = ReadFirstBracket()
        return_list = self.listBuilderFields.builder_list
        self.listBuilderFields.builder_list = []
        return return_list


