SET filename = %1

IF "%1" == "" (
    python adf2diagram.py
)

IF NOT "%1" == "" (
    python adf2diagram.py "%1"
)

pause
